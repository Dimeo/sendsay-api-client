<?php

namespace Sendsay\ApiClient;

use MyCLabs\Enum\Enum;
use Sendsay\ApiClient\DTO\AbstractDTO;
use Sendsay\ApiClient\DTO\SendEmailSendWhenData;
use Sendsay\ApiClient\entity\AbstractEntity;
use Sendsay\ApiClient\enum\IssueSendWhen;
use Sendsay\ApiClient\exception\ApiResponseErrorException;
use Sendsay\ApiClient\exception\CommonErrorException;

class helpers
{
    public static function configure(object $object, array $options)
    {

        $reflection = new \ReflectionClass($object);
        foreach ($options as $propName => $propValue) {
            $propName = str_replace('.', '_', $propName);
            if (substr($propName,0,1) == '-') {
                $propName = substr_replace($propName, '_',0,1);
            }

            $setterName = 'set' . ucfirst($propName);
            if (method_exists($object, $setterName)) {
                $object->$setterName($propValue);
                continue;
            }
            try {
                $property = $reflection->getProperty($propName);
            } catch (\ReflectionException $e) {
                continue;
            }

            $property->setAccessible(true);
            $propType = $property->getType();
            $propertyType = $propType !== NULL ? $propType->getName() : NULL;

            try {
                $subRef = new \ReflectionClass($propertyType);
            } catch (\Exception $e) {
                $subRef = NULL;
            }

            if (!$subRef) {
                $theVal = $propValue;
            } elseif ($subRef->isSubclassOf(Enum::class)) {
                $theVal = $propertyType::from($propValue);
            } elseif (
                (
                    $subRef->isSubclassOf(AbstractDTO::class)
                    ||
                    $subRef->isSubclassOf(AbstractEntity::class)
                )
                &&
                is_array($propValue)
            ) {
                $theVal = new $propertyType($propValue);
            } else {
                $theVal = $propValue;
            }
            $property->setValue($object, $theVal);

//            $object->$propName = $propValue;
        }
        return $object;
    }

    public static function countriesList(): array
    {
        return [
            '004' => 'АФГАНИСТАН',
            '008' => 'АЛБАНИЯ',
            '010' => 'АНТАРКТИДА',
            '012' => 'АЛЖИР',
            '016' => 'АМЕРИКАНСКОЕ САМОА',
            '020' => 'АНДОРРА',
            '024' => 'АНГОЛА',
            '028' => 'АНТИГУА И БАРБУДА',
            '031' => 'АЗЕРБАЙДЖАН',
            '032' => 'АРГЕНТИНА',
            '036' => 'АВСТРАЛИЯ',
            '040' => 'АВСТРИЯ',
            '044' => 'БАГАМЫ',
            '048' => 'БАХРЕЙН',
            '050' => 'БАНГЛАДЕШ',
            '051' => 'АРМЕНИЯ',
            '052' => 'БАРБАДОС',
            '056' => 'БЕЛЬГИЯ',
            '060' => 'БЕРМУДЫ',
            '064' => 'БУТАН',
            '068' => 'БОЛИВИЯ, МНОГОНАЦИОНАЛЬНОЕ ГОСУДАРСТВО',
            '070' => 'БОСНИЯ И ГЕРЦЕГОВИНА',
            '072' => 'БОТСВАНА',
            '074' => 'ОСТРОВ БУВЕ',
            '076' => 'БРАЗИЛИЯ',
            '084' => 'БЕЛИЗ',
            '086' => 'БРИТАНСКАЯ ТЕРРИТОРИЯ В ИНДИЙСКОМ ОКЕАНЕ',
            '090' => 'СОЛОМОНОВЫ ОСТРОВА',
            '092' => 'ВИРГИНСКИЕ ОСТРОВА (БРИТАНСКИЕ)',
            '096' => 'БРУНЕЙ-ДАРУССАЛАМ',
            '100' => 'БОЛГАРИЯ',
            '104' => 'МЬЯНМА',
            '108' => 'БУРУНДИ',
            '112' => 'БЕЛАРУСЬ',
            '116' => 'КАМБОДЖА',
            '120' => 'КАМЕРУН',
            '124' => 'КАНАДА',
            '132' => 'КАБО-ВЕРДЕ',
            '136' => 'ОСТРОВА КАЙМАН',
            '140' => 'ЦЕНТРАЛЬНО-АФРИКАНСКАЯ РЕСПУБЛИКА',
            '144' => 'ШРИ-ЛАНКА',
            '148' => 'ЧАД',
            '152' => 'ЧИЛИ',
            '156' => 'КИТАЙ',
            '158' => 'ТАЙВАНЬ (КИТАЙ)',
            '162' => 'ОСТРОВ РОЖДЕСТВА',
            '166' => 'КОКОСОВЫЕ (КИЛИНГ) ОСТРОВА',
            '170' => 'КОЛУМБИЯ',
            '174' => 'КОМОРЫ',
            '175' => 'МАЙОТТА',
            '178' => 'КОНГО',
            '180' => 'КОНГО, ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА',
            '184' => 'ОСТРОВА КУКА',
            '188' => 'КОСТА-РИКА',
            '191' => 'ХОРВАТИЯ',
            '192' => 'КУБА',
            '196' => 'КИПР',
            '203' => 'ЧЕХИЯ',
            '204' => 'БЕНИН',
            '208' => 'ДАНИЯ',
            '212' => 'ДОМИНИКА',
            '214' => 'ДОМИНИКАНСКАЯ РЕСПУБЛИКА',
            '218' => 'ЭКВАДОР',
            '222' => 'ЭЛЬ-САЛЬВАДОР',
            '226' => 'ЭКВАТОРИАЛЬНАЯ ГВИНЕЯ',
            '231' => 'ЭФИОПИЯ',
            '232' => 'ЭРИТРЕЯ',
            '233' => 'ЭСТОНИЯ',
            '234' => 'ФАРЕРСКИЕ ОСТРОВА',
            '238' => 'ФОЛКЛЕНДСКИЕ ОСТРОВА (МАЛЬВИНСКИЕ)',
            '239' => 'ЮЖНАЯ ДЖОРДЖИЯ И ЮЖНЫЕ САНДВИЧЕВЫ ОСТРОВА',
            '242' => 'ФИДЖИ',
            '246' => 'ФИНЛЯНДИЯ',
            '248' => 'ЭЛАНДСКИЕ ОСТРОВА',
            '250' => 'ФРАНЦИЯ',
            '254' => 'ФРАНЦУЗСКАЯ ГВИАНА',
            '258' => 'ФРАНЦУЗСКАЯ ПОЛИНЕЗИЯ',
            '260' => 'ФРАНЦУЗСКИЕ ЮЖНЫЕ ТЕРРИТОРИИ',
            '262' => 'ДЖИБУТИ',
            '266' => 'ГАБОН',
            '268' => 'ГРУЗИЯ',
            '270' => 'ГАМБИЯ',
            '275' => 'ПАЛЕСТИНА, ГОСУДАРСТВО',
            '276' => 'ГЕРМАНИЯ',
            '288' => 'ГАНА',
            '292' => 'ГИБРАЛТАР',
            '296' => 'КИРИБАТИ',
            '300' => 'ГРЕЦИЯ',
            '304' => 'ГРЕНЛАНДИЯ',
            '308' => 'ГРЕНАДА',
            '312' => 'ГВАДЕЛУПА',
            '316' => 'ГУАМ',
            '320' => 'ГВАТЕМАЛА',
            '324' => 'ГВИНЕЯ',
            '328' => 'ГАЙАНА',
            '332' => 'ГАИТИ',
            '334' => 'ОСТРОВ ХЕРД И ОСТРОВА МАКДОНАЛЬД',
            '336' => 'ПАПСКИЙ ПРЕСТОЛ (ГОСУДАРСТВО - ГОРОД ВАТИКАН)',
            '340' => 'ГОНДУРАС',
            '344' => 'ГОНКОНГ',
            '348' => 'ВЕНГРИЯ',
            '352' => 'ИСЛАНДИЯ',
            '356' => 'ИНДИЯ',
            '360' => 'ИНДОНЕЗИЯ',
            '364' => 'ИРАН (ИСЛАМСКАЯ РЕСПУБЛИКА)',
            '368' => 'ИРАК',
            '372' => 'ИРЛАНДИЯ',
            '376' => 'ИЗРАИЛЬ',
            '380' => 'ИТАЛИЯ',
            '384' => 'КОТ Д\'ИВУАР',
            '388' => 'ЯМАЙКА',
            '392' => 'ЯПОНИЯ',
            '398' => 'КАЗАХСТАН',
            '400' => 'ИОРДАНИЯ',
            '404' => 'КЕНИЯ',
            '408' => 'КОРЕЯ, НАРОДНО-ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА',
            '410' => 'КОРЕЯ, РЕСПУБЛИКА',
            '414' => 'КУВЕЙТ',
            '417' => 'КИРГИЗИЯ',
            '418' => 'ЛАОССКАЯ НАРОДНО-ДЕМОКРАТИЧЕСКАЯ РЕСПУБЛИКА',
            '422' => 'ЛИВАН',
            '426' => 'ЛЕСОТО',
            '428' => 'ЛАТВИЯ',
            '430' => 'ЛИБЕРИЯ',
            '434' => 'ЛИВИЯ',
            '438' => 'ЛИХТЕНШТЕЙН',
            '440' => 'ЛИТВА',
            '442' => 'ЛЮКСЕМБУРГ',
            '446' => 'МАКАО',
            '450' => 'МАДАГАСКАР',
            '454' => 'МАЛАВИ',
            '458' => 'МАЛАЙЗИЯ',
            '462' => 'МАЛЬДИВЫ',
            '466' => 'МАЛИ',
            '470' => 'МАЛЬТА',
            '474' => 'МАРТИНИКА',
            '478' => 'МАВРИТАНИЯ',
            '480' => 'МАВРИКИЙ',
            '484' => 'МЕКСИКА',
            '492' => 'МОНАКО',
            '496' => 'МОНГОЛИЯ',
            '498' => 'МОЛДОВА, РЕСПУБЛИКА',
            '499' => 'ЧЕРНОГОРИЯ',
            '500' => 'МОНТСЕРРАТ',
            '504' => 'МАРОККО',
            '508' => 'МОЗАМБИК',
            '512' => 'ОМАН',
            '516' => 'НАМИБИЯ',
            '520' => 'НАУРУ',
            '524' => 'НЕПАЛ',
            '528' => 'НИДЕРЛАНДЫ',
            '531' => 'КЮРАСАО',
            '533' => 'АРУБА',
            '534' => 'СЕН-МАРТЕН (нидерландская часть)',
            '535' => 'БОНЭЙР, СИНТ-ЭСТАТИУС И САБА',
            '540' => 'НОВАЯ КАЛЕДОНИЯ',
            '548' => 'ВАНУАТУ',
            '554' => 'НОВАЯ ЗЕЛАНДИЯ',
            '558' => 'НИКАРАГУА',
            '562' => 'НИГЕР',
            '566' => 'НИГЕРИЯ',
            '570' => 'НИУЭ',
            '574' => 'ОСТРОВ НОРФОЛК',
            '578' => 'НОРВЕГИЯ',
            '580' => 'СЕВЕРНЫЕ МАРИАНСКИЕ ОСТРОВА',
            '581' => 'МАЛЫЕ ТИХООКЕАНСКИЕ ОТДАЛЕННЫЕ ОСТРОВА СОЕДИНЕННЫХ ШТАТОВ',
            '583' => 'МИКРОНЕЗИЯ, ФЕДЕРАТИВНЫЕ ШТАТЫ',
            '584' => 'МАРШАЛЛОВЫ ОСТРОВА',
            '585' => 'ПАЛАУ',
            '586' => 'ПАКИСТАН',
            '591' => 'ПАНАМА',
            '598' => 'ПАПУА-НОВАЯ ГВИНЕЯ',
            '600' => 'ПАРАГВАЙ',
            '604' => 'ПЕРУ',
            '608' => 'ФИЛИППИНЫ',
            '612' => 'ПИТКЕРН',
            '616' => 'ПОЛЬША',
            '620' => 'ПОРТУГАЛИЯ',
            '624' => 'ГВИНЕЯ-БИСАУ',
            '626' => 'ТИМОР-ЛЕСТЕ',
            '630' => 'ПУЭРТО-РИКО',
            '634' => 'КАТАР',
            '638' => 'РЕЮНЬОН',
            '642' => 'РУМЫНИЯ',
            '643' => 'РОССИЯ',
            '646' => 'РУАНДА',
            '652' => 'СЕН-БАРТЕЛЕМИ',
            '654' => 'СВЯТАЯ ЕЛЕНА, ОСТРОВ ВОЗНЕСЕНИЯ, ТРИСТАН-ДА-КУНЬЯ',
            '659' => 'СЕНТ-КИТС И НЕВИС',
            '660' => 'АНГИЛЬЯ',
            '662' => 'СЕНТ-ЛЮСИЯ',
            '663' => 'СЕН-МАРТЕН (французская часть) ',
            '666' => 'СЕН-ПЬЕР И МИКЕЛОН',
            '670' => 'СЕНТ-ВИНСЕНТ И ГРЕНАДИНЫ',
            '674' => 'САН-МАРИНО',
            '678' => 'САН-ТОМЕ И ПРИНСИПИ',
            '682' => 'САУДОВСКАЯ АРАВИЯ',
            '686' => 'СЕНЕГАЛ',
            '688' => 'СЕРБИЯ',
            '690' => 'СЕЙШЕЛЫ',
            '694' => 'СЬЕРРА-ЛЕОНЕ',
            '702' => 'СИНГАПУР',
            '703' => 'СЛОВАКИЯ',
            '704' => 'ВЬЕТНАМ',
            '705' => 'СЛОВЕНИЯ',
            '706' => 'СОМАЛИ',
            '710' => 'ЮЖНАЯ АФРИКА',
            '716' => 'ЗИМБАБВЕ',
            '724' => 'ИСПАНИЯ',
            '728' => 'ЮЖНЫЙ СУДАН',
            '729' => 'СУДАН',
            '732' => 'ЗАПАДНАЯ САХАРА',
            '740' => 'СУРИНАМ',
            '744' => 'ШПИЦБЕРГЕН И ЯН МАЙЕН',
            '748' => 'ЭСВАТИНИ',
            '752' => 'ШВЕЦИЯ',
            '756' => 'ШВЕЙЦАРИЯ',
            '760' => 'СИРИЙСКАЯ АРАБСКАЯ РЕСПУБЛИКА',
            '762' => 'ТАДЖИКИСТАН',
            '764' => 'ТАИЛАНД',
            '768' => 'ТОГО',
            '772' => 'ТОКЕЛАУ',
            '776' => 'ТОНГА',
            '780' => 'ТРИНИДАД И ТОБАГО',
            '784' => 'ОБЪЕДИНЕННЫЕ АРАБСКИЕ ЭМИРАТЫ',
            '788' => 'ТУНИС',
            '792' => 'ТУРЦИЯ',
            '795' => 'ТУРКМЕНИСТАН',
            '796' => 'ОСТРОВА ТЕРКС И КАЙКОС',
            '798' => 'ТУВАЛУ',
            '800' => 'УГАНДА',
            '804' => 'УКРАИНА',
            '807' => 'СЕВЕРНАЯ МАКЕДОНИЯ',
            '818' => 'ЕГИПЕТ',
            '826' => 'СОЕДИНЕННОЕ КОРОЛЕВСТВО ВЕЛИКОБРИТАНИИ И СЕВЕРНОЙ ИРЛАНДИИ',
            '831' => 'ГЕРНСИ',
            '832' => 'ДЖЕРСИ',
            '833' => 'ОСТРОВ МЭН',
            '834' => 'ТАНЗАНИЯ, ОБЪЕДИНЕННАЯ РЕСПУБЛИКА',
            '840' => 'СОЕДИНЕННЫЕ ШТАТЫ',
            '850' => 'ВИРГИНСКИЕ ОСТРОВА (США)',
            '854' => 'БУРКИНА-ФАСО',
            '858' => 'УРУГВАЙ',
            '860' => 'УЗБЕКИСТАН',
            '862' => 'ВЕНЕСУЭЛА (БОЛИВАРИАНСКАЯ РЕСПУБЛИКА)',
            '876' => 'УОЛЛИС И ФУТУНА',
            '882' => 'САМОА',
            '887' => 'ЙЕМЕН',
            '894' => 'ЗАМБИЯ',
            '895' => 'АБХАЗИЯ',
            '896' => 'ЮЖНАЯ ОСЕТИЯ',
            '897' => 'ДНР',
            '898' => 'ЛНР'
        ];
    }
    public static function getCountryName(int $code): string
    {
        $query = strval($code);

        if (strlen($query) < 3) {
            $query = str_pad($query, 3, '0', STR_PAD_LEFT);
        }
        $found = array_filter(self::countriesList(), function ($item) use ($query) { return $item == $query; }, ARRAY_FILTER_USE_KEY);

        if (empty($found)) {
            return '';
        }
        $k = array_keys($found);
        return $found[$k[0]];
    }
    public static function getCountryCode(string $query): string
    {
        $query = trim($query);
        if (is_numeric($query)) {
            if (strlen($query) < 3) {
                $query = str_pad($query, 3, '0', STR_PAD_LEFT);
            }
            $found = array_filter(self::countriesList(), function ($item) use ($query) { return $item == $query; }, ARRAY_FILTER_USE_KEY);
        } else {
            $query = mb_strtoupper($query);
            $found = array_filter(self::countriesList(), function ($item) use ($query) { return $item == $query; });
        }
        if (empty($found)) {
            return '';
        }
        $k = array_keys($found);
        return $k[0];
    }

    public static function arrayIsList(array $arr): bool
    {
        if ($arr === []) {
            return true;
        }
        return array_keys($arr) === range(0, count($arr) - 1);
    }

    public static function prepareArrayForRequest(array &$arr): void
    {
        foreach ($arr as $key => &$value) {
            if (is_array($value)) {
                if (self::arrayIsList($value)) {
                    continue;
                }
                $hasNullValuesOnly = count(array_filter(array_values($value), function ($i) { return $i !== NULL; })) == 0;
                if ($hasNullValuesOnly) {
                    $newV = array_keys($value);
                    $arr[$key] = $newV;
//                    $value = $newV;
                    continue;
                }
                self::prepareArrayForRequest($value);
            }
        }
    }
    
    /**
     * Подготовка массива данных для вставки в параметры запроса к Апи, относящихся к моменту отправки рассылки.
     * Параметр sendwhen и возможные сопутствующие later.time и delay.time
     * @param SendEmailSendWhenData $sendWhenData
     * @return array
     */
    public static function processIssueSendWhen(SendEmailSendWhenData $sendWhenData): array
    {
        $sendWhen = $sendWhenData->sendwhen;
        $res = ['sendwhen' => $sendWhen->getValue()];
        
        if ($sendWhen == 'later') {
            if (empty($sendWhenData->sendAt)) {
                $message = 'Invalid Data in ' . self::class . ' method ' . __FUNCTION__ . '.';
                $message .= ' When parameter SENDWHEN is "later" the concrete dateTime must be supplied.';
                throw new CommonErrorException($message);
            } else {
                $res['later.time'] = $sendWhenData->sendAt->format('Y-m-d H:i');
                return $res;
            }
        }
        if ($sendWhen == 'delay') {
            if (empty($sendWhenData->delayMinutes)) {
                $message = 'Invalid Data in ' . self::class . ' method ' . __FUNCTION__ . '.';
                $message .= ' When parameter SENDWHEN is "delay" the delay in minutes must be supplied. ';
                throw new CommonErrorException($message);
            } else {
                $res['delay.time'] = $sendWhenData->delayMinutes;
            }
        }
        return $res;
    }
}