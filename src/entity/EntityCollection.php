<?php

namespace Sendsay\ApiClient\entity;

use Laminas\Validator\ValidatorChain;
use Laminas\Validator\ValidatorInterface;
use Sendsay\ApiClient\validator\validatorsCollectionI;

/**
 * @property EntityI[] $entities
 */
class EntityCollection implements EntityCollectionI, ArrayableI
{
    protected array $entities = [];
    protected bool $isLastPage = false;

    public function __construct(array $entities, bool $isLastPage = false)
    {
//        $this->entities = $entities;
        $this->entities = array_filter($entities, function ($item) { return $item instanceof EntityI; });
        $this->isLastPage = $isLastPage;
    }

    public function key(): string
    {
        return key($this->entities);
    }
    public function count(): int
    {
        return count($this->entities);
    }

    public function next(): void
    {
        next($this->entities);
    }

    public function current(): EntityI
    {
        return current($this->entities);
    }

    public function rewind(): void
    {
        reset($this->entities);
    }

    public function valid(): bool
    {
        return key($this->entities) !== NULL;
    }

    public function append(EntityI $entity): EntityCollection
    {
        $this->entities[] = $entity;
        return $this;
    }

    public function isLastPage(): bool
    {
        return $this->isLastPage;
    }

    public function toArray(): array
    {
        $result = ['isLastPage' => $this->isLastPage];
        foreach ($this->entities as $key => $entity) {
            $result[$key] = $entity->toArray();
        }
        return $result;
    }

}