<?php

namespace Sendsay\ApiClient\entity;

use Laminas\Validator\NotEmpty;
use Laminas\Validator\ValidatorChain;
use Sendsay\ApiClient\enum\AddrType;
use Sendsay\ApiClient\enum\GroupType;
use Sendsay\ApiClient\validator\InEnumValidator;
use Sendsay\ApiClient\validator\ValidatorCollection;

/**
 * @property string $id
 * @property string $name
 * @property string $type
 * @property string $addr_type
 * @property array $filter
 * @property int $protected
 * @property int $expimp
 * @property \DateTime $create_time
 * @property \DateTime $update_time
 * @property mixed $dictnode
 * @property int $reltype
 * @property int $relref
 */
class GroupList extends AbstractEntity
{
    const READABLE_PROPS = [
        'id', 'name', 'type', 'addr_type', 'filter', 'protected', 'expimp', 'create_time', 'update_time', 'dictnode', 'reltype', 'relref', 'status'
    ];
    private ?string $id = NULL;
    private ?string $name = NULL;
    private ?string $type = 'list';
    private string $addr_type = 'email';
    private array $filter = [];
    private int $protected = 0;
    private int $expimp = 0;
    private ?\DateTime $create_time = NULL;
    private ?\DateTime $update_time = NULL;
    private $dictnode = NULL;
    private int $reltype = 0;
    private int $relref = 0;
    private int $status = 1;

    private function validatorsOptions(): array
    {
        return [
            'id' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'List ID cannot be empty']]],
            ],
            'name' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'List Name cannot be empty']]],
            ],
            'type' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'List Type cannot be empty']]],
                ['class' => InEnumValidator::class,
                    'options' => [
                        'messages' => [InEnumValidator::IS_OUT => "'%value%' is unacceptable value for list type"],
                        'enum' => new  GroupType('list')
                    ]
                ],
            ],
            'addr_type' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'List address type cannot be empty']]],
                ['class' => InEnumValidator::class,
                    'options' => [
                        'messages' => [InEnumValidator::IS_OUT => "'%value%' is unacceptable value for list address type"],
                        'enum' => new  AddrType('email')
                    ]
                ],
            ]
        ];
    }

    private array $validationErrors = [];

    private ValidatorCollection $validators;

    public function __construct(?array $options)
    {
        parent::__construct($options);
        $this->init();
    }

    public function init($params = NULL): void
    {
        $this->setValidators($this->createValidatorsFromOptionsArray($this->validatorsOptions()));
        return;
    }

    public function getEntityName(): string
    {
        return 'GroupList';
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }
    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }
    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function setType(string $v): void
    {
        try {
            $enum = GroupType::from($v);
            $this->type = $enum->getValue();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function setAddr_type(string $v): void
    {
        try {
            $enum = AddrType::from($v);
            $this->addr_type = $enum->getValue();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function setCreate_time($v): void
    {
        if (!is_integer($v) && !is_string($v)) {
            throw new \Exception('Illegal create time value type');
        }

        if (is_string($v)) {
            $timestamp = strtotime($v);
        } elseif (is_integer($v)) {
            $timestamp = $v;
        }

        $d = new \DateTime();
        $d->setTimestamp($timestamp);
        $this->create_time = $d;
    }
    public function setUpdate_time($v): void
    {
        if (!is_integer($v) && !is_string($v)) {
            throw new \Exception('Illegal create time value type');
        }

        if (is_string($v)) {
            $timestamp = strtotime($v);
        } elseif (is_integer($v)) {
            $timestamp = $v;
        }

        $d = new \DateTime();
        $d->setTimestamp($timestamp);
        $this->update_time = $d;
    }


    public function getReadableProps(): array
    {
        return self::READABLE_PROPS;
    }

    public function __get($name)
    {
        if ($name === 'create_time' && $this->create_time instanceof \DateTime) {
            return $this->create_time->format('Y-m-d H:i:s');
        }

        if ($name === 'update_time' && $this->update_time instanceof \DateTime) {
            return $this->update_time->format('Y-m-d H:i:s');
        }

        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name) && in_array($name, self::READABLE_PROPS)) {
            return $this->$name;
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setterName = 'set' . ucfirst($name);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } elseif (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Trying to set unknown property: ' . get_class($this) . '::' . $name);
        }
    }

}