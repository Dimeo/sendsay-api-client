<?php

namespace Sendsay\ApiClient\entity;

use Laminas\Validator\ValidatorChain;
use Laminas\Validator\ValidatorInterface;

interface EntityCollectionI extends \Iterator
{
    public function __construct(array $propsList, bool $isLastPage = false);
    public function current(): EntityI;
    public function key(): string;
    public function next(): void;
    public function append(EntityI $entity): EntityCollectionI;
    public function isLastPage(): bool;
}