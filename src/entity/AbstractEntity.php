<?php

namespace Sendsay\ApiClient\entity;

use Laminas\Validator\NotEmpty;
use MyCLabs\Enum\Enum;
use Sendsay\ApiClient\enum\AddrType;
use Sendsay\ApiClient\enum\GroupType;
use Sendsay\ApiClient\exception\EntityValidationErrorException;
use Sendsay\ApiClient\helpers;
use Sendsay\ApiClient\validator\InEnumValidator;
use Sendsay\ApiClient\validator\ValidatorCollection;

abstract class AbstractEntity implements EntityI, ArrayableI
{
    public function __construct(?array $options)
    {
        $options = empty($options) ? [] : $options;
        helpers::configure($this, $options);
    }

    abstract public function init($params=NULL): void;
    abstract public function getReadableProps(): array;
    abstract public function setValidators(ValidatorCollection $validators): void;
    abstract public function getValidators(): ValidatorCollection;
    abstract public function getValidationErrors(): array;
    abstract public function setValidationErrors(array $errorsList): void;
    abstract public function isValid(): bool;
    abstract public function getEntityName(): string;

    public function validate(): bool
    {
        $validationErrors = [];

        foreach ($this->getValidators() as $propName => $validator) {
            if (!property_exists($this, $propName) || !in_array($propName, $this->getReadableProps())) {
                continue;
            }
            if (!$validator->isValid($this->$propName)) {
                $errMessages = $validator->getMessages();
                foreach ($errMessages as $errMessage) {
                    $validationErrors[] = $errMessage;
                }
            }
        }
        $this->setValidationErrors($validationErrors);
        return empty($validationErrors);
    }

    public function createValidatorsFromOptionsArray(array $validatorsOptions): ValidatorCollection
    {
        $validatedPropsList = array_keys($validatorsOptions);
        $validators = new ValidatorCollection($validatedPropsList);

        foreach ($validatorsOptions as $propName => $validatorsConfigs) {
            if (!is_array($validatorsConfigs)) {
                continue;
            }
            foreach ($validatorsConfigs as $validatorConfig) {
                if (!is_array($validatorConfig)) {
                    continue;
                }
                if (!isset($validatorConfig['class'])) {
                    throw new \Exception('key "class" is required to build validator from array');
                }
                $validatorClassName = $validatorConfig['class'];
                $validatorOptions = $validatorConfig['options'] ?? [];
                try {
                    $validator = new $validatorClassName($validatorOptions);
                    $validators->append($propName, $validator);
                } catch (\Exception $e) {
                    continue;
                }
            }
        }
        return $validators;
    }

    public function toArray(): array
    {
        if (!$this->validate()) {
            throw new EntityValidationErrorException($this);
        }
        $propsList = $this->getReadableProps();

        $result = [];
        foreach ($propsList as $propName) {
            $arrKey = $propName;
            // Если есть ведущее подчеркивание - заменить на минус
            // Все остальные подчеркивания (если есть) заменить на точки
            if (substr($arrKey,0,1) == '_') {
                $arrKey = substr_replace($arrKey, '-',0,1);
            }
            $arrKey = str_replace('_','.',$arrKey);

            if ($this->$propName instanceof ArrayableI) {
                $v = $this->$propName->toArray();
            } elseif ($this->$propName instanceof Enum) {
                $v = $this->$propName->getValue();
            } elseif (NULL === $this->$propName) {
              continue;
            } else {
                $v = $this->$propName;
            }
            try {
                $result[$arrKey] = $v;
            } catch (\Exception $e) {
                $result[$arrKey] = NULL;
            }
        }
        return $result;
    }
}