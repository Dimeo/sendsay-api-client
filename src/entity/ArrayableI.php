<?php

namespace Sendsay\ApiClient\entity;

use Sendsay\ApiClient\validator\ValidatorCollection;

interface ArrayableI
{

    public function toArray(): array;

}