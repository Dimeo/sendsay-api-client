<?php

namespace Sendsay\ApiClient\entity\letter;

use Laminas\Validator\NotEmpty;
use Sendsay\ApiClient\DTO\message\ViberMessage;
use Sendsay\ApiClient\DTO\message\VkMessage;
use Sendsay\ApiClient\validator\ValidatorCollection;

/**
 * @property string $subject тема. Обязательно
 * @property VkMessage $message
 * @property int $draft_id "номер черновика, содержимое которого даст выпуск" -- должен иметь vk-версию
 */
class VkLetter extends AbstractLetter
{

    private string $subject;

    private ?VkMessage $message = NULL;

    private ?int $draft_id = NULL;

    private ValidatorCollection $validators;
    private array $validationErrors = [];


    public function __construct(?array $options)
    {
        $message = $options['message'] ?? [];
        if (is_array($message)) {
            $options['message'] = new VkMessage($message);
        }

        parent::__construct($options);
        $this->init();
    }

    public function init($params = NULL): void
    {
        $validatorsOptions = $this->validatorOptions();
        if (!empty($validatorsOptions)) {
            $this->setValidators($this->createValidatorsFromOptionsArray($validatorsOptions));
        } else {
            $this->setValidators(new ValidatorCollection([]));
        }
        return;
    }

    public function getReadableProps(): array
    {
        return ['subject','draft_id','message'];
    }

    private function validatorOptions(): array
    {
        return [
            'subject' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'from-name property of SMS message cannot be empty']]],
            ]
        ];
    }
    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getEntityName(): string
    {
        return 'VkLetter';
    }

    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name) && in_array($name, $this->getReadableProps())) {
            return $this->$name;
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setterName = 'set' . ucfirst($name);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } elseif (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Trying to set unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function validate(): bool
    {
        $isEnoughData = true;
        if (NULL == $this->message && NULL === $this->draft_id ) {
            $isEnoughData = false;
            $this->validationErrors[] = 'Unacceptable. Not enough data. Both "message" and "draft_id" are empty in ' . self::class;
        }
        return parent::validate() && $isEnoughData;
    }
}