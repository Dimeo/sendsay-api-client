<?php

namespace Sendsay\ApiClient\entity\letter;

use Laminas\Validator\EmailAddress;
use Laminas\Validator\NotEmpty;
use Sendsay\ApiClient\DTO\collection\AttachesCollection;
use Sendsay\ApiClient\DTO\Message;
use Sendsay\ApiClient\DTO\message\SmsMessage;
use Sendsay\ApiClient\validator\ValidatorCollection;

/**
 * @property string $from_name имя отправителя должно быть в списке уже промодерированных имён
 * @property SmsMessage $message номер черновика содержимое которого даст выпуск. должен иметь sms версию
 * @property int $draft_id
 */
class SmsLetter extends AbstractLetter
{
    private ?string $from_name = NULL;
    private ?SmsMessage $message = NULL;

    /**
     * "номер черновика содержимое которого даст выпуск"
     * должен иметь sms версию
     */
    private ?int $draft_id = NULL;

    private ValidatorCollection $validators;
    private array $validationErrors = [];


    public function __construct(?array $options)
    {
        $message = $options['message'] ?? [];
        if (is_array($message)) {
            $options['message'] = new SmsMessage($message);
        }

        parent::__construct($options);
        $this->init();
    }

    public function init($params = NULL): void
    {
        $validatorsOptions = $this->validatorOptions();
        if (!empty($validatorsOptions)) {
            $this->setValidators($this->createValidatorsFromOptionsArray($validatorsOptions));
        } else {
            $this->setValidators(new ValidatorCollection([]));
        }
        return;
    }

    public function getReadableProps(): array
    {
        return ['from_name','message','draft_id'];
    }

    private function validatorOptions(): array
    {
        return [
            'from_name' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'from-name property of SMS message cannot be empty']]],
            ],
        ];
    }
    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getEntityName(): string
    {
        return 'SmsLetter';
    }

    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name) && in_array($name, $this->getReadableProps())) {
            return $this->$name;
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setterName = 'set' . ucfirst($name);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } elseif (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Trying to set unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function validate(): bool
    {
        $isEnoughData = true;
        if (NULL == $this->message && NULL === $this->draft_id ) {
            $isEnoughData = false;
            $this->validationErrors[] = 'Unacceptable. Not enough data. Both "message" and "draft_id" are empty in ' . self::class;
        }
        return parent::validate() && $isEnoughData;
    }
}