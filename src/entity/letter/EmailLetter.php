<?php

namespace Sendsay\ApiClient\entity\letter;

use Laminas\Validator\EmailAddress;
use Laminas\Validator\NotEmpty;
use Sendsay\ApiClient\DTO\Attach;
use Sendsay\ApiClient\DTO\collection\AttachesCollection;
use Sendsay\ApiClient\DTO\Message;
use Sendsay\ApiClient\DTO\message\EmailMessage;
use Sendsay\ApiClient\entity\AbstractEntity;
use Sendsay\ApiClient\enum\AddrType;
use Sendsay\ApiClient\enum\GroupType;
use Sendsay\ApiClient\helpers;
use Sendsay\ApiClient\validator\InEnumValidator;
use Sendsay\ApiClient\validator\ValidatorCollection;

/**
 * @property string $subject
 * @property string $from_name
 * @property string $from_email
 * @property string $reply_name
 * @property string $reply_email
 * @property string $to_name
 * @property EmailMessage $message
 * @property string $draft_id в качестве идентификатора может использоваться не только целочисленный ID, но и уникальный текстовый псевдоним черновика
 * @property int $autotext генерация текстовой версии из html
 * @property ?AttachesCollection $attaches
 *
 */
class EmailLetter extends AbstractLetter
{
    private ?string $subject = NULL;
    private ?string $from_name = NULL;

    /**
     * адрес должен быть в списке уже подтверждённых адресов
     */
    private ?string $from_email = NULL;
    private ?string $reply_name = NULL;

    /**
     * Обратный адрес для ответа (email)"
     * адрес должен быть в списке уже подтверждённых адресов
     */
    private ?string $reply_email = NULL;

    /**
     * "Имя получателя"
     * В этом поле уместна персонализация для подстановки имени и фамилии получателя
     */
    private ?string $to_name = NULL;
    private ?EmailMessage $message = NULL;

    /**
     * "номер черновика содержимое которого даст выпуск"
     * должен иметь html и/или текстовую версию. Можно указывать alias или id
     */
    private ?string $draft_id = NULL;
    
    /**
     * недокументированный параметр указывающий на черновик
     * кот. будет использоваться для группировки в статистике транзакционных
     * писем, отправленных без черновика
     */
    private ?int $basedraft_id = NULL;

    /**
     * генерация текстовой версии из html
     * 0 - откл.
     * 1 - Вкл с шириной строки 80 симв.
     * Др.числ.зн. - вкл. ширина строки = указанное число символов
     */
    private int $autotext = 0;

    private ?AttachesCollection $attaches = NULL;

    private ValidatorCollection $validators;
    private array $validationErrors = [];


    public function __construct(?array $options)
    {
        $message = $options['message'] ?? NULL;
        if (is_array($message)) {
            $options['message'] = new EmailMessage($message);
        }

        if (isset($options['attaches']) && is_array($options['attaches'])) {
            $options['attaches'] =  AttachesCollection::createFromArray($options['attaches']);
        }

        parent::__construct($options);
        $this->init();
    }

    public function init($params = NULL): void
    {
        $validatorsOptions = $this->validatorOptions();
        if (!empty($validatorsOptions)) {
            $this->setValidators($this->createValidatorsFromOptionsArray($validatorsOptions));
        } else {
            $this->setValidators(new ValidatorCollection([]));
        }
        return;
    }

    public function getReadableProps(): array
    {
        return ['subject','from_name','from_email','reply_name','reply_email','to_name','message','draft_id','basedraft_id','autotext','attaches'];
    }

    private function validatorOptions(): array
    {
        return [
//            'subject' => [
//                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'Subject of Email must be NOT empty']]],
//            ],
            'from_email' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'from-email property of email message cannot be empty']]],
                ['class' => EmailAddress::class, 'options' => []],
            ],
            'replay_email' => [
                ['class' => EmailAddress::class, 'options' => []],
            ],
        ];
    }
    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getEntityName(): string
    {
        return 'EmailLetter';
    }

    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name) && in_array($name, $this->getReadableProps())) {
            return $this->$name;
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setterName = 'set' . ucfirst($name);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } elseif (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Trying to set unknown property: ' . get_class($this) . '::' . $name);
        }
    }
    public function validate(): bool
    {
        $parentPass = parent::validate();
        $dataIsOk = true;
        if (NULL == $this->message && NULL === $this->draft_id ) {
            $dataIsOk = false;
            $this->validationErrors[] = 'Unacceptable. Not enough data. Both "message" and "draft_id" are empty in ' . self::class;
        }
        if (NULL !== $this->message && NULL !== $this->draft_id ) {
            $dataIsOk = false;
            $this->validationErrors[] = 'Unacceptable. Extra data. Something one of "message" OR "draft_id" can be set only in ' . self::class . ' but both are set.';
        }

        if (NULL === $this->subject && NULL === $this->draft_id ) {
            $dataIsOk = false;
            $this->validationErrors[] = 'Unacceptable. When prop "draft_id" is empty prop "subject" is required in ' . self::class . ' but both are NULL.';
        }
        return $parentPass && $dataIsOk;
    }
}