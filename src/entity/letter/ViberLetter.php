<?php

namespace Sendsay\ApiClient\entity\letter;

use Laminas\Validator\NotEmpty;
use Sendsay\ApiClient\DTO\collection\AttachesCollection;
use Sendsay\ApiClient\DTO\message\SmsMessage;
use Sendsay\ApiClient\DTO\message\ViberMessage;
use Sendsay\ApiClient\validator\ValidatorCollection;

/**
 * @property string $button_url параметр обязателен, если нужна кнопка после сообщения
 * @property string $button_text "надпись на кнопке" -- параметр необязателен, по умолчанию "Узнать"
 * @property ViberMessage $message
 * @property int $draft_id "номер черновика, содержимое которого даст выпуск" -- должен иметь viber-версию
 */
class ViberLetter extends AbstractLetter
{

    private ?string $button_url = NULL;
    private ?string $button_text = NULL;
    private ?string $from_name = NULL;

//    private ?array $attaches = NULL; // не сейчас... может когда нибудь позже
    private ?ViberMessage $message = NULL;

    /**
     * "номер черновика содержимое которого даст выпуск"
     * должен иметь sms версию
     */
    private ?int $draft_id = NULL;

    private ValidatorCollection $validators;
    private array $validationErrors = [];


    public function __construct(?array $options)
    {
        $message = $options['message'] ?? [];
        if (is_array($message)) {
            $options['message'] = new ViberMessage($message);
        }

        parent::__construct($options);
        $this->init();
    }

    public function init($params = NULL): void
    {
        $validatorsOptions = $this->validatorOptions();
        if (!empty($validatorsOptions)) {
            $this->setValidators($this->createValidatorsFromOptionsArray($validatorsOptions));
        } else {
            $this->setValidators(new ValidatorCollection([]));
        }
        return;
    }

    public function getReadableProps(): array
    {
        return ['button_url','button_text','draft_id', 'message', 'from_name'];
    }

    private function validatorOptions(): array
    {
        return [];
    }
    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getEntityName(): string
    {
        return 'ViberLetter';
    }

    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name) && in_array($name, $this->getReadableProps())) {
            return $this->$name;
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setterName = 'set' . ucfirst($name);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } elseif (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Trying to set unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function validate(): bool
    {
        $isEnoughData = true;
        if (NULL == $this->message && NULL === $this->draft_id ) {
            $isEnoughData = false;
            $this->validationErrors[] = 'Unacceptable. Not enough data. Both "message" and "draft_id" are empty in ' . self::class;
        }
        return parent::validate() && $isEnoughData;
    }
}