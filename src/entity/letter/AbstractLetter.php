<?php

namespace Sendsay\ApiClient\entity\letter;

use Sendsay\ApiClient\validator\ValidatorCollection;

abstract class AbstractLetter extends \Sendsay\ApiClient\entity\AbstractEntity
{
    public static function createEmailLetter(array $data): EmailLetter
    {
        return new EmailLetter($data);
    }

    public static function createSmsLetter(array $data): SmsLetter
    {
        return new SmsLetter($data);
    }

    public static function createViberLetter(array $data): ViberLetter
    {
        return new ViberLetter($data);
    }

    public static function createVKLetter(array $data): VkLetter
    {
        return new VkLetter($data);
    }

}