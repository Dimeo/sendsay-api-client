<?php

namespace Sendsay\ApiClient\entity\issue;

use Laminas\Validator\NotEmpty;
use Sendsay\ApiClient\entity\letter\EmailLetter;
use Sendsay\ApiClient\enum\AddrType;
use Sendsay\ApiClient\validator\Regexp;
use Sendsay\ApiClient\DTO\ContactRate;
use Sendsay\ApiClient\DTO\TzBest;
use Sendsay\ApiClient\DTO\TzLimit;
use Sendsay\ApiClient\DTO\TzObservance;
use Sendsay\ApiClient\entity\AbstractEntity;
use Sendsay\ApiClient\entity\ArrayableI;
use Sendsay\ApiClient\entity\letter\AbstractLetter;
use Sendsay\ApiClient\enum\AccumulateBy;
use Sendsay\ApiClient\enum\UsersUrlRemove;
use Sendsay\ApiClient\validator\InEnumValidator;
use Sendsay\ApiClient\validator\ValidatorCollection;

/**
 * @property string $alias
 * @property array $name
 * @property int $reltype
 * @property int $relref
 * @property EmailLetter $letter
 * @property string $class_id
 * @property int $relink
 * @property array $relink_param
 * @property int $multiple
 * @property TzLimit $tz_limit
 * @property TzObservance $tz_observance
 * @property TzBest $tz_best
 * @property ContactRate $contact_rate
 * @property string $users_slice
 * @property bool $only_unique
 * @property string $link_qsid
 * @property array $campaign_id
 * @property string $dkim_id
 * @property array $extra
 * @property string $group
 * @property string $users_url
 * @property UsersUrlRemove $users_url_remove
 * @property string $issue_member_list
 * @property string $unsub_list
 * @property bool $ignore_stoplist
 * @property string $basegroup_id
 * @property bool $accumulate
 * @property AccumulateBy $accumulate_by
 */
class DraftToUpdate extends AbstractEntity
{
    const READABLE_PROPS = ['alias','name','reltype','relref','letter','class_id','relink','relink_param','multiple',
        'tz_limit','tz_observance','tz_best','contact_rate','users_slice','only_unique','link_qsid','campaign_id',
        'dkim_id','extra','group','users_url','users_url_remove','issue_member_list','unsub_list','ignore_stoplist',
        'basegroup_id','accumulate','accumulate_by'];

    // эти поля в dot notation при конвертации в массив переводить НЕ нужно. Они должны остаться как есть
    const UNDERSCORED_PROPS = ['tz_limit', 'tz_observance', 'tz_best', 'contact_rate', 'only_unique', 'issue_member_list', 'unsub_list', 'ignore_stoplist', 'accumulate_by'];

    // ---------------
    private ?string $alias = NULL; // альтернативный идентификатор. Не должен начинаться с цифры и не должен содержать пробелы. Может использоваться во всех местах где требуется указать код черновика
    private ?string $name = NULL; // "название черновика" -- обязательно при создании

    /** @link https://sendsay.ru/api/api.html#%D0%9F%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8C%D1%81%D0%BA%D0%B8%D0%B5-%D0%BC%D0%B5%D1%82%D0%BA%D0%B8-%D0%BE%D0%B1%D1%8A%D0%B5%D0%BA%D1%82%D0%BE%D0%B2  */
    private int $reltype = 0; // пользовательские метки объектов. Тип классификации. Только положительное число
    private int $relref = 0; // пользовательские метки объектов. Указатель на что-то внутри классификации. Только положительное число

//    private ?AbstractLetter $letter= NULL; // обязательно при создании
    private ?EmailLetter $letter= NULL; // обязательно при создании
    private ?string $class_id = NULL; // class.id
    private int $relink =  1; // преобразовывать ссылки для учета перехода по ним
    private array $relink_param = [ 'link' => 1, 'test' => 0 ]; // ссылки, оформленные тегом <a> преобразовывать, а существование адресов в ссылках НЕ проверять

    // 0 - не более одного письма. 1 - сколько совпадений столько и писем.
    //  Учёт множественных совпадений итератор в фильтре.
    // Игнорируется при issue.send.multi
    private int $multiple = 0;

    private ?TzLimit $tz_limit = NULL; // ограничение на частоту отправки сообщений

    private ?TzObservance $tz_observance = NULL; // Учёт часового пояса получателя

    private ?TzBest $tz_best = NULL; // Высылка письма в наиболее подходящее для каждого получателя время индивидуально на основе его прошлой активности

    private ?ContactRate $contact_rate = NULL; // Ограничение количества контактов на получателя

    /**
     * Ограничение на размер тиража
     * Если число - тираж составит не более "число" первых получателей (1-...)
     * Если с % - тираж составит указанный процент от всех получателей (1-100)
     */
    private ?string $users_slice = '100%'; //  игнорируется при issue.send.multi

    /**
     * Следить за уникальностью адресов в списке
     * 0 - повторно встреченные адреса участвуют в рассылке
     * 1 - повторно встреченные адреса исключаются из выпуска
     */
    private ?bool $only_unique = NULL; // 0|1.  игнорируется при issue.send.multi

    /**
     * параметр преобразования ссылок для отслеживания перехода по ним с помощью сторонних сервисов
     * добавляется ко всем ссылкам через query-string. Обычно содержит UTM метки.
     * Если к какой-то ссылке параметр добавлять не требуется, то укажите в теге ссылки в коде письма
     * параметр data-no-qsid со значением 1
     */
    private ?string $link_qsid = NULL; // значение должно быть уже url-encoded

    /**
     *  список идентификаторов компаний, куда включить при выпуске.
     *  Если нужно, чтобы при выпуске ничего ни в какую компанию не включалось, то указать [0]
     */
    private ?array $campaign_id = NULL; // "коды кампаний"

    /**
     * номер проверенного dkim-ключа
     * Значение ключа или 0.
     * Если 0, то будет использоваться общий системный ключ
     */
    private ?string $dkim_id = NULL;

    /**
     * Данные для подстановок в тексте и теме сообщения.
     * в коде сообщения подстановки указываются в виде [% KeyName.subKey.subSubKey %]
     * в массиве extra соответственно ['KeyName' => ['subKey' => ['subSubKey' => 'значение для подстановки']]]
     * Объединяется с extra выпуска, но имеет меньший приоритет
     */
    private ?array $extra = NULL;

    private ?string $group = NULL; // "код группы подписчиков" -- игнорируется при выпуске

    /**
     * URL, по которому находятся список адресов и данных для импортирования
     * Внешние ссылки http / https / ftp / ftps / sftp или из хранилища загрузок rfs://upload/путь-до-файла
     * и др. согласно документации
     * @link https://sendsay.ru/api/api.html#%D0%92%D0%BD%D0%B5%D1%81%D0%B5%D0%BD%D0%B8%D0%B5-%D1%81%D0%BF%D0%B8%D1%81%D0%BA%D0%B0-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%BE%D0%B2
     */
    private ?string $users_url = NULL; // "адрес реестра получателей" -- игнорируется при выпуске

    /**
     * Возможные значения  none|always|onerror|onok
     * по умолчанию none = не удалять
     */
    private ?UsersUrlRemove $users_url_remove = NULL; // "удаление реестра" -- игнорируется при выпуске

    /**
     * код группы-списка, в кот. будут накапливаться адреса, попавшие в выпуск с данным черновиком
     * каждый адрес вносится только один первый раз
     */
    private ?string $issue_member_list = NULL;

    /**
     * Код группы-списка, в кот. будут накапливаться адреса тех, кто нажал "Отписаться" после выпуска с данным черновиком
     * Каждый адрес вносится только один первый раз. При последующих выпусках с этим черновиком участники этой группы
     * будут исключаться (отправки не будет).
     */
    private ?string $unsub_list = NULL;

    /**
     * игнорирование стоп-листа
     * игнорировать пользовательский стоп-лист при выпуске. Доступно только при заключении отдельного соглашения
     */
    private ?bool $ignore_stoplist = NULL;

    /**
     * обязательный код базовой группы телеграм-бота для импорта Телеграм.
     * Для остальных - игнорируется
     */
    private ?string $basegroup_id = NULL;

    /**
     * параметр объединения Экспресс-выпусков.
     * Только для Экспресс-выпуска.
     * Выдержка из документации:
     * если Экспресс-выпуск с такими же именем (параметр "name") когда-либо уже выпускался в текущем интервале накопления,
     * то этот запускаемый выпуск будет оформлен не как новый, а как продолжение уже вышедшего.
     * полезно в случае, если ваши CRM не может выдать весь реестр для рассылки за раз,
     * а только порциями.
     * с учётом параллельности приёма и выпуска заданий на рассылку, настоятельно рекомендуется
     * выпустить сначала первую порцию и потом уже последующие можно запускать параллельно
     */
    private ?bool $accumulate = NULL;

    /**
     * параметр объединения Экспресс-выпусков.
     * Только для Экспресс-выпуска.
     * Допустимые значения month|week|day
     * По умолчанию month
     *
     */
    private ?AccumulateBy $accumulate_by = NULL;

    // ---------------

    private array $validationErrors = [];
    private ValidatorCollection $validators;

    public function __construct(?array $options)
    {
        if (isset($options['letter']) && is_array($options['letter'])) {
            $options['letter'] = new EmailLetter($options['letter']);
        }
        parent::__construct($options);
        $this->init();
    }


    public function init($params = NULL): void
    {
        $this->setValidators($this->createValidatorsFromOptionsArray($this->validatorsOptions()));
        return;
    }

    public function getReadableProps(): array
    {
        return self::READABLE_PROPS;
    }

    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getEntityName(): string
    {
        return 'DraftToUpdate';
    }

    private function validatorsOptions(): array
    {
        return [

            'users_slice' => [
                // может содержать только цифры или цифры + % в конце
                ['class' => Regexp::class, 'options' => ['pattern' => '/^\d+%?$/', 'messages' => [Regexp::NOT_MATCH => 'Invalid value for users_slice']]],
            ],
            'alias' => [
                // Не должен начинаться с цифры и не должен содержать пробелы.
                [
                    'class' => Regexp::class, 'options' => ['pattern' => '/^[^0-9| ][^ ][0-9a-zA-Zа-яА-Я|\-_\/\\\\]+$/', 'messages' => [Regexp::NOT_MATCH => 'Alias cannot starts with digit and must not contain spaces']]
                ]
            ],
            'users_url_remove' => [
                ['class' => InEnumValidator::class,
                    'options' => [
                        'messages' => [InEnumValidator::IS_OUT => "'%value%' is unacceptable value for users_url_remove"],
                        'enum' => new UsersUrlRemove('none'),
                        'skipIfNull' => true,
                    ]
                ],
            ],
            'accumulate_by' => [
                ['class' => InEnumValidator::class,
                    'options' => [
                        'messages' => [InEnumValidator::IS_OUT => "'%value%' is unacceptable value for accumulate_by"],
                        'enum' => new AccumulateBy('month'),
                        'skipIfNull' => true,
                    ]
                ],
            ]
        ];
    }

    public function toArray(): array
    {
        $result = parent::toArray();
        foreach (self::UNDERSCORED_PROPS as $pName) {
            $dotNotated = str_replace('_', '.', $pName);
            if (isset($result[$dotNotated])) {
                $result[$pName] = $result[$dotNotated];
                unset($result[$dotNotated]);
            }
        }
        return $result;
    }
    
    /**
     * @return EmailLetter|null
     */
    public function getLetter(): ?EmailLetter
    {
        return $this->letter;
    }

    public function setAccumulate_by($v): void
    {
        if (is_string($v)) {
            try {
                $enum = AccumulateBy::from($v);
                $this->accumulate_by = $enum;
            } catch (\Exception $e) {
                throw $e;
            }
        } else {
            $this->accumulate_by = $v;
        }
    }
    public function setUsers_url_remove($v): void
    {
        if (is_string($v)) {
            try {
                $enum = UsersUrlRemove::from($v);
                $this->users_url_remove = $enum;
            } catch (\Exception $e) {
                throw $e;
            }
        } else {
            $this->users_url_remove = $v;
        }
    }

    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name) && in_array($name, self::READABLE_PROPS)) {
            if ($this->$name instanceof ArrayableI) {
                return $this->$name->toArray();
            } else {
                return $this->$name;
            }
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setterName = 'set' . ucfirst($name);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } elseif (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Trying to set unknown property: ' . get_class($this) . '::' . $name);
        }
    }


}