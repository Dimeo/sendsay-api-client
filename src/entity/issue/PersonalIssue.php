<?php

namespace Sendsay\ApiClient\entity\issue;

use Laminas\Validator\NotEmpty;
use Sendsay\ApiClient\entity\letter\AbstractLetter;
use Sendsay\ApiClient\enum\AddrType;
use Sendsay\ApiClient\enum\GroupType;
use Sendsay\ApiClient\enum\IssueSendWhen;
use Sendsay\ApiClient\validator\InEnumValidator;
use Sendsay\ApiClient\validator\ValidatorCollection;

class PersonalIssue extends AbstractIssue
{
    const READABLE_PROPS = [];
    const UNDERSCORED_PROPS = ['weak_draft', 'issue_include_filter', 'issue_exclude_filter', 'issue_member_list', 'unsub_list', 'ignore_stoplist'];

    // ---------------
    private ?string $name = NULL; // "Название выпуска", -- параметр необязателен, при отсутствии используется тема письма или имя отправителя sms
    private ?array $label = NULL; // Список произвольных меток, позволяющих фильтровать/классифицировать. От 0 до 10 строк не длиннее 128 символов
    private AbstractLetter $letter;
    private string $group = 'personal';
    private ?string $basegroup_id = NULL;
    private IssueSendWhen $sendwhen;
    private bool $weak_draft = true;
    private ?string $dkim_id = NULL;
    private ?string $issue_include_filter = NULL;
    private ?string $issue_exclude_filter = NULL;
    private ?string $group_exclude = NULL;
    private ?string $issue_member_list = NULL;
    private ?string $unsub_list = NULL;
    private bool $ignore_stoplist = false;
    private ?string $class_id = NULL;



    // ---------------

    private array $validationErrors = [];
    private ValidatorCollection $validators;

    public function __construct(?array $options)
    {
        parent::__construct($options);
        $this->init();
    }


    public function init($params = NULL): void
    {
        $this->setValidators($this->createValidatorsFromOptionsArray($this->validatorsOptions()));
        return;
    }

    public function getReadableProps(): array
    {
        return self::READABLE_PROPS;
    }

    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getEntityName(): string
    {
        return 'PersonalIssue';
    }

    private function validatorsOptions(): array
    {
        return [
            'id' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'List ID cannot be empty']]],
            ],
            'name' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'List Name cannot be empty']]],
            ],
            'type' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'List Type cannot be empty']]],
                ['class' => InEnumValidator::class,
                    'options' => [
                        'messages' => [InEnumValidator::IS_OUT => "'%value%' is unacceptable value for list type"],
                        'enum' => new  GroupType('list')
                    ]
                ],
            ],
            'addr_type' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'List address type cannot be empty']]],
                ['class' => InEnumValidator::class,
                    'options' => [
                        'messages' => [InEnumValidator::IS_OUT => "'%value%' is unacceptable value for list address type"],
                        'enum' => new  AddrType('email')
                    ]
                ],
            ]
        ];
    }

    public function toArray(): array
    {
        $result = parent::toArray();
        if (isset($result['weak.draft'])) {
            $result['weak_draft'] = $result['weak.draft'];
            unset($result['weak.draft']);
        }
        return $result;
    }

}