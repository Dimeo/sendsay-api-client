<?php

namespace Sendsay\ApiClient\entity\issue;

use Sendsay\ApiClient\entity\letter\EmailLetter;
use Sendsay\ApiClient\entity\letter\SmsLetter;
use Sendsay\ApiClient\entity\letter\ViberLetter;
use Sendsay\ApiClient\entity\letter\VkLetter;

abstract class AbstractIssue extends \Sendsay\ApiClient\entity\AbstractEntity
{
    public static function createGroupListIssue(array $data)
    {
    }

    public static function createGroupFilterIssue(array $data)
    {
    }

    public static function createMassIssue(array $data)
    {
    }

    public static function createPersonalIssue(array $data)
    {
    }
}