<?php

namespace Sendsay\ApiClient\entity;

use Sendsay\ApiClient\validator\ValidatorCollection;

interface ValidatableI
{

    public function setValidators(ValidatorCollection $validators): void;
    public function getValidators(): ValidatorCollection;
    public function getValidationErrors(): array;
    public function setValidationErrors(array $errorsList): void;
    public function validate(): bool;
    public function isValid(): bool;

}