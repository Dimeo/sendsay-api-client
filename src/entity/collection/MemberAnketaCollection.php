<?php

namespace Sendsay\ApiClient\entity\collection;

use http\Exception\RuntimeException;
use Sendsay\ApiClient\entity\anketa\MemberAnketa;
use Sendsay\ApiClient\entity\EntityCollection;
use Sendsay\ApiClient\entity\EntityI;

/**
 * @property MemberAnketa[] $entities
 */
class MemberAnketaCollection extends EntityCollection
{
    public function __construct(array $entities, bool $isLastPage = false)
    {
        $this->entities = array_filter($entities, function ($item) { return $item instanceof MemberAnketa; });
        $this->isLastPage = $isLastPage;
    }

    public function current(): MemberAnketa
    {
        return current($this->entities);
    }

    public function append(EntityI $entity): MemberAnketaCollection
    {
        if (!$entity instanceof MemberAnketa) {
            throw new RuntimeException(get_class($entity) . ' cannot be append to ' . MemberAnketaCollection::class);
        }
        $this->entities[] = $entity;
        return $this;
    }


}