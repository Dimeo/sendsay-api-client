<?php

namespace Sendsay\ApiClient\entity\anketa;

use Laminas\I18n\Validator\PostCode;
use Laminas\Validator\InArray;
use Sendsay\ApiClient\DTO\Confirm;
use Sendsay\ApiClient\DTO\CreateUpdate;
use Sendsay\ApiClient\DTO\Import;
use Sendsay\ApiClient\DTO\Last;
use Sendsay\ApiClient\DTO\MemberError;
use Sendsay\ApiClient\DTO\Tg;
use Sendsay\ApiClient\DTO\Vk;
use Sendsay\ApiClient\entity\AbstractEntity;
use Sendsay\ApiClient\enum\AddrType;
use Sendsay\ApiClient\helpers;
use Sendsay\ApiClient\validator\PostalCode;
use Sendsay\ApiClient\validator\ValidatorCollection;

/**
 * @property CreateUpdate | NULL $create
 * @property CreateUpdate | NULL $update
 * @property Import | NULL $import
 * @property AddrType $addr_type
 * @property DateTime $birthDate
 * @property string $email
 * @property string | NULL $domain
 * @property int | NULL $id
 * @property int | NULL $dataset
 * @property Last | NULL $last
 * @property int $haslock
 * @property Confirm | NULL $confirm
 * @property MemberError | NULL $error
 * @property string $lockremove
 * @property Vk | NULL $vk
 * @property Tg | NULL $tg
 * @property array | NULL $heads
 * @property array | NULL $stoplist
 * @property array | NULL $_group
 */
class MemberAnketa extends AbstractEntity
{
    private ?CreateUpdate $create = NULL;
    private ?CreateUpdate $update = NULL;
    private ?Import $import = NULL;
    private ?AddrType $addr_type = NULL;
    private string $email = '';
    private ?string $domain = NULL;
    private ?int $id = NULL;
    private ?int $dataset = NULL;
    private ?Last $last = NULL;
    private int $haslock = 0; // 0-нет, 1-отписался, 2-не подтвержден, 4-фатальные ошибки доставки, 3=2+1, 5=4+1, 6=4+2, 7=1+2+4
    private ?Confirm $confirm = NULL;
    private ?MemberError $error = NULL;
    private ?string $lockremove = '0'; //  адрес отписан или в стоп-листе (1) или нет (0)
    private ?Vk $vk = NULL;
    private ?Tg $tg = NULL;
    private ?array $heads = NULL;
    private ?array $stoplist = NULL;
    private ?array $_group = NULL;


    private ValidatorCollection $validators;
    private array $validationErrors = [];


    public function __construct(?array $options)
    {
        if(is_array($options)) {
            foreach ($options as $key => $value) {
                $parts = explode('.', $key);
                if (count($parts) < 2) {
                    continue;
                }
                $options[$parts[0]][$parts[1]] = $value;
            }
        }
        parent::__construct($options);
        $this->init();
    }

    public function init($params = NULL): void
    {
        $validatorsOptions = $this->validatorOptions();
        if (!empty($validatorsOptions)) {
            $this->setValidators($this->createValidatorsFromOptionsArray($validatorsOptions));
        } else {
            $this->setValidators(new ValidatorCollection([]));
        }
        return;
    }

    public function getReadableProps(): array
    {
        return ['create','update','import','addr_type','email','domain','id','dataset','last', 'haslock', 'confirm', 'error', 'lockremove', 'vk', 'tg', 'heads', 'stoplist', '_group'];
    }

    private function validatorOptions(): array
    {
        return [];
    }
    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getEntityName(): string
    {
        return 'MemberAnketa';
    }

    public function __get($name)
    {

        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name) && in_array($name, $this->getReadableProps())) {
            return $this->$name;
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setterName = 'set' . ucfirst($name);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } elseif (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Trying to set unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function toArray(): array
    {
        $result =  parent::toArray();
        return array_filter($result, function ($item) {
            return $item !== NULL;
        });
    }

}