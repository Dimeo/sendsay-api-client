<?php

namespace Sendsay\ApiClient\entity\anketa;

use Sendsay\ApiClient\entity\AbstractEntity;
use Sendsay\ApiClient\helpers;
use Sendsay\ApiClient\validator\ValidatorCollection;

/**
 * @property string $num
 * @property \Sendsay\ApiClient\DTO\Tg $tg
 */
class Tg extends AbstractEntity
{
    private ?\Sendsay\ApiClient\DTO\Tg $tg = NULL;
    private ?string $num = NULL;

    private ValidatorCollection $validators;
    private array $validationErrors = [];


    public function __construct(?array $options)
    {
        if (empty($options) || helpers::arrayIsList($options)) {
            return;
        }

        $keys = array_keys($options);

        if (count($keys) > 1) {
            $numValue = $options['num'] ?? NULL;
            $tgValue = $options['tg'] ?? NULL;
        } elseif(count($keys) == 1) {
            $numValue = $keys[0];
            $tgValue = $options[$numValue];
        } else {
            $numValue = NULL;
            $tgValue = NULL;
        }

        if (!empty($numValue)) {
            $this->num = !(is_array($numValue) || is_object($numValue)) ? strval($numValue) : NULL;
        }
        if (!empty($tgValue)) {
            if($tgValue instanceof \Sendsay\ApiClient\DTO\Tg) {
                $this->tg = $tgValue;
            } elseif (is_array($tgValue)) {
                $this->tg = new \Sendsay\ApiClient\DTO\Tg($tgValue);
            } else {
                $this->tg = NULL;
            }
        }
        $this->init();
    }

    public function init($params = NULL): void
    {
        $validatorsOptions = $this->validatorOptions();
        if (!empty($validatorsOptions)) {
            $this->setValidators($this->createValidatorsFromOptionsArray($validatorsOptions));
        } else {
            $this->setValidators(new ValidatorCollection([]));
        }
        return;
    }

    public function getReadableProps(): array
    {
        return ['num', 'tg'];
    }

    private function validatorOptions(): array
    {
        return [];
    }
    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getEntityName(): string
    {
        return 'tg';
    }

    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name) && in_array($name, $this->getReadableProps())) {
            return $this->$name;
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setterName = 'set' . ucfirst($name);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } elseif (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Trying to set unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function toArray(): array
    {
        if ($this->num === NULL || $this->tg === NULL) {
            return [];
        }
        $result = [$this->num => $this->tg->toArray()];
        return $result;
    }
}