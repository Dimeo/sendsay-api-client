<?php

namespace Sendsay\ApiClient\entity\anketa;

use Laminas\I18n\Validator\PostCode;
use Laminas\Validator\Digits;
use Laminas\Validator\InArray;
use Laminas\Validator\NotEmpty;
use Sendsay\ApiClient\entity\AbstractEntity;
use Sendsay\ApiClient\helpers;
use Sendsay\ApiClient\validator\PostalCode;
use Sendsay\ApiClient\validator\ValidatorCollection;

/**
 * @property string $lastName
 * @property string $firstName
 * @property string $middleName
 * @property int $gender
 * @property \DateTime $birthDate
 * @property string $address
 * @property int $postalCode
 * @property int $country
 * @property string $city
 */
class BaseAnketa extends AbstractEntity
{
    private ?string $lastName = NULL;
    private ?string $firstName = NULL;
    private ?string $middleName = NULL;
    private ?int $gender = NULL; // 0 - женский, 1 - мужской NULL - не указано
    private ?\DateTime $birthDate = NULL;
    private ?string $address = NULL;
    private ?string $postalCode = '';
    private ?string $country = NULL;
    private ?string $city = NULL;

    private ValidatorCollection $validators;
    private array $validationErrors = [];


    public function __construct(?array $options)
    {
        if(is_array($options)) {
            foreach ($options as $key => $value) {
                $parts = explode('.', $key);
                if (count($parts) < 2) {
                    continue;
                }
                $options[$parts[0]][$parts[1]] = $value;
            }
        }
        parent::__construct($options);
        $this->init();
    }

    public function init($params = NULL): void
    {
        $this->setValidators($this->createValidatorsFromOptionsArray($this->validatorOptions()));
        return;
    }

    public function getReadableProps(): array
    {
        return ['lastName','firstName','middleName','gender','birthDate','address','postalCode','country','city'];
    }

    private function validatorOptions(): array
    {
        return [
            'postalCode' => [
                [
                    'class' => PostalCode::class,
                    'options' => [
                        'locale' => 'ru_RU',
                        'messages' => [
                            PostCode::INVALID => 'Postal code is invalid',
                            PostCode::NO_MATCH => "The value '%value%' does not appear to be a Russian postal code"
                        ]
                    ]
                ],
            ],
            'gender' => [
                [
                    'class' => InArray::class,
                    'options' => [
                        'haystack' => [0,1],
                        'messages' => [
                            InArray::NOT_IN_ARRAY => "gender value can be only 1 for male or 0 for female"
                        ]
                    ]
                ]
            ],
        ];
    }
    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getEntityName(): string
    {
        return 'BaseAnketa';
    }

    public function setBirthDate($v): void
    {
        if (!is_integer($v) && !is_string($v)) {
            throw new \Exception('Illegal create time value type');
        }

        if (is_string($v)) {
            $timestamp = strtotime($v);
        } elseif (is_integer($v)) {
            $timestamp = $v;
        }

        $d = new \DateTime();
        $d->setTimestamp($timestamp);
        $this->birthDate = $d;
    }

    public function setGender($value): void
    {
        if (is_numeric($value)) {
            $this->gender = intval($value);
            return;
        }
        if (is_string($value)) {
            $value = mb_strtolower(trim($value));
            $value = str_replace('.', '', $value);
            $variants = ['m' => 1,'male' => 1,'м' => 1,'муж' => 1,'мужской' => 1,'f' => 0,'female'=> 0 ,'ж' => 0,'жен' => 0,'женский' => 0, '1' => 1, '0' => 0];
            if (array_key_exists($value,$variants)) {
                $this->gender = $variants[$value];
            } else {
                $this->gender = NULL;
            }
        }
    }
    public function setCountry($value): void
    {
        if (is_numeric($value)) {
            $this->country = intval($value);
            return;
        }
        if (is_string($value)) {
            $value = mb_strtolower(trim($value));
            $value = helpers::getCountryCode($value);
            $this->country = intval($value);
            return;
        }
        if (is_array($value)) {
            $this->country = intval(array_keys($value)[0]);
        }
        return;
    }

    public function __get($name)
    {
        if ($name === 'birthDate' && $this->birthDate instanceof \DateTime) {
            return $this->birthDate->format('Y-m-d');
        }

        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name) && in_array($name, $this->getReadableProps())) {
            return $this->$name;
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setterName = 'set' . ucfirst($name);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } elseif (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Trying to set unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function toArray(): array
    {
        $result =  parent::toArray();

        if ($this->gender !== NULL) {
            $result['gender'] = [strval($this->gender)];
        }
        if (!empty($this->country)) {
            $countryCode = helpers::getCountryCode($this->country);

            $result['country'] = !empty($countryCode) ? [$countryCode] : NULL;
        }

        return array_filter($result, function ($item) {
            return !empty($item);
        });
    }

}