<?php

namespace Sendsay\ApiClient\entity\anketa\question;

use Laminas\Validator\Digits;
use Laminas\Validator\GreaterThan;
use Laminas\Validator\InArray;
use Laminas\Validator\NotEmpty;
use Sendsay\ApiClient\DTO\collection\QuestionAnswersCollection;
use Sendsay\ApiClient\validator\arrays\arrayValuesAre;

/**
 * @property QuestionAnswersCollection $answers
 * @property array $order
 */
class OneOfList extends AbstractQuestion
{
    const TYPE_NAME = '1m';
    protected QuestionAnswersCollection $answers;
    protected array $order;
    public function __construct(?array $options)
    {
        if (isset($options['answers']) && is_array($options['answers'])) {
            $options['answers'] = QuestionAnswersCollection::createFromArray($options['answers']);
        }
        parent::__construct($options);
        $this->type = static::TYPE_NAME;
    }

    public function getReadableProps(): array
    {
        return array_merge(parent::getReadableProps(), ['answers', 'order']);
    }

    protected function validatorOptions(): array
    {
        $v = parent::validatorOptions();
        $v['order'][] = [
            'class' => arrayValuesAre::class,
            'options' => [
                'acceptableValuesList' => array_map(function ($elem)  {
                        if (is_array($elem) && count($elem) > 0) {
                            return array_keys($elem)[0];
                        }
                    }, $this->answers->toArray())
//                'messages' => [
//                    arrayValuesAre::IS_OUT => 'unacceptable value'
//                ]
            ]
        ];
        $v['type'][] = ['class' => InArray::class,
            'options' => [
                'haystack' => [static::TYPE_NAME],
                'messages' => [
                    InArray::NOT_IN_ARRAY => "parameter 'type' of this anketa question " . static::class . " must be equal to '" . static::TYPE_NAME . "'"
                ]
            ]
        ];

        return $v;
    }
    public function getEntityName(): string
    {
        return 'oneOfMultipleAnketaQuestion';
    }
}