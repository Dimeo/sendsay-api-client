<?php

namespace Sendsay\ApiClient\entity\anketa\question;

use Laminas\Validator\InArray;
use Sendsay\ApiClient\DTO\collection\QuestionAnswersCollection;
use Sendsay\ApiClient\validator\arrays\arrayValuesAre;

class MultiOfList extends OneOfList
{
    const TYPE_NAME = 'nm';

//    public function __construct(?array $options)
//    {
//        parent::__construct($options);
//        $this->type = self::TYPE_NAME;
//    }

    public function getEntityName(): string
    {
        return 'manyOfMultipleAnketaQuestion';
    }
}