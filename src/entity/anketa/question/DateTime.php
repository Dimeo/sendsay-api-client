<?php

namespace Sendsay\ApiClient\entity\anketa\question;

use Laminas\Validator\Digits;
use Laminas\Validator\GreaterThan;
use Laminas\Validator\InArray;
use Laminas\Validator\NotEmpty;
use Sendsay\ApiClient\enum\QuestionType;
use Sendsay\ApiClient\validator\InEnumValidator;

/**
 * @property string $dtsubtype
 */
class DateTime extends AbstractQuestion
{
    const TYPE_NAME = 'dt';
    const DEFAULT_DIGITS_ERROR_MESSAGE = 'Parameter "width" of anketa question of type "free" must contain only digits';
    protected ?string $dtsubtype = NULL;

    public function __construct(?array $options)
    {
        parent::__construct($options);
        $this->type = self::TYPE_NAME;
    }

    public function getReadableProps(): array
    {
        return array_merge(parent::getReadableProps(), ['dtsubtype']);
    }

    protected function validatorOptions(): array
    {
        $v = parent::validatorOptions();
        $v['dtsubtype'] = [
            ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'Parameter "dtsubtype" is required for anketa question of type "dateTime"']]],
            ['class' => InEnumValidator::class,
                'options' => [
                    'messages' => [InEnumValidator::IS_OUT => "'%value%' is unacceptable value for parameter 'dtsubtype' of question of type dateTime"],
                    'enum' => new  \Sendsay\ApiClient\enum\DateTime('ys')
                ]
            ],
        ];

        $v['type'][] = ['class' => InArray::class,
            'options' => [
                'haystack' => [self::TYPE_NAME],
                'messages' => [
                    InArray::NOT_IN_ARRAY => "parameter 'type' of question of type dateTime must be equal to '" . self::TYPE_NAME . "'"
                ]
            ]
        ];

        return $v;
    }
    public function getEntityName(): string
    {
        return 'dtAnketaQuestion';
    }
    public function setDtsubtype(string $v): void
    {
        try {
            $enum = \Sendsay\ApiClient\enum\DateTime::from($v);
            $this->dtsubtype = $enum->getValue();
        } catch (\Exception $e) {
            throw $e;
        }
    }

}