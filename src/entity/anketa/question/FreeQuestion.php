<?php

namespace Sendsay\ApiClient\entity\anketa\question;

use Laminas\Validator\Digits;
use Laminas\Validator\GreaterThan;
use Laminas\Validator\InArray;
use Laminas\Validator\NotEmpty;

/**
 * @property int $width
 */
class FreeQuestion extends AbstractQuestion
{
    const TYPE_NAME = 'free';
    const DEFAULT_DIGITS_ERROR_MESSAGE = 'Parameter "width" of anketa question of type "free" must contain only digits';
    protected ?int $width = NULL;

    public function __construct(?array $options)
    {
        parent::__construct($options);
        $this->type = self::TYPE_NAME;
    }

    public function getReadableProps(): array
    {
        return array_merge(parent::getReadableProps(), ['width']);
    }

    protected function validatorOptions(): array
    {
        $v = parent::validatorOptions();
        $v['width'] = [
            ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'Parameter "width" is required for anketa question of type "free"']]],
            ['class' => GreaterThan::class, 'options' => ['min' => 0, 'messages' => [GreaterThan::NOT_GREATER => 'Parameter "width" of anketa question of type "free" must be greater than 0 (zero)']]],
            ['class' => Digits::class, 'options' => ['messages' =>
                [
                    Digits::NOT_DIGITS => self::DEFAULT_DIGITS_ERROR_MESSAGE,
                    Digits::STRING_EMPTY => self::DEFAULT_DIGITS_ERROR_MESSAGE,
                    Digits::INVALID => self::DEFAULT_DIGITS_ERROR_MESSAGE,
                ]
            ]]
        ];
        $v['type'][] = ['class' => InArray::class,
            'options' => [
                'haystack' => [self::TYPE_NAME],
                'messages' => [
                    InArray::NOT_IN_ARRAY => "parameter 'type' of free anketa question must be equal to 'free'"
                ]
            ]
        ];

        return $v;
    }
    public function getEntityName(): string
    {
        return 'freeAnketaQuestion';
    }
}