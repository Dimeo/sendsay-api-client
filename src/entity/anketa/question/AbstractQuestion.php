<?php

namespace Sendsay\ApiClient\entity\anketa\question;

use Laminas\Validator\NotEmpty;
use Sendsay\ApiClient\entity\AbstractEntity;
use Sendsay\ApiClient\enum\GroupType;
use Sendsay\ApiClient\enum\QuestionType;
use Sendsay\ApiClient\helpers;
use Sendsay\ApiClient\validator\InEnumValidator;
use Sendsay\ApiClient\validator\ValidatorCollection;

/**
 * @property string $name
 * @property string $type
 * @property string $id
 */
class AbstractQuestion extends AbstractEntity
{
    protected string $name = '';
    protected ?string $type = NULL;
    protected ?string $id = NULL;
    // =============================

    private ValidatorCollection $validators;
    private array $validationErrors = [];


    public function __construct(?array $options)
    {
        if (empty($options) || helpers::arrayIsList($options)) {
            return;
        }
        parent::__construct($options);
        $this->init();
    }

    public function init($params = NULL): void
    {
        $validatorsOptions = $this->validatorOptions();
        if (!empty($validatorsOptions)) {
            $this->setValidators($this->createValidatorsFromOptionsArray($validatorsOptions));
        } else {
            $this->setValidators(new ValidatorCollection([]));
        }
        return;
    }

    public function getReadableProps(): array
    {
        return ['name', 'type', 'id'];
    }

    protected function validatorOptions(): array
    {
        return [
            'name' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'Question name cannot be empty']]],
            ],
            'type' => [
                ['class' => NotEmpty::class, 'options' => ['messages' => [NotEmpty::IS_EMPTY => 'Question type cannot be empty']]],
                ['class' => InEnumValidator::class,
                    'options' => [
                        'messages' => [InEnumValidator::IS_OUT => "'%value%' is unacceptable value for question type"],
                        'enum' => new  QuestionType('free')
                    ]
                ],
            ],

        ];
    }
    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getEntityName(): string
    {
        return 'anketaQuestion';
    }

    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name) && in_array($name, $this->getReadableProps())) {
            return $this->$name;
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setterName = 'set' . ucfirst($name);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } elseif (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Trying to set unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function setType(string $v): void
    {
        try {
            $enum = QuestionType::from($v);
            $this->type = $enum->getValue();
        } catch (\Exception $e) {
            throw $e;
        }
    }
}