<?php

namespace Sendsay\ApiClient\entity;

use Sendsay\ApiClient\entity\anketa\BaseAnketa;
use Sendsay\ApiClient\entity\anketa\DateTime;
use Sendsay\ApiClient\entity\anketa\MemberAnketa;
use Sendsay\ApiClient\entity\collection\MemberAnketaCollection;
use Sendsay\ApiClient\validator\ValidatorCollection;

/**
 * @property MemberAnketaCollection $head
 * @property array $heads
 * @property int $dataset
 * @property MemberDatakey $data
 */
class MemberDataSet extends AbstractEntity
{
    private ?MemberAnketaCollection $head = NULL;
    private ?array $heads = NULL;
    private ?int $dataset = NULL;
    private ?MemberDatakey $data = NULL;


    private ValidatorCollection $validators;
    private array $validationErrors = [];


    public function __construct(?MemberAnketaCollection $head, ?array $heads, ?int $dataset, ?MemberDatakey $data)
    {
        $this->head = $head;
        $this->heads = $heads;
        $this->dataset = $dataset;
        $this->data = $data;

        $this->init();
    }

    public static function createFromApiResponsedArray(array $data): MemberDataSet
    {
        $heads = !empty($data['heads']) ? $data['heads'] : NULL;
        $dataset = !empty($data['dataset']) ? intval($data['dataset']) : NULL;
        $dataKey = !empty($data['data']) ? MemberDatakey::createFromApiResponsedArray($data['data']) : NULL;

        if (empty($data['head'])) {
            $head = NULL;
        } else {
            $collectionData = [];
            foreach($data['head'] as $key => $memberAnketaData) {
                $collectionData[$key] = new MemberAnketa($memberAnketaData);
            }
            $head = new MemberAnketaCollection($collectionData);
        }

        return new self($head, $heads,$dataset, $dataKey);
    }

    public function init($params = NULL): void
    {
        $validatorsOptions = $this->validatorOptions();
        if (!empty($validatorsOptions)) {
            $this->setValidators($this->createValidatorsFromOptionsArray($validatorsOptions));
        } else {
            $this->setValidators(new ValidatorCollection([]));
        }
        return;
    }

    public function getReadableProps(): array
    {
        return ['head','heads','dataset','data'];
    }

    private function validatorOptions(): array
    {
        return [];
    }
    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getEntityName(): string
    {
        return 'MemberDataSet';
    }

    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name) && in_array($name, $this->getReadableProps())) {
            return $this->$name;
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setterName = 'set' . ucfirst($name);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } elseif (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Trying to set unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function toArray(): array
    {
        $result =  parent::toArray();
        return array_filter($result, function ($item) {
            return $item !== NULL;
        });
    }

}