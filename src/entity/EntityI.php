<?php

namespace Sendsay\ApiClient\entity;

interface EntityI extends ValidatableI
{
    public function init(array $params): void;
    public function getReadableProps(): array;
    public function getEntityName(): string;

}