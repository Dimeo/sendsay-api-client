<?php

namespace Sendsay\ApiClient\entity;

use Laminas\I18n\Validator\PostCode;
use Laminas\Validator\InArray;
use MyCLabs\Enum\Enum;
use Sendsay\ApiClient\entity\anketa\BaseAnketa;
use Sendsay\ApiClient\entity\anketa\DateTime;
use Sendsay\ApiClient\entity\anketa\MemberAnketa;
use Sendsay\ApiClient\helpers;
use Sendsay\ApiClient\validator\PostalCode;
use Sendsay\ApiClient\validator\ValidatorCollection;

/**
 * @property BaseAnketa | NULL $base
 * @property MemberAnketa | NULL $member
 * @property array | NULL $custom
 * @property array | NULL $_group
 */
class MemberDatakey extends AbstractEntity
{
    private ?BaseAnketa $base = NULL;
    private ?MemberAnketa $member = NULL;
    private ?array $custom = NULL;

    private ValidatorCollection $validators;
    private array $validationErrors = [];

    protected bool $allowDynamicProps = true;
    protected ?array $_group = NULL;

    public function __construct(?BaseAnketa $base, ?MemberAnketa $member, ?array $custom)
    {
        $this->base = $base;
        $this->member = $member;
        $this->custom = $custom;
//        parent::__construct($options);
        $this->init();
    }

    public static function createFromApiResponsedArray(array $data): MemberDatakey
    {
        $base = !empty($data['base']) ? new BaseAnketa($data['base']) : NULL;
        $member = !empty($data['member']) ? new MemberAnketa($data['member']) : NULL;
        $custom = !empty($data['custom']) ? $data['custom'] : NULL;
        unset($data['base']);
        unset($data['member']);
        unset($data['custom']);

        $instance = new self($base, $member, $custom);
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if ($key == '-group') {
                    $instance->_group = $value;
                } else {
                    $instance->$key = $value;
                }
            }
        }
        return $instance;
    }

    public function init($params = NULL): void
    {
        $validatorsOptions = $this->validatorOptions();
        if (!empty($validatorsOptions)) {
            $this->setValidators($this->createValidatorsFromOptionsArray($validatorsOptions));
        } else {
            $this->setValidators(new ValidatorCollection([]));
        }
        return;
    }

    public function getReadableProps(): array
    {
        return ['base','member','custom', '_group'];
    }

    private function validatorOptions(): array
    {
        return [];
    }
    public function setValidators(ValidatorCollection $validators): void
    {
        $this->validators = $validators;
    }

    public function getValidators(): ValidatorCollection
    {
        return $this->validators;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function setValidationErrors(array $errorsList): void
    {
        $this->validationErrors = $errorsList;
    }

    public function isValid(): bool
    {
        return empty($this->validationErrors);
    }

    public function getEntityName(): string
    {
        return 'MemberDatakey';
    }

    public function validate(): bool
    {
        if (!empty($this->validators) && !parent::validate()) {
            return false;
        }
        $baseIsOk = $this->base->validate();
        $memberIsOk = $this->member->validate();
        $errors = array_merge($this->base->getValidationErrors(), $this->member->getValidationErrors());
        $this->setValidationErrors($errors);

        return $baseIsOk && $memberIsOk;
    }
    
    public function isInGroup(string $groupListId): bool
    {
        if (empty($this->_group)) {
            return false;
        }
        $v = $this->_group[$groupListId] ?? [];
        return array_key_exists('y', $v) && !empty($v['y']);
    }
    
    public function addToGrouplist(string $groupListId): void
    {
        if (!$this->isInGroup($groupListId)) {
            $this->_group[$groupListId] = 1;
        }
        return;
    }
    
    public function isConfirmed(): bool
    {
        $c1 = !empty($this->member);
        $cnf = $this->member->confirm;
        $c2 = !empty($cnf);
        $c3 = $c2 && !empty($cnf->time) && $cnf->lock !== true;
        return $c1 && $c2 && $c3;
    }
    
    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif (property_exists($this, $name) && in_array($name, $this->getReadableProps())) {
            return $this->$name;
        } else {
            throw new \Exception('Getting unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function __set($name, $value)
    {
        $setterName = 'set' . ucfirst($name);
        if (method_exists($this, $setterName)) {
            return $this->$setterName($value);
        } elseif ($this->allowDynamicProps || property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            throw new \Exception('Trying to set unknown property: ' . get_class($this) . '::' . $name);
        }
    }

    public function toArray(): array
    {
        $result =  parent::toArray();
        $allProps = get_object_vars($this);

        $ref = new \ReflectionClass($this);
        $props = $ref->getProperties();
        foreach ($props as $prop) {
            unset($allProps[$prop->getName()]);
        }
        foreach ($allProps as $dynamicPropName => $dynamicPropValue) {
            if ($this->$dynamicPropName instanceof ArrayableI) {
                $result[$dynamicPropName] = $this->$dynamicPropName->toArray();
            } elseif ($this->$dynamicPropName instanceof Enum) {
                $result[$dynamicPropName] = $this->$dynamicPropName->getValue();
            } else {
                $result[$dynamicPropName] = $this->$dynamicPropName;
            }
        }
        $result = array_filter($result, function ($item) {
            return $item !== NULL && $item != [];
        });
        helpers::prepareArrayForRequest($result);
        return $result;
    }

}