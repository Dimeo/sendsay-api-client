<?php

namespace Sendsay\ApiClient\filter;

use MyCLabs\Enum\Enum;
use Sendsay\ApiClient\enum\filterItem\FIlterOpers;
use Sendsay\ApiClient\enum\filterItem\GroupListField;

class GroupListFilterItem extends AbstractFilterItem
{
    public function __construct(GroupListField $a, FIlterOpers $op, $v)
    {
        parent::__construct($a, $op, $v);
    }

}