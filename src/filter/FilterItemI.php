<?php

namespace Sendsay\ApiClient\filter;

use MyCLabs\Enum\Enum;

interface FilterItemI
{
    public function getA(): Enum;
    public function getOp(): Enum;
    public function getV();
}
