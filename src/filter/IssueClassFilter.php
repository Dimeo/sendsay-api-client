<?php

namespace Sendsay\ApiClient\filter;

class IssueClassFilter extends AbstractFilter
{

    function addFilterItem(IssueClassFilterItem $filterItem): IssueClassFilter
    {
        $this->items[] = $filterItem;
        return $this;
    }
}