<?php

namespace Sendsay\ApiClient\filter;

class IssueDraftFilter extends AbstractFilter
{

    function addFilterItem(IssueDraftFilterItem $filterItem): IssueDraftFilter
    {
        $this->items[] = $filterItem;
        return $this;
    }
}