<?php

namespace Sendsay\ApiClient\filter;
use Sendsay\ApiClient\entity\ArrayableI;

/**
 * @property FilterItemI[] $items
 */
abstract class AbstractFilter implements ArrayableI
{
    protected array $items = [];
//    abstract function addFilterItem($filterItem):void;

    /**
     * @return FilterItemI[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function toArray(): array
    {
        $res = [];
        $items = $this->getItems();
        foreach ($items as $item) {
            $res [] = $item->toArray();
        }
        return $res;
    }
}