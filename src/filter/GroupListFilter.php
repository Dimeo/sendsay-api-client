<?php

namespace Sendsay\ApiClient\filter;

class GroupListFilter extends AbstractFilter
{

    function addFilterItem(GroupListFilterItem $filterItem): GroupListFilter
    {
        $this->items[] = $filterItem;
        return $this;
    }
}