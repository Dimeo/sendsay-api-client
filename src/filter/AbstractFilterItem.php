<?php

namespace Sendsay\ApiClient\filter;

use MyCLabs\Enum\Enum;
use Sendsay\ApiClient\entity\ArrayableI;

abstract class AbstractFilterItem implements FilterItemI, ArrayableI
{
    private Enum $a;
    private Enum $op;
    private $v;
    public function __construct(Enum $a, Enum $op, $v)
    {
        $this->a = $a;
        $this->op = $op;
        $this->v = $v;
    }

    public function getA(): Enum
    {
        return $this->a;
    }

    public function getOp(): Enum
    {
        return $this->op;
    }

    public function getV() {
        return $this->v;
    }

    public function toArray(): array
    {
        return [
            'a' => $this->getA()->getValue(),
            'op' => $this->getOp()->getValue(),
            'v' => $this->getV()
        ];
    }
}