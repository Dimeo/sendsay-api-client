<?php

namespace Sendsay\ApiClient\filter;

use Sendsay\ApiClient\enum\filterItem\FIlterOpers;
use Sendsay\ApiClient\enum\filterItem\IssueDraftField;

class IssueDraftFilterItem extends AbstractFilterItem
{
    public function __construct(IssueDraftField $a, FIlterOpers $op, $v)
    {
        parent::__construct($a, $op, $v);
    }
}