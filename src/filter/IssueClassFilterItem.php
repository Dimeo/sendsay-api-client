<?php

namespace Sendsay\ApiClient\filter;

use Sendsay\ApiClient\enum\filterItem\FIlterOpers;
use Sendsay\ApiClient\enum\filterItem\IssueClassField;
use Sendsay\ApiClient\enum\filterItem\IssueDraftField;

class IssueClassFilterItem extends AbstractFilterItem
{
    public function __construct(IssueClassField $a, FIlterOpers $op, $v)
    {
        parent::__construct($a, $op, $v);
    }
}