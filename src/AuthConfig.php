<?php

namespace Sendsay\ApiClient;
use Sendsay\ApiClient\enum\AuthMethod;

/**
 * Содержит данные для авторизации.
 * Реализованы только два способа авторизации из четырех возможных.
 * По логину-паролю с получением временного идентификатороа сессии
 * и по Апи ключу (предпочтительнее)
 * @property string $login общий логин, идентификатор аккаунта
 * @property string $sublogin логин пользователя, от имени которого будет активность
 * @property string $password пароль пользователя, от имени которого будет активность
 * @property string $apikey ключ, кот. нужно предварительно, самостоятельно получить. Если он в ниличии, то выше указанные sublogin и password игнорируются
 */
class AuthConfig
{
    private string $login;
    private string $sublogin;
    private string $password;
    private string $apikey;
    public function __construct(array $params)
    {
        helpers::configure($this, $params);
        $this->init();
    }

    public function init()
    {
        if (empty($this->login)) {
            throw new \RuntimeException('Login for account (account ID) is required');
        }

        if (empty($this->apikey) && (empty($this->sublogin) || empty($this->password))) {
            throw new \RuntimeException('Either apikey neither sublogin together with password must be supplied');
        }
    }

    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }
        return NULL;
    }

    public function getAvailableAuthMethod(): AuthMethod
    {
        if (!empty($this->apikey)) {
            return AuthMethod::API_KEY();
        }
        if (!empty($this->sublogin) && !empty($this->password)) {
            return AuthMethod::LOGIN_PWD();
        }
        return AuthMethod::NONE();
    }
}