<?php

namespace Sendsay\ApiClient\validator\arrays;

use MyCLabs\Enum\Enum;
use Sendsay\ApiClient\enum\EmptyEnum;
use Sendsay\ApiClient\helpers;

class arrayValuesAre extends \Laminas\Validator\AbstractValidator
{
    const IS_OUT = 'notMatches';

    protected $messageTemplates = [
        self::IS_OUT => "'%value%' is(are) unacceptable value(s)",
    ];

    protected array $acceptableValuesList;

    protected string $unacceptableValues;

    public function __construct($options = null)
    {
        parent::__construct($options);
        $this->acceptableValuesList = $options['acceptableValuesList'] ?? [];
        if (!helpers::arrayIsList($this->acceptableValuesList)) {
            throw new \RuntimeException('Assoc arrays are unacceptable here');
        }
    }

    /**
     * @inheritDoc
     */
    public function isValid($value): bool
    {
        if (!is_array($value)) {
            $value = (array) $value;
        }
        $this->setValue($value);

        $diffs = array_diff($this->value, $this->acceptableValuesList);

        if (count($diffs) > 0) {
            $this->unacceptableValues = implode(', ', $diffs);
            $this->error(self::IS_OUT, $this->unacceptableValues);
            return false;
        }

        return true;
    }
}