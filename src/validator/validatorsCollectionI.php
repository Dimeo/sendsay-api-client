<?php

namespace Sendsay\ApiClient\validator;

use Laminas\Validator\ValidatorChain;
use Laminas\Validator\ValidatorInterface;

interface validatorsCollectionI extends \Iterator
{
    public function __construct(array $propsList);
    public function current(): ValidatorChain;
    public function key(): string;
    public function next(): void;
    public function append(string $key, ValidatorInterface $validator): validatorsCollectionI;

}