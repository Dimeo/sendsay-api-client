<?php

namespace Sendsay\ApiClient\validator;

use Laminas\Validator\ValidatorChain;
use Laminas\Validator\ValidatorInterface;

class ValidatorCollection implements validatorsCollectionI
{
    private string $index;
    private array $validatorsList = [];

    public function __construct(array $propsList)
    {
//        if (empty($propsList)) {
//            throw new \Exception('empty properties list unacceptable');
//        }
        $this->index = 0;
        foreach ($propsList as $propName) {
            $this->validatorsList[$propName] = new ValidatorChain();
        }
    }

    public function key(): string
    {
        return key($this->validatorsList);
    }
    public function count(): int
    {
        return count($this->validatorsList);
    }

    public function next(): void
    {
        next($this->validatorsList);
    }

    public function current(): ValidatorChain
    {
        return current($this->validatorsList);
    }

    public function rewind(): void
    {
        reset($this->validatorsList);
    }

    public function valid(): bool
    {
        return key($this->validatorsList) !== NULL;
    }

    public function append(string $key, ValidatorInterface $validator): ValidatorCollection
    {
        if (!isset($this->validatorsList[$key])) {
            $this->validatorsList[$key] = new ValidatorChain();
        }
        $this->validatorsList[$key]->attach($validator);
        return $this;
    }

}