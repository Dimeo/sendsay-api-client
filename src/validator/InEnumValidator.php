<?php

namespace Sendsay\ApiClient\validator;

use MyCLabs\Enum\Enum;
use Sendsay\ApiClient\enum\EmptyEnum;

class InEnumValidator extends \Laminas\Validator\AbstractValidator
{
    const IS_OUT = 'notMatches';

    protected $messageTemplates = [
        self::IS_OUT => "'%value%' is unacceptable value",
    ];

    protected Enum $enum;
    protected bool $skipIfNull = false;

    public function __construct($options = null)
    {
        parent::__construct($options);
        $this->enum = $options['enum'] ?? new EmptyEnum('');
        if (isset($options['skipIfNull'])) {
            $this->skipIfNull = boolval($options['skipIfNull']);
        }
    }

    /**
     * @inheritDoc
     */
    public function isValid($value)
    {
        if ($this->skipIfNull && NULL === $value) {
            return true;
        }

        $this->setValue($value);
        $availableValues = array_values($this->enum::toArray());
        if (!in_array($value, $availableValues)) {
            $this->error(self::IS_OUT);
            return false;
        }
        return true;
    }
}