<?php

namespace Sendsay\ApiClient\validator;

use Laminas\Validator\Regex;

class Regexp extends Regex
{
    protected bool $skipIfEmpty = true;
    public function isValid($value)
    {
        if ($this->skipIfEmpty && empty($value)) {
            return true;
        }
        return parent::isValid($value);
    }
}