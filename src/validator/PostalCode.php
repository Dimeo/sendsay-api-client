<?php

namespace Sendsay\ApiClient\validator;

use Laminas\I18n\Validator\PostCode;

class PostalCode extends PostCode
{
    protected bool $allowEmpty = true;
    public function isValid($value)
    {
        if ($this->allowEmpty && empty($value)) {
            return true;
        }
        return parent::isValid($value);
    }
}