<?php

namespace Sendsay\ApiClient\DTO;

class Vk extends Tg
{
    public ?\DateTime $member = NULL;
    public ?\DateTime $left = NULL;

    public function getMemberString(): string
    {
        if (empty($this->member)) {
            return '';
        }
        return $this->member->format(self::DATE_TIME_OUT_FORMAT);
    }

    public function getLeftString(): string
    {
        if (empty($this->left)) {
            return '';
        }
        return $this->left->format(self::DATE_TIME_OUT_FORMAT);
    }
}