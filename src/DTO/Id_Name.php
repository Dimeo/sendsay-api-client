<?php

namespace Sendsay\ApiClient\DTO;

class Id_Name extends AbstractDTO
{
    public ?string $id = NULL;
    public ?string $name = NULL;
}