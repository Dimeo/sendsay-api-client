<?php

namespace Sendsay\ApiClient\DTO;

abstract class AbstractDTO implements \Sendsay\ApiClient\entity\ArrayableI
{
    const DATE_TIME_OUT_FORMAT = 'Y-m-d H:i:s';

    const MENUS_REPLACER = '___';
    const DOT_REPLACER = '_DOT_';

    public function toArray(): array
    {
        $reflection = new \ReflectionClass($this);
        $props = $reflection->getProperties();
        $result = [];
        foreach ($props as $prop) {
            $propName = $prop->getName();
            if ($this->$propName === NULL) {
                continue;
            }
            $propType = $prop->getType()->getName();
            $arrKeyForProp = $this->_innerConvertPropNameToArrKey($propName);
            switch ($propType) {
                case 'DateTime':
                    $result[$arrKeyForProp] = !empty($this->$propName) ? $this->$propName->format(self::DATE_TIME_OUT_FORMAT) : NULL;
                    break;
                default:
                    $result[$arrKeyForProp] = $this->$propName;
            }
        }
        return array_filter($result, function ($item) { return $item !== NULL; });
    }

    public function __construct(array $data = [])
    {
        if (empty($data)) {
            return;
        }

        $reflection = new \ReflectionClass($this);
        $props = $reflection->getProperties();
        foreach ($props as $prop) {
            $propName = $prop->getName();
            $arrKeyForPropName = $this->_innerConvertPropNameToArrKey($propName);

            if (!isset($data[$arrKeyForPropName])) {
                continue;
            }
            $propType = $prop->getType()->getName();
            switch ($propType) {
                case 'int':
                    $this->innerSetIntProp($propName, $data[$arrKeyForPropName]);
                    break;
                case 'string':
                    $this->innerSetStringProp($propName, $data[$arrKeyForPropName]);
                    break;
                case 'bool':
                    $this->innerSetBoolProp($propName, $data[$arrKeyForPropName]);
                    break;
                case 'DateTime':
                    $this->innerSetDateTimeProp($propName, $data[$arrKeyForPropName]);
                    break;
                default:
                    $this->$propName = $data[$arrKeyForPropName];
            }
        }
    }

    private function innerSetIntProp(string $propName, $propValue): void
    {
        $this->$propName = intval($propValue);
    }
    private function innerSetBoolProp(string $propName, $propValue): void
    {
        $this->$propName = boolval($propValue);
    }
    private function innerSetStringProp(string $propName, $propValue): void
    {
        $this->$propName = strval($propValue);
    }

    private function innerSetDateTimeProp(string $propName, $propValue): void
    {
        if ($propValue instanceof \DateTime) {
            $this->$propName = $propValue;
            return;
        } else {
            if (is_string($propValue)) {
                $timestamp = strtotime($propValue);
            } elseif (is_integer($propValue)) {
                $timestamp = $propValue;
            } else {
                $timestamp = false;
            }
            if (!$timestamp) {
                return;
            }
            $d = new \DateTime();
            $d->setTimestamp($timestamp);
            $this->$propName = $d;
        }
    }

    private function _innerConvertPropNameToArrKey(string $str): string
    {
        $str = str_replace(self::MENUS_REPLACER, '-', $str);
        return str_replace(self::DOT_REPLACER, '.', $str);
    }
    private function _innerConvertArrKeyToPropName(string $str): string
    {
        return str_replace('-', self::MENUS_REPLACER, $str);
    }

}