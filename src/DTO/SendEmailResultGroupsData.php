<?php

namespace Sendsay\ApiClient\DTO;
/**
 * код группы-списка попавших в выпуск. В указанной группе будут накапливаться адреса, успешно вошедшие в список. Эта опция доступна не во всех тарифах!! Возможна ошибка not_allowed_arg
 * @propperty string $issue_member_list
 * В указанную группу-список накапливаются адреса, нажавшие в выпуске "ссылку тематической отписки"
  * Каждый адрес вносится только один первый раз. При последующих выпусках по этому классу участники - это группы-списка из него исключаются.
 * Ссылка тематической отписки оформляется как [% param.url_unsub_topic %]. Также можно использовать ссылку "Отмены тематической отписки",оформляемую как [% param.url_unsub_topic_cancel %].
 * Подписчики, нажавшие её в каком либо имеющимся у них старом письме (новые они не получают из-за нахождения в группе-списке отписки), удаляются из группы отписки и со следующего выпуска по этому
 * классу могут опять получать письма (если нет других препятствий).
 * @propperty string $unsub_list
 */
class SendEmailResultGroupsData extends AbstractDTO
{
    public ?string $issue_member_list = NULL; // уточняйте доступность в вашем тарифе
    public ?string $unsub_list = NULL;
}