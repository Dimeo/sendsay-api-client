<?php

namespace Sendsay\ApiClient\DTO;

class CreateUpdate extends AbstractDTO
{
    public ?\DateTime $time = NULL;
    public ?string $host = NULL;
}