<?php

namespace Sendsay\ApiClient\DTO;

use Sendsay\ApiClient\entity\EntityI;

interface DTOCollectionI extends \Iterator
{
    public function __construct(array $DTOList);
    public function current(): AbstractDTO;
    public function key(): string;
    public function next(): void;
    public function append(AbstractDTO $entity): DTOCollectionI;
}