<?php

namespace Sendsay\ApiClient\DTO\collection;

use Sendsay\ApiClient\DTO\AbstractDTO;
use Sendsay\ApiClient\DTO\Attach;
use Sendsay\ApiClient\DTO\attach\ExcelAttach;
use Sendsay\ApiClient\DTO\attach\OuterBase64FileAttach;
use Sendsay\ApiClient\DTO\attach\OuterFileAttach;
use Sendsay\ApiClient\DTO\attach\OuterUrlAttach;
use Sendsay\ApiClient\DTO\attach\OuterUrlFileAttach;
use Sendsay\ApiClient\DTO\attach\PdfAttach;
use Sendsay\ApiClient\DTO\QuestionAnswer;

class AttachesCollection extends \Sendsay\ApiClient\DTO\AbstractDTOCollection
{
    public function __construct(array $attachesList)
    {
        $this->DTOList = array_filter($attachesList, function ($item) { return $item instanceof Attach; });
    }

    public function current(): Attach
    {
        return current($this->DTOList);
    }

    public function append(AbstractDTO $DTO): AttachesCollection
    {
        $this->DTOList[] = $DTO;
        return $this;
    }

    public function getByIndex(int $index): Attach
    {
        return $this->DTOList[$index];
    }
    public function remove(AbstractDTO $DTO): AttachesCollection
    {
        $this->DTOList = array_filter($this->DTOList, function ($currentDtoItem) use ($DTO) {
            return $currentDtoItem != $DTO;
        });
        return $this;
    }

    public function removeByIndex(int $index): AttachesCollection
    {
        try {
            array_splice($this->DTOList, $index,1);
        } catch (\Exception $e) {
            // do nothing
        }
        return $this;
    }

    public static function createFromArray(array $data): AttachesCollection
    {
        $theList = [];
        foreach ($data as $dItem) {
            if (isset($dItem['xlsx'])) {
                $theList[] = new ExcelAttach($dItem);
                continue;
            }
            if (isset($dItem['pdf'])) {
                $theList[] = new PdfAttach($dItem);
                continue;
            }
            if (isset($dItem['url'])) {
                $theList[] = new OuterUrlFileAttach($dItem);
                continue;
            }
            if (isset($dItem['encoding'])) {
                $theList[] = new OuterBase64FileAttach($dItem);
                continue;
            }
            $theList[] = new OuterFileAttach($dItem);
        }
        return new AttachesCollection($theList);
    }
}