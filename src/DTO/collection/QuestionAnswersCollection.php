<?php

namespace Sendsay\ApiClient\DTO\collection;

use Sendsay\ApiClient\DTO\AbstractDTO;
use Sendsay\ApiClient\DTO\AbstractDTOCollection;
use Sendsay\ApiClient\DTO\QuestionAnswer;

class QuestionAnswersCollection extends \Sendsay\ApiClient\DTO\AbstractDTOCollection
{
    public function __construct(array $questionAnswersList)
    {
        $this->DTOList = array_filter($questionAnswersList, function ($item) { return $item instanceof QuestionAnswer; });
    }

    public function current(): QuestionAnswer
    {
        return current($this->DTOList);
    }

    public function append(AbstractDTO $DTO): QuestionAnswersCollection
    {
        $this->DTOList[] = $DTO;
        return $this;
    }

    public function getByIndex(int $index): QuestionAnswer
    {
        return $this->DTOList[$index];
    }
    public function getByAnswerCode(string $answerCode): QuestionAnswer
    {
        try {
            $index = array_keys(array_filter($this->DTOList, function ($answer) use ($answerCode) {
                /** @var QuestionAnswer $answer */
                return $answer->answerCode == $answerCode;
            }))[0];
            return $this->DTOList[$index];
        } catch (\Exception $e) {
            throw new \RuntimeException('unable to get answer of code: ' . $answerCode);
        }
    }

    public function remove(AbstractDTO $DTO): QuestionAnswersCollection
    {
        $this->DTOList = array_filter($this->DTOList, function ($currentDtoItem) use ($DTO) {
            return $currentDtoItem != $DTO;
        });
        return $this;
    }

    public function removeByIndex(int $index): QuestionAnswersCollection
    {
        try {
            array_splice($this->DTOList, $index,1);
        } catch (\Exception $e) {
            // do nothing
        }
        return $this;
    }


    public static function createFromArray(array $data): QuestionAnswersCollection
    {
        $theList = [];
        foreach ($data as $answerCode => $answerText) {
//            if (!is_array($itemParams)) {
//                continue;
//            }
            try {
                $theList[] = new QuestionAnswer([],$answerCode, $answerText);
            } catch (\RuntimeException $e) {
                continue;
            }
        }
        return new QuestionAnswersCollection($theList);
    }

    public function toArray(): array
    {
        $result = [];
        foreach ($this->DTOList as $key => $entity) {
            /** @var QuestionAnswer $entity */
//            $result[$key] = $entity->toArray();
            $result[$entity->answerCode] = $entity->answerText;
        }
        return $result;
    }

}