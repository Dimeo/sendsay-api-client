<?php

namespace Sendsay\ApiClient\DTO;

class Tg extends AbstractDTO
{
    public ?\DateTime $sub = NULL;
    public ?\DateTime $unsub = NULL;
    public ?\DateTime $err = NULL;

    public function getSubString(): string
    {
        if (empty($this->sub)) {
            return '';
        }
        return $this->sub->format(self::DATE_TIME_OUT_FORMAT);
    }

    public function getUnsubString(): string
    {
        if (empty($this->unsub)) {
            return '';
        }
        return $this->unsub->format(self::DATE_TIME_OUT_FORMAT);
    }
    public function getErrString(): string
    {
        if (empty($this->err)) {
            return '';
        }
        return $this->err->format(self::DATE_TIME_OUT_FORMAT);
    }
}