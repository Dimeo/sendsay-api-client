<?php

namespace Sendsay\ApiClient\DTO\message;

use Sendsay\ApiClient\DTO\AbstractDTO;

class VkMessage extends AbstractDTO
{
    public string $vk;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function toArray(): array
    {
        if (empty($this->vk)) {
            throw new \RuntimeException('Vk text must be NOT empty');
        }
        return parent::toArray();
    }
}