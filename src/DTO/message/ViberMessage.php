<?php

namespace Sendsay\ApiClient\DTO\message;

use Sendsay\ApiClient\DTO\AbstractDTO;

class ViberMessage extends AbstractDTO
{
    public string $viber;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function toArray(): array
    {
        if (empty($this->viber)) {
            throw new \RuntimeException('Viber text must be NOT empty');
        }
        return parent::toArray();
    }
}