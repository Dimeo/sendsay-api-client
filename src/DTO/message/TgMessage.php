<?php

namespace Sendsay\ApiClient\DTO\message;

use Sendsay\ApiClient\DTO\AbstractDTO;

class TgMessage extends AbstractDTO
{
    public string $tg;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function toArray(): array
    {
        if (empty($this->tg)) {
            throw new \RuntimeException('Tg text must be NOT empty');
        }
        return parent::toArray();
    }
}