<?php

namespace Sendsay\ApiClient\DTO\message;

use Sendsay\ApiClient\DTO\AbstractDTO;

class SmsMessage extends AbstractDTO
{
    public string $sms;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function toArray(): array
    {
        if (empty($this->sms)) {
            throw new \RuntimeException('SMS text must be NOT empty');
        }
        return parent::toArray();
    }
}