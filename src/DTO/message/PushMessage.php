<?php

namespace Sendsay\ApiClient\DTO\message;

use Sendsay\ApiClient\DTO\AbstractDTO;

class PushMessage extends AbstractDTO
{
    public string $push;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function toArray(): array
    {
        if (empty($this->push)) {
            throw new \RuntimeException('Push text must be NOT empty');
        }
        return parent::toArray();
    }
}