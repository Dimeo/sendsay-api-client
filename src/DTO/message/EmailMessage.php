<?php

namespace Sendsay\ApiClient\DTO\message;

use Sendsay\ApiClient\DTO\AbstractDTO;

/**
 * хотя бы одна из версий сообщения должна быть не пустой
 */
class EmailMessage extends AbstractDTO
{
    public string $html = '';
    public string $amp = '';
    public string $text = '';

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function toArray(): array
    {
        if (empty($this->html) && empty($this->text) && empty($this->amp)) {
            throw new \RuntimeException('at least one of properties must be NOT empty');
        }
        return array_filter(parent::toArray(),function ($i) { return !empty($i); });
    }
}