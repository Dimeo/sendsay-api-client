<?php

namespace Sendsay\ApiClient\DTO;

use http\Exception\RuntimeException;

/**
 * Объект, описывающий параметр tz_observance, который используется для
 * Учёт часового пояса получателя.
 * В документации видим:
 * Для не SMS при отсутствии учёта часового пояса всё выходит сразу (если нет прочих настроек).
 *
 * Для SMS при отсутствии явно заданного учёта часового пояса он учитывает на основании номера телефона.

 * Не совместимо с tz_best.

 * При выпуске с учётом временной зоны, письма будут заранее сформированы, но их первая попытка
 * доставки начнётся не ранее, чем во временной зоне получателя начнётся указанный час
 * по его локальному времени.
 *
 * Источником данных может быть один или несколько пунктов анкеты подписчика.
 *
 * Берётся первое определившееся время по порядку указания источников.
 *
 * Если часовой пояс не удалось определить ни по одному из источников, то письмо начинает
 * доставляться как описано в default (с учётом tz_limit).
 *
 * В качестве указания на часовой пояс понимаются названия большинства стран и крупных городов
 * в английском и русском написании (регистр не важен).
 *
 * А также числовые смещения относительно UTC в виде:
 *
 *           +hhmm  -hhmm  UTC+hhmm  UTC-hhmm
 *           +hh    -hh    UTC+hh    UTC-hh
 *           +h     -h     UTC+h     UTC-h
 *           hh
 *           h
 *
 * Используйте вызов sys.settings.get, чтобы проверить понимает ли система ваш способ записи.
 * Если нет - обратитесь в Службу поддержки за расширением нашего списка городов и стран.
 *
 * Во избежание путаницы с пониманием часовых поясов, настоятельно рекомендуется планировать
 * такие рассылки не менее чем за 24 часа и учитывать особенности России - текущий час в Москве
 * по всех других часовых поясах страны (за исключением Калининграда) уже прошёл и настанет
 * там через через 15-23 часа в зависимости от удалённости.
 *
 * При совместном использовании с tz_limit ограничение на частоту отправки действует независимо в каждом
 * часовом поясе.
 *
 * Настройка может быть указана и будет учтена по убыванию приоритета: при выпуске, в классе выпуска, в черновике, в классе черновика, глобально (sys.settings.set)
 *
 * Если требуется отключить соблюдение местного времени, то в более приоритетном источнике необходимо задать tz_observance.source с не существующим ключом данных.
 *
 */
class TzObservance extends AbstractDTO
{
    /**
     * час по местному времени для начала доставки писем
     * не обязательно, по умолчанию - час из default
     */
    public ?int $hour = NULL; // 0-23

    /**
     * Источники данных о временной зоне подписчика в его анкете. Обязательно. Один или более.
     */
    public array $source = ['base.tz', 'member.last.tz'];

    /**
     * час начала для тех, у кого местное время не определилось
     * параметр необязателен, по умолчанию - текущий час для выпуска "сейчас"
     * или час запуска для отложенного выпуска - т.е. для них письма будут
     * отправляться сразу при выпуске
     * если час указан, то письма начнут отправлять, начиная с указанного часа сегодня,
     * а если он уже полностью прошёл - с этого часа завтра
     */
    public ?int $default = NULL; // 0-23


    public function toArray(): array
    {
        if (NULL !== $this->default && ($this->default < 0 || $this->default > 23)) {
            throw new RuntimeException('hour can be between 0 and 23 only');
        }

        if (NULL !== $this->hour && ($this->hour < 0 || $this->hour > 23)) {
            throw new RuntimeException('hour can be between 0 and 23 only');
        }
        return parent::toArray();
    }

}