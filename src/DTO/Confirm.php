<?php

namespace Sendsay\ApiClient\DTO;

class Confirm extends AbstractDTO
{
    public ?bool $lock = NULL;
    public ?\DateTime $time = NULL;
    public ?string $host = NULL;
    
}