<?php

namespace Sendsay\ApiClient\DTO;

use Sendsay\ApiClient\entity\ArrayableI;
use Sendsay\ApiClient\entity\EntityCollectionI;
use Sendsay\ApiClient\entity\EntityI;

/**
 * @property AbstractDTO[] $DTOList
 */
abstract class AbstractDTOCollection implements DTOCollectionI, ArrayableI
{
    protected array $DTOList = [];

    public function __construct(array $DTOList)
    {
        $this->DTOList = array_filter($DTOList, function ($item) { return $item instanceof AbstractDTO; });
    }

    public function key(): string
    {
        return key($this->DTOList);
    }
    public function count(): int
    {
        return count($this->DTOList);
    }

    public function next(): void
    {
        next($this->DTOList);
    }

    public function current(): AbstractDTO
    {
        return current($this->DTOList);
    }

    public function rewind(): void
    {
        reset($this->DTOList);
    }

    public function valid(): bool
    {
        return key($this->DTOList) !== NULL;
    }

    public function append(AbstractDTO $DTO): AbstractDTOCollection
    {
        $this->DTOList[] = $DTO;
        return $this;
    }

    public function toArray(): array
    {
        $result = [];
        foreach ($this->DTOList as $key => $entity) {
            $result[$key] = $entity->toArray();
        }
        return $result;
    }

}