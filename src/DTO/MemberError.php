<?php

namespace Sendsay\ApiClient\DTO;

class MemberError extends AbstractDTO
{
    public int $lock = 1; // заблокирован из-за ошибок доставки (1) или нет (0)
    public ?\DateTime $date = NULL; //дата последней ошибки доставки (Ys) (удаляется при первой же успешной доставке)
    public ?string $str = NULL; // описание последней ошибки доставки (удаляется при первой же успешной доставке)
    public int $error = 0; // общее число ошибок доставки, произошедших подряд (устанавливается в 0 при первой же успешной доставке)
    public ?int $issue = NULL; // выпуск последней ошибки доставки
    public ?int $letter = NULL; // письмо последней ошибки доставки
    public ?int $lock_issue = NULL; // выпуск приведший к блокировке
    public ?int $lock_letter = NULL; //письмо приведшее к блокировке


    public function getDateString(): string
    {
        if (empty($this->date)) {
            return '';
        }
        return $this->date->format(self::DATE_TIME_OUT_FORMAT);
    }
}