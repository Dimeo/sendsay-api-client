<?php

namespace Sendsay\ApiClient\DTO;

use http\Exception\RuntimeException;



/**
 * Объект, описывающий параметр contact_rate, который используется для
 * Ограничение количества контактов на получателя.
 * Выдержка из документации:
 * ограничивает число высылаемых сообщений для каждого получателя
 * если за последний interval получателю уже сформировано max и более сообщений, то он исключается из текущего выпуска
 * если у выпуска есть класс (указан или получен из черновика), то в расчёт лимита берутся только выпуски, вышедшие по такому же классу
 * если у выпуска нет класса , то в расчёт лимита берутся вообще все выпуски
 * должны быть заданы оба параметра сразу или ни одного
 * параметр необязателен, по умолчанию не ограничено
 */
class ContactRate extends AbstractDTO
{
    const INTERVAL_PATTERN = '/^\-1$|^\d+d?$/'; //

    /**
     * -1 - за все время
     * Число >1 - за сколько последних часов проверять.
     * Число >1 с префиксом "d", например "2d" - за сколько последних дней проверять. День начинается в 00:00:00
     * Примеры:
     * 2d - за вчера и сегодня, т.е. с 00:00:00 вчера
     * 1d - за сегодня, т.е. с 00:00:00 сегодня
     */
    public ?string $interval = '-1';

    /**
     * "число" -- лимит сообщений. число от 1
     */
    public ?int $max = NULL;


    public function toArray(): array
    {
        $dataIsOk = (!empty($this->interval) && !empty($this->max)) || (empty($this->source) && empty($this->max));
        if (!$dataIsOk) {
            throw new RuntimeException(self::class . ' Both properties "source" and "max" must be either empty neither not empty simultaneously');
        }

        if (!boolval(preg_match(self::INTERVAL_PATTERN, $this->interval))) {
            throw new RuntimeException(self::class . ': "' .  $this->interval .  '" is unacceptable value for property "interval" ');
        }

        if ($this->max < 1) {
            throw new RuntimeException(self::class . ': "' .  $this->max .  '" is unacceptable value for property "max" ');
        }

        return parent::toArray();
    }

}