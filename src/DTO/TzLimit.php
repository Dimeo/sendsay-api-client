<?php

namespace Sendsay\ApiClient\DTO;

use http\Exception\RuntimeException;

/**
 * Объект, описывающий параметр tz_limit, который используется для
 * ограничения на частоту отправки.
 * Согласно документации:
 * Все ограничения касаются только первой попытки отправки сообщения.
 * Если сообщение не принято сразу, то ограничения могут не быть соблюдены.
 *
 * При совместном использовании с tz_observance, ограничение на частоту отправки действует независимо в каждом
 * часовом поясе.
 *
 * Для SMS частота автоматически ограничивается в каждом часовом поясе отдельно и без tz_observance.
 */
class TzLimit extends AbstractDTO
{
    const MIN_NUMBERS = 200;
    const MIN_RATE = 0;
    // number и rate должны быть либо оба указаны либо оба пустыми
    // за rate минут будет передано в доставку в не более number сообщений
    // начиная со второго раза, number будет увеличиваться на increase каждый раз от текущего значения
    // Если параметр отсутствует или пуст или обе величины равны 0, то ограничения нет.
    public ?int $number = NULL; // сколько cообщений" -- обязательно, >= 200
    public ?int $rate = NULL; // "за сколько минут"  -- обязательно, >= 0

    // если число, то увеличение number на указанное целое число, >= 0
    // если процент, то увеличение number на указанное число процентов
    // процент принимается с точностью до сотых
    public ?string $increase = NULL;
    public bool $usePercents = false;

    public function toArray(): array
    {
        if (!empty($this->increase)) {
            $this->increase = strval(intval($this->increase));
            if ($this->usePercents) {
                $this->increase = $this->increase . '%';
            }
        }

        if ( (!empty($this->number) && empty($this->rate)) || (empty($this->number) && !empty($this->rate)) ) {
            throw new RuntimeException(self::class . ': Both "number" and "rate" must be either empty neither set simultaneously.');
        }
        if ($this->number !== NULL && $this->number < self::MIN_NUMBERS) {
            $this->number = self::MIN_NUMBERS;
        }

        if ($this->rate !== NULL && $this->rate < self::MIN_RATE) {
            $this->rate = self::MIN_RATE;
        }

        if ($this->increase !== NULL && $this->increase < 0) {
            $this->increase = 0;
        }

        $result =  parent::toArray();
        if (isset($result['usePercents'])) {
            unset($result['usePercents']);
        }

        if (empty($result)) {
            return [];
        }

        $result = ['default' => [
            'batch' => $result
        ]];
        return $result;
    }

}