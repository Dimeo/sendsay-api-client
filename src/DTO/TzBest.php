<?php

namespace Sendsay\ApiClient\DTO;

use http\Exception\RuntimeException;

/**
 * Объект, описывающий параметр tz_best, который используется для
 * оысылки письма в наиболее подходящее для каждого получателя время индивидуально на основе его прошлой активности.
 * Выдержка из документации:
 * для активации данной возможности просто обратитесь в Службу поддержки
 * например,
 *   day и 4   - учитывая статистику дней недели, выбрать наилучшее время в ближайшие 4 часа
 *   week и 12 -учитывая суммарную статистику недели, выбрать наилучшее время в ближайшие 12 часа
 *
 * если используется day и в указанном интервале вообще не было активности,
 * то алгоритм переключается на week, при этом interval устанавливается в 24, если он был более 24
 * если в указанном интервале лучшими являются несколько часов, то выбирается самый ранний
 * не совместимо с выпуском по часовым поясам tz_observance
 * должны быть заданы оба параметра сразу или ни одного
 * параметр default необязателен и не учитывается без source и interval
 */
class TzBest extends AbstractDTO
{
    /**
     * "day" или "week" -- лучшее время определять с учётом дня недели выпуска рассылки (day)
     * или по суммарным данным всех дней недели (week).
     * По умолчанию - day
     */
    public ?string $source = 'day';

    /**
     * число
     * от 1 до 24 для week и от 1 до 168 для day
     * число ближайших от выпуска часов, среди которых искать наилучший (считая и час выпуска).
     * По умолчанию - 4
     */
    public ?int $interval = 4;

    /**
     * от 00 до 23
    -- час начала для тех, у кого время не определилось
    -- параметр необязателен, по умолчанию - текущий час для выпуска "сейчас"
    -- или час запуска для отложенного выпуска - т.е. для них письма будут
    -- отправляться сразу при выпуске
    -- если час указан, то начнут отправлять им письма, начиная с указанного часа сегодня,
    -- а если он уже полностью прошёл - с этого часа завтра
     */
    public ?int $default = NULL; // 0-23


    public function toArray(): array
    {
        $dataIsOk = (!empty($this->interval) && !empty($this->source)) || (empty($this->source) && empty($this->interval));
        if (!$dataIsOk) {
            throw new RuntimeException(self::class . ' Both properties "source" and "interval" must be either empty neither not empty simultaneously');
        }

        if (!in_array($this->source,['day', 'week'])) {
            throw new RuntimeException(self::class . ' Properties "source" must has value "day" or "week" only');
        }

        if ($this->source == 'day' && ($this->interval < 1 && $this->interval > 168)) {
            throw new RuntimeException(self::class . ' Properties "interval" must be between 1 and 168 when "source" is "day"');
        }
        if ($this->source == 'week' && ($this->interval < 1 && $this->interval > 24)) {
            throw new RuntimeException(self::class . ' Properties "interval" must be between 1 and 24 when "source" is "week"');
        }

        if (NULL !== $this->default && ($this->default < 0 || $this->default > 23)) {
            throw new RuntimeException(self::class . ' Property "default" must be between 0 and 23 or NULL');
        }

        return parent::toArray();
    }

}