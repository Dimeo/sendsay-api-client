<?php

namespace Sendsay\ApiClient\DTO\attach;

use Sendsay\ApiClient\DTO\AbstractDTO;
use Sendsay\ApiClient\DTO\Attach;

class PdfAttach extends Attach
{
    public int $pdf; // номер черновика
}