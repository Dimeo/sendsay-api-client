<?php

namespace Sendsay\ApiClient\DTO\attach;

use Sendsay\ApiClient\DTO\AbstractDTO;
use Sendsay\ApiClient\DTO\Attach;

class OuterUrlFileAttach extends Attach
{
    public string $url; // персонализированный url откуда забирать - для каждого свой
    public ?string $mime___type = NULL;
    public ?string $charset = NULL;
}