<?php

namespace Sendsay\ApiClient\DTO\attach;

use Sendsay\ApiClient\DTO\AbstractDTO;
use Sendsay\ApiClient\DTO\Attach;

class OuterFileAttach extends Attach
{
    public string $name;
    public string $content;
    public string $mime___type;
    public string $charset;

}