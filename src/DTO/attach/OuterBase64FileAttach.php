<?php

namespace Sendsay\ApiClient\DTO\attach;

use Sendsay\ApiClient\DTO\AbstractDTO;
use Sendsay\ApiClient\DTO\Attach;

class OuterBase64FileAttach extends OuterFileAttach
{
    const PRE_VALUE = 'base64';
    public string $encoding = self::PRE_VALUE;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        $this->encoding = self::PRE_VALUE;
    }
}