<?php

namespace Sendsay\ApiClient\DTO\attach;

use Sendsay\ApiClient\DTO\AbstractDTO;
use Sendsay\ApiClient\DTO\Attach;

class ExcelAttach extends Attach
{
    public int $xlsx; // номер черновика
}