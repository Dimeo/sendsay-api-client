<?php

namespace Sendsay\ApiClient\DTO;
/**
 * Все присутствующие здесь опции доступны НЕ во всех тарифах, поэтому возможна ошибка not_allowed_arg от АПИ при попытке их использования
 * @propperty string $issue_include_filter фильтр включения в выпуск - только адреса, попадающие под этот фильтр будут пропущены в выпуск.
 * @propperty string $issue_exclude_filter фильтр исключения из выпуска - адреса, попадающие под этот фильтр будут исключены из выпуска.
 * @propperty array $group_DOT_exclude Группы списки для исключения получателей. Если адрес получателя присутствует хотя бы в одной из указанных групп-списков, то он исключается из выпуска
 */
class SendEmailFiltersData extends AbstractDTO
{
    public ?string $issue_include_filter = NULL; // уточняйте доступность в вашем тарифе Sendsay
    public ?string $issue_exclude_filter = NULL; // уточняйте доступность в вашем тарифе Sendsay
    public ?array $group_DOT_exclude = NULL; // уточняйте доступность в вашем тарифе Sendsay
}