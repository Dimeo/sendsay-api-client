<?php

namespace Sendsay\ApiClient\DTO;
use Sendsay\ApiClient\enum\IssueSendWhen;

/**
 */
class SendEmailSendWhenData extends AbstractDTO
{
    public IssueSendWhen $sendwhen;
    public ?\DateTime $sendAt = NULL;
    public ?int $delayMinutes = NULL;
}