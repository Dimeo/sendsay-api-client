<?php

namespace Sendsay\ApiClient\DTO;

use \RuntimeException;
use Sendsay\ApiClient\helpers;

class QuestionAnswer extends AbstractDTO
{
    public string $answerCode;
    public string $answerText;

    /**
     * Возможные варианты:
     * (['someCode', 'Текст ответа'])
     * (['answerCode' => 'someCode', 'answerText' => 'вариант ответа'])
     * ([], 'someCode', 'Вариант ответе')
     * @param array $data
     * @param string|null $code
     * @param string|null $text
     * @throws \RuntimeException
     */
    public function __construct(array $data, ?string $code = NULL, ?string $text = NULL)
    {
        if (!empty($data)) {
            if (helpers::arrayIsList($data))  {
                if (count($data) !== 2) {
                    throw new RuntimeException('The array of length 2 elements is required.');
                }
                $this->answerCode = strval($data[0]);
                $this->answerText = strval($data[1]);
            } else {
                $this->answerCode = $data['answerCode'] ?? '';
                $this->answerText = $data['answerText'] ?? '';
            }
        } else {
            if (NULL == $code || NULL === $text) {
                throw new RuntimeException('Both answerCode and answerText must be supplied');
            }
            $this->answerCode = $code;
            $this->answerText = $text;
        }
        if (empty($this->answerCode) || empty($this->answerText)) {
            throw new RuntimeException('Both answerCode and answerText cannot has empty values');
        }
    }

    public function toArray(): array
    {
        return [$this->answerCode => $this->answerText];
    }
}