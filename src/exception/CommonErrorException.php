<?php

namespace Sendsay\ApiClient\exception;

class CommonErrorException extends \RuntimeException implements SendSayApiExceptionI
{
    const DEFAULT_MESSAGE = 'An error occurred!';
    public function __construct(string $message = '', $code = 0, \Throwable $previous = null)
    {
        if (empty($message)) {
            $message = self::DEFAULT_MESSAGE . "\n" . $message;
        }
        parent::__construct($message, $code, $previous);
    }
}