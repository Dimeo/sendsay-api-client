<?php

namespace Sendsay\ApiClient\exception;

use Sendsay\ApiClient\entity\AbstractEntity;
use Sendsay\ApiClient\entity\EntityI;

class EntityValidationErrorException extends \RuntimeException implements SendSayApiExceptionI
{
    private array  $errorsList = [];
    public function __construct(EntityI $entity, $message = "", $code = 0, \Throwable $previous = null)
    {
        $this->errorsList = $entity->getValidationErrors();

        if (empty($message)) {
            $message = 'Entity ' . $entity->getEntityName() . ' validation failed. ' . $this->getErrorsListAsString();
        }
        parent::__construct($message, $code, $previous);
    }

    public function getErrorsList(): array
    {
        return $this->errorsList;
    }

    public function getErrorsListAsString(): string
    {
        return print_r($this->errorsList, true);
    }

}