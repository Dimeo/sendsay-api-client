<?php

namespace Sendsay\ApiClient\exception;

class ApiResponseErrorException extends \RuntimeException implements SendSayApiExceptionI
{
    const DEFAULT_MESSAGE = 'sendSay api responsed with errors.';
    private array  $errorsList = [];
    public function __construct(array $errorsList, string $message = '', $code = 0, \Throwable $previous = null)
    {
        $this->errorsList = $errorsList;
        if (empty($message)) {
            $message = self::DEFAULT_MESSAGE . "\n" . $this->getErrorsListAsString();
        }
        parent::__construct($message, $code, $previous);

    }

    public function getErrorsList(): array
    {
        return $this->errorsList;
    }

    public function getErrorsListAsString(): string
    {
        return print_r($this->errorsList, true);
    }

}