<?php

namespace Sendsay\ApiClient;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use Laminas\Cache\Exception\ExceptionInterface;
use Laminas\Cache\Storage\Adapter\Filesystem;
use Laminas\Cache\Storage\Adapter\FilesystemOptions;
use Psr\Http\Message\RequestInterface;
use Sendsay\ApiClient\enum\AuthMethod;
use Sendsay\ApiClient\exception\ApiResponseErrorException;


class HttpClient implements HttpClientI
{
    const API_URL = 'https://api.sendsay.ru/general/api/v100/json/';
    const ALLOW_REDIRECTS = true;
    const SESSION_CACHE_KEY = 'sessionID';
    private \GuzzleHttp\Client $guzzle;
    private AuthConfig $authConfig;
    private Filesystem $cache;
    private $guzzleClientOptions = [
        'base_uri' => '',
        'timeout' => 60,
        'handler' => NULL,
    ];

    private $cacheOptions = [
        'ttl' => 60 * 60 * 12,
        'namespace' => 'sendSayApi',
        'namespace_separator' => ':',
        'cache_dir' => '', // string default ""	Directory to store cache files.
        'clear_stat_cache' => true, // boolean default	true	Call clearstatcache() enabled?
        'dir_level' => 1, // integer	default 1	Defines how much sub-directories should be created.
        'dir_permission' => 0755, // integer	default false	0700 Set explicit permission on creating new directories.
        'file_locking' => true, // boolean	default true	Lock files on writing.
        'file_permission' => 0600, // integer	default false	0600 Set explicit permission on creating new files.
        // 'key_pattern' => '', // string	default /^[a-z0-9_\+\-]*$/Di	Validate key against pattern.
        'no_atime' => true, // boolean	default true	Don’t get ‘fileatime’ as ‘atime’ on metadata.
        'no_ctime' => true, //boolean	default true	Don’t get ‘filectime’ as ‘ctime’ on metadata.
        'umask' => false, //integer|false	default false	Use umask to set file and directory permissions.
        'suffix' => 'dat', //string	default dat	Suffix for cache files
        'tag_suffix' => 'tag',    // string	tag	Suffix for tag files
    ];


    public function __construct(AuthConfig $authConfig, ?string $cacheDirPath)
    {
        if (empty($cacheDirPath)) {
            $cacheDirPath = __DIR__ . '/cache';
        }
        $this->guzzleClientOptions['base_uri'] = self::API_URL . $authConfig->login;

        if (!is_dir($cacheDirPath)) {
            mkdir($cacheDirPath, 0755, true);
        }
        $this->authConfig = $authConfig;
        $this->cacheOptions['cache_dir'] = $cacheDirPath;
        $this->init();
    }

    private function init(): void
    {
        try {
            $options = new FilesystemOptions($this->cacheOptions);
            $this->cache = new Filesystem($options);
        } catch (Exception $e) {
            $a = 12;
            //do nothing
        }

        $authHeader = $this->buildAuthHeaders();
        $handlersStack = new HandlerStack();
        $handlersStack->setHandler(new CurlHandler());
        $handlersStack->push(Middleware::mapRequest(function (RequestInterface $request) use ($authHeader) {
            return $request->withHeader($authHeader['name'], $authHeader['value']);
        }));
        $this->guzzleClientOptions['handler'] = $handlersStack;
        $this->guzzle = new \GuzzleHttp\Client($this->guzzleClientOptions);
        return;
    }

    private function buildAuthHeaders(): array
    {
        $authConfig = $this->authConfig;
        $result = ['name' => 'Authorization', 'value' => ''];
        $authMethod = $authConfig->getAvailableAuthMethod();
        if ($authMethod == AuthMethod::LOGIN_PWD()) {
            $result['value'] = 'sendsay session=' . $this->getSessionId($authConfig);
        } elseif ($authMethod == AuthMethod::API_KEY()) {
            $result['value'] = 'sendsay apikey=' . $authConfig->apikey;
        }

        return $result;

    }

    private function getSessionId(AuthConfig $authConfig): string
    {
        if (!empty($this->cache)) {
            try {
                $cached = $this->cache->getItem(self::SESSION_CACHE_KEY);
                if (!empty($cached) && $this->checkSessionAuth($cached)) {
                    return $cached;
                }
            } catch (ExceptionInterface $e) {
                // do nothing. Just ignore cache
            }
        }
        $loginRequest = new Request('POST', $this->guzzleClientOptions['base_uri']);
        $loginRequestData = [
            'action' => 'login',
            'login' => $authConfig->login,
            'sublogin' => $authConfig->sublogin,
            'passwd' => $authConfig->password,
        ];
        $client = new \GuzzleHttp\Client(['timeout' => 60]);
        try {
            $response = $client->send($loginRequest, ['allow_redirects' => self::ALLOW_REDIRECTS, 'json' => $loginRequestData]);
            $apiResponse = new ApiResponse($response);
            $sessionId = $apiResponse->data['session'] ?? '';
            try {
                $this->cache->setItem(self::SESSION_CACHE_KEY, $sessionId);
            } catch (ExceptionInterface $e) {
                throw $e;
            }
        } catch (GuzzleException $e) {
            $sessionId = '';
        }
        return $sessionId;
    }
    private function logout(string $sessionId): bool
    {
        $logoutRequest = new Request('POST', $this->guzzleClientOptions['base_uri']);
        $logoutRequestData = [
            'action' => 'logout',
            'session' => $sessionId,
        ];
        $client = new \GuzzleHttp\Client(['timeout' => 60]);
        $response = $client->send($logoutRequest, ['allow_redirects' => self::ALLOW_REDIRECTS, 'json' => $logoutRequestData]);
        return $response->getStatusCode() == 200;
    }

    private function checkSessionAuth(string $sessionId): bool
    {
        $data = ['allow_redirects' => self::ALLOW_REDIRECTS, 'json' => ['action' => 'pong', 'session' => $sessionId]];
        try {
            $checkAuthRequest = new Request('POST', $this->guzzleClientOptions['base_uri']);
            $client = new \GuzzleHttp\Client(['timeout' => 60]);
            $resp = $client->send($checkAuthRequest, $data);
        } catch (GuzzleException $e) {
            return false;
        }
        return $resp->getStatusCode() == 200;
    }

    /**
     * @param array $data
     * @return ApiResponse
     * @throws GuzzleException
     * @throws ApiResponseErrorException
     */
    public function sendRequest(array $data): ApiResponse
    {
        try {
            $resp = $this->guzzle->request('POST', '', ['allow_redirects' => self::ALLOW_REDIRECTS, 'json' => $data]);
        } catch (GuzzleException $e) {
            throw $e;
        }
        $result = new ApiResponse($resp);
        if ($result->hasErrors()) {
            throw new ApiResponseErrorException($result->getErrors());
        }
        return $result;
    }
}