<?php

namespace Sendsay\ApiClient;

use Psr\Http\Message\ResponseInterface;
use Sendsay\ApiClient\entity\ArrayableI;

class ApiResponse implements ArrayableI
{
    const COMMON_FIELDS_LIST = ['request.id', 'duration', 'errors'];
    public string $request_id;
    public float $duration;
    public array $errors = [];
    public array $data;

    public function __construct(ResponseInterface $response)
    {
        $statusCode = $response->getStatusCode();
        if ($statusCode != 200) {
//            throw new \Exception('sendSay Api has responsed by status code ' . $statusCode, 400);
            throw new \RuntimeException('sendSay Api has responsed by status code ' . $statusCode, 400);
        }
        $responseContent = $response->getBody()->getContents();
        $responseContent = json_decode($responseContent, true);
        $this->init($responseContent);
    }

    private function init(array $data): void
    {
        foreach (self::COMMON_FIELDS_LIST as $keyName) {
            $propName = str_replace('.', '_', $keyName);
            if (isset($data[$keyName])) {
                $this->$propName = $data[$keyName];
            }
            unset($data[$keyName]);
        }
        $this->data = $data;
    }

    public function hasErrors(): bool
    {
        return !empty($this->errors);
    }

    public function getErrors(): array
    {
        return $this->errors ?? [];
    }

    public function getData(): array
    {
        return $this->data ?? [];
    }
    
    public function toArray(): array
    {
        return [
            'request_id' => $this->request_id,
            'duration' => $this->duration,
            'errors' => $this->errors,
            'data' => $this->data,
        ];
    }
}