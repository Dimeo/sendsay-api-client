<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 * UsersUrlRemove enum
 *
 * @extends Enum<UsersUrlRemove::*>
 * @method static UsersUrlRemove NONE() не удалять
 * @method static UsersUrlRemove ALWAYS() всегда
 * @method static UsersUrlRemove ONERROR() при завершении с ошибкой
 * @method static UsersUrlRemove ONOK() при завершении без ошибок
 */
final class UsersUrlRemove extends Enum
{
    private const NONE = 'none';
    private const ALWAYS = 'always';
    private const ONERROR = 'onerror';
    private const ONOK = 'onok';
}
