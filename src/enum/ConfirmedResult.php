<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 * ConfirmedResult enum
 *
 * @extends Enum<ConfirmedResult::*>
 * @method static ConfirmedResult ERROR()
 * @method static ConfirmedResult MEMBER()
 * @method static ConfirmedResult WRONGCOOKIE()
 * @method static ConfirmedResult SUCCESS()
 */
final class ConfirmedResult extends Enum
{
    private const ERROR = 'error';
    private const MEMBER = 'member';
    private const WRONGCOOKIE = 'wrongcookie';
    private const SUCCESS = 'success';
}
