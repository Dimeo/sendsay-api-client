<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 * list of available objects for campaign
 *
 * @extends Enum<ObjForCampaignType::*>
 * @method static ObjForCampaignType ISSUE()
 * @method static ObjForCampaignType CRON()
 * @method static ObjForCampaignType SPLIT()
 * @method static ObjForCampaignType FORM()
 * @method static ObjForCampaignType SEQUENCE()
 * @method static ObjForCampaignType LINKGROUP()
 * @method static ObjForCampaignType DATAROW()
 */
final class ObjForCampaignType extends Enum
{
    private const ISSUE = 'issue';
    private const CRON = 'cron';
    private const SPLIT = 'split';
    private const FORM = 'form';
    private const SEQUENCE = 'sequence';
    private const LINKGROUP = 'linkgroup';
    private const DATAROW = 'datarow';
}
