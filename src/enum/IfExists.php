<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 * IfExists enum
 *
 * @extends Enum<IfExists::*>
 * @method static IfExists ERROR()
 * @method static IfExists MUST()
 * @method static IfExists UPDATE()
 * @method static IfExists OVERWRITE()
 */
final class IfExists extends Enum
{
    private const ERROR = 'error';
    private const MUST = 'must';
    private const UPDATE = 'update';
    private const OVERWRITE = 'overwrite';
}
