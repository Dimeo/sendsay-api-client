<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 *  *
 * @extends Enum<AccumulateBy::*>
 * @method static AccumulateBy MONTH()
 * @method static AccumulateBy WEEK()
 * @method static AccumulateBy DAY()
 * Класс, содержащий список допустимых значений для параметра accumulate_by
 * Используется для объединения экспресс-выпусков
 * Обозначает интервал накопления. Параметр необязателен. По умолчанию month, если не изменено через sys.settings.set issue.accumulate_by
 * календарная неделя, приходящаяся на два месяца, - это две разные недели для обеспечения правильного учёта месячного лимита трафика
 * без разницы, какой именно интервал накопления был (и был ли вообще) у выпуска, который будет выбран базовым
 * например, первого числа в понедельник выпуски с любым интервалом накопления будут использовать один и тот же выпуск с нужным именем как базовый,
 * так как он будет удовлетворять и условию, что он в один и тот же день, и условию, что в одну и ту же неделю, и условию, что в один и тот же месяц
 */
final class AccumulateBy extends Enum
{
    private const MONTH = 'month';
    private const WEEK = 'week';
    private const DAY = 'day';
}
