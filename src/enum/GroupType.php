<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 * addrType enum
 *
 * @extends Enum<GroupType::*>
 * @method static GroupType LIST()
 * @method static GroupType FILTER()
 */
final class GroupType extends Enum
{
    private const LIST = 'list';
    private const FILTER = 'filter';
}
