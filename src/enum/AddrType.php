<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 * addrType enum
 *
 * @extends Enum<AddrType::*>
 * @method static AddrType EMAIL()
 * @method static AddrType MSISDN()
 * @method static AddrType VIBER()
 * @method static AddrType CDID()
 * @method static AddrType PUSH()
 * @method static AddrType VK()
 * @method static AddrType TG()
 * @method static AddrType ANY()
 * @method static AddrType ID()
 */
final class AddrType extends Enum
{
    private const EMAIL = 'email';
    private const MSISDN = 'msisdn'; // ном. тел
    private const VIBER = 'viber';
    private const CSID = 'csid'; // клиентский идентификатор
    private const PUSH = 'push';
    private const VK = 'vk';
    private const TG = 'tg';
    private const ANY = 'any';
    private const ID = 'id'; // внутренний id подписчика
}
