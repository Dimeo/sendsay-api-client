<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 * SendConfirmType enum
 *
 * @extends Enum<SendConfirmType::*>
 * @method static SendConfirmType CONFIRM()
 * @method static SendConfirmType UNSUBCANCEL()
 * @method static SendConfirmType UNSUBSENDERCANCEL()
 */
final class SendConfirmType extends Enum
{
    private const CONFIRM = 'confirm'; // высылка писем для подтверждения внесения в базу
    private const UNSUBCANCEL = 'unsubcancel'; // высылка писем для удаления из глобального стоп-листа
    private const UNSUBSENDERCANCEL = 'unsubsendercancel'; // высылка писем для удаления из стоп-листа по отправителю
}
