<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 * addrType enum
 *
 * @extends Enum<EmptyEnum::*>
 */
final class EmptyEnum extends Enum
{
    private const NUL = '';
}
