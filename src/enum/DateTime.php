<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 * DateTime enum
 * Список возможных вариантов точности для Даты-времени
 * @extends Enum<DateTime::*>
 * @method static DateTime YD()
 * @method static DateTime YH()
 * @method static DateTime YM()
 * @method static DateTime YS()
 */
final class DateTime extends Enum
{
    private const YD = 'yd'; // от года до дня. В данных записывается и выводится в формате "YYYY-MM-DD"
    private const YH = 'yh'; // от года до часа. В данных записывается и выводится в формате "YYYY-MM-DD hh"
    private const YM = 'ym'; // от года до минуты. В данных записывается и выводится в формате "YYYY-MM-DD hh:mm"
    private const YS = 'ys'; // от года до секунды. В данных записывается и выводится в формате "YYYY-MM-DD hh:mm:ss"






}
