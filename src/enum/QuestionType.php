<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 * QuestionType enum
 * Список возможных типов вопросов анкеты
 * @link https://sendsay.ru/api/api.html#%D0%94%D0%BE%D0%B1%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F-%D0%BD%D0%BE%D0%B2%D0%BE%D0%B3%D0%BE-%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D0%B0-%D0%B0%D0%BD%D0%BA%D0%B5%D1%82%D1%8B
 * @extends Enum<QuestionType::*>
 * @method static QuestionType FREE()
 * @method static QuestionType DATE_TIME()
 * @method static QuestionType ONE_OF_MULTIPLE()
 * @method static QuestionType NUMBER_OF_MULTIPLE()
 * @method static QuestionType INTEGER()
 */
final class QuestionType extends Enum
{
    private const FREE = 'free'; // свободный выбор
    private const DATE_TIME = 'dt'; // дата и время
    private const ONE_OF_MULTIPLE = '1m'; // выбор одного из списка
    private const NUMBER_OF_MULTIPLE = 'nm'; // выбор нескольких из списка
    private const INTEGER = 'int'; // целое число

}
