<?php

namespace Sendsay\ApiClient\enum\filterItem;

use MyCLabs\Enum\Enum;

/**
 * IssueDraftField enum
 *
 * @extends Enum<IssueDraftField::*>
 * @method static IssueDraftField DID()
 * @method static IssueDraftField NAME()
 * @method static IssueDraftField CHANNEL()
 * @method static IssueDraftField CREATED()
 * @method static IssueDraftField UPDATED()
 * @method static IssueDraftField ALIAS()
 * @method static IssueDraftField RELTYPE()
 * @method static IssueDraftField RELREF()
 * @method static IssueDraftField IS_TEMPLATE()
 * @method static IssueDraftField HAS_PUBLIC_PREVIEW()
 */
final class IssueDraftField extends Enum
{
    private const DID = 'issue_draft.id'; // код черновика
    private const NAME = 'issue_draft.name'; // "название"
    private const CHANNEL = 'issue_draft.channel'; // канал отправки email|sms|viber|push|vk|tg
    private const CREATED = 'issue_draft.create.date'; // "дата и время создания" -- Ys, null
    private const UPDATED = 'issue_draft.update.date'; // "дата и время последнего изменения" -- Ys, null
    private const ALIAS = 'issue_draft.alias'; // "альтернативный идентификатор"
    private const RELTYPE = 'issue_draft.reltype'; // число
    private const RELREF = 'issue_draft.relref'; // число
    private const IS_TEMPLATE = 'issue_draft.template'; // 0|1 -- признак шаблона (шаблон - заранее предустановленный черновик с оформлением)
    private const HAS_PUBLIC_PREVIEW = 'issue_draft.public_preview'; // "ccылка просмотра черновика без пароля"

}