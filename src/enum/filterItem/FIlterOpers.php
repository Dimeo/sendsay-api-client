<?php
namespace Sendsay\ApiClient\enum\filterItem;

use MyCLabs\Enum\Enum;

/**
 * addrType enum
 *
 * @extends Enum<FIlterOpers::*>
 * @method static FIlterOpers EQ()
 * @method static FIlterOpers NOT_EQ()
 * @method static FIlterOpers LESS_THAN()
 * @method static FIlterOpers LESS_OR_EQ()
 * @method static FIlterOpers GREATER_THAN()
 * @method static FIlterOpers GREATER_OR_EQ()
 * @method static FIlterOpers IN()
 * @method static FIlterOpers NOT_IN()
 * @method static FIlterOpers IS_NULL()
 * @method static FIlterOpers IS_NOT_NULL()
 */
final class FIlterOpers extends Enum
{
    // == != < <= > >=  in !in is_null !is_null
    private const EQ = '==';
    private const NOT_EQ = '!=';
    private const LESS_THAN = '<';
    private const LESS_OR_EQ = '<=';
    private const GREATER_THAN = '>';
    private const GREATER_OR_EQ = '>=';
    private const IN = 'in';
    private const NOT_IN = '!in';
    private const IS_NULL = 'is_null';
    private const NOT_IS_NULL = '!is_null';
}
