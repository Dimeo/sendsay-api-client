<?php

namespace Sendsay\ApiClient\enum\filterItem;

use MyCLabs\Enum\Enum;
use Sendsay\ApiClient\enum\AddrType;

/**
 * addrType enum
 *
 * @extends Enum<GroupListField::*>
 * @method static GroupListField GID()
 * @method static GroupListField NAME()
 * @method static GroupListField ADDR_TYPE()
 * @method static GroupListField CREATE_TIME()
 * @method static GroupListField UPDATE_TIME()
 * @method static GroupListField RELTYPE()
 * @method static GroupListField RELREF()
 * @method static GroupListField DICTNODE()
 * @method static GroupListField TYPE()
 * @method static GroupListField EXPIMP()
 * @method static GroupListField STATUS()
 */
final class GroupListField extends Enum
{

    private const GID = 'group.gid'; // символический код группы
    private const NAME = 'group.name'; // название группы
    private const ADDR_TYPE = 'group.addr_type'; // тип адресов:  email|msisdn|viber|csid|push|vk|tg|any
    private const CREATE_TIME = 'group.create.time'; // дата и время создания:  Ys | null
    private const UPDATE_TIME = 'group.update.time'; // дата и время последнего изменения: Ys | null
    private const RELTYPE = 'group.reltype'; // число
    private const RELREF = 'group.relref'; // число
    private const DICTNODE = 'group.dictnode'; // номер узла словаря
    private const TYPE = 'group.type'; // тип группы: list | filter
    private const EXPIMP = 'group.expimp'; // группа от Экспресс-импорта (!=0) или обычная (==0)
    private const STATUS = 'status'; // группа от Экспресс-импорта (!=0) или обычная (==0)

}