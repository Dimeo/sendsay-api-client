<?php

namespace Sendsay\ApiClient\enum\filterItem;

use MyCLabs\Enum\Enum;

/**
 * IssueClassField enum Список доступных полей класса выпуска
 * @linkhttps://sendsay.ru/api/api.html#%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%BA%D0%BB%D0%B0%D1%81%D1%81%D0%BE%D0%B2
 * @extends Enum<IssueClassField::*>
 * @method static IssueClassField CID()
 * @method static IssueClassField NAME()
 * @method static IssueClassField CREATED()
 * @method static IssueClassField UPDATED()
 * @method static IssueClassField ALIAS()
 * @method static IssueClassField RELTYPE()
 * @method static IssueClassField RELREF()
 */
final class IssueClassField extends Enum
{
    private const CID = 'issue_class.id'; // код класса
    private const NAME = 'issue_class.name'; // "название"
    private const CREATED = 'issue_class.create.date'; // "дата и время создания" -- Ys, null
    private const UPDATED = 'issue_class.update.date'; // "дата и время последнего изменения" -- Ys, null
    private const ALIAS = 'issue_class.alias'; // "альтернативный идентификатор"
    private const RELTYPE = 'issue_class.reltype'; // число
    private const RELREF = 'issue_class.relref'; // число
}