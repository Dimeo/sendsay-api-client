<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 * sendwhen enum
 *
 * @extends Enum<IssueSendWhen::*>
 * @method static IssueSendWhen NOW()
 * @method static IssueSendWhen LATER()
 * @method static IssueSendWhen DELAY()
 * @method static IssueSendWhen SAVE()
 * @method static IssueSendWhen TEST()
 */
final class IssueSendWhen extends Enum
{
    private const NOW = 'now';
    private const LATER = 'later';
    private const DELAY = 'delay';
    private const SAVE = 'save';
    private const TEST = 'test';
}
