<?php
namespace Sendsay\ApiClient\enum;

use MyCLabs\Enum\Enum;

/**
 * addrType enum
 *
 * @extends Enum<AuthMethod::*>
 * @method static AuthMethod LOGIN_PWD()
 * @method static AuthMethod API_KEY()
 * @method static AuthMethod NONE()
 */
final class AuthMethod extends Enum
{
    private const LOGIN_PWD = 'login';
    private const API_KEY = 'apikey';
    private const NONE = 'none';
}
