<?php

namespace Sendsay\ApiClient;

use Sendsay\ApiClient\entity\EntityI;

interface HttpClientI
{
    public function __construct(AuthConfig $authConfig, ?string $cacheDirPath);

    public function sendRequest(array $data): ApiResponse;
}