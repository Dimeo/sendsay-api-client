<?php

namespace Sendsay\ApiClient\service;

use Sendsay\ApiClient\exception\ApiResponseErrorException;
use Sendsay\ApiClient\HttpClient;

class AbstractService implements ServiceI
{
    protected HttpClient $httpClient;
    public function __construct(HttpClient &$httpClient)
    {
        $this->httpClient = $httpClient;
    }
    protected function _innerExecRequest(array $requestData, ?string $returnKeyName = NULL): array
    {
        try {
            $response = $this->httpClient->sendRequest($requestData);
            $responseData = $response->getData();
            if (NULL !== $responseData) {
                $result = $responseData[$returnKeyName] ?? $response->toArray();
            } else {
//                $result = $responseData;
                $result = $response->toArray();
            }
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
        return !is_array($result) ? [$result] : $result;
    }
}