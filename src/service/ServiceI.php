<?php

namespace Sendsay\ApiClient\service;

use Sendsay\ApiClient\HttpClient;

interface ServiceI
{
    public function __construct(HttpClient &$httpClient);
}