<?php

namespace Sendsay\ApiClient\service;

use Sendsay\ApiClient\entity\anketa\question\AbstractQuestion;
use Sendsay\ApiClient\exception\ApiResponseErrorException;
use Sendsay\ApiClient\exception\EntityValidationErrorException;

/**
 * Класс для работы с подтвержденными отправителями Email.
 * @link https://sendsay.ru/blog/courses/course/lesson3/
 */
class EmailSender extends AbstractService
{
    /**
     * Список подтвержденных отправителей
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%B0%D0%B4%D1%80%D0%B5%D1%81%D0%BE%D0%B2-email-%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D1%82%D0%B5%D0%BB%D0%B5%D0%B9
     */
    public function list(): array
    {
        $requestData = [
            'action' => 'issue.emailsender.list',
        ];
        return parent::_innerExecRequest($requestData, 'list');
    }

    /**
     * Чтение адреса отправителя
     * @param string $id
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%B0%D0%B4%D1%80%D0%B5%D1%81%D0%BE%D0%B2-email-%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D1%82%D0%B5%D0%BB%D0%B5%D0%B9
     */
    public function get(string $id): array
    {
        $requestData = [
            'action' => 'issue.emailsender.get',
            'id' => $id
        ];
        return parent::_innerExecRequest($requestData, 'obj');
    }

    /**
     * Создать или изменить адрес отправителя
     * Если id НЕ указан - создание, иначе - изменение
     * @param string $email
     * @param string|null $label
     * @param int|null $id
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B8%D0%BB%D0%B8-%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B0%D0%B4%D1%80%D0%B5%D1%81%D0%B0-email-%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D1%82%D0%B5%D0%BB%D1%8F
     */
    public function set(string $email, ?string $label = NULL, ?int $id = NULL): array
    {
        $obj = ['name' => $email];
        if (NULL !== $label) {
            $obj['label'] = $label;
        }

        $requestData = [
            'action' => 'issue.emailsender.set',
            'obj' => $obj,
            'return_fresh_obj' => 1
        ];

        if (NULL !== $id) {
            $requestData['id'] = $id;
        }
        return parent::_innerExecRequest($requestData, 'obj');
    }

    /**
     * Удаление адреса отправителя
     * @param string $id
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B0%D0%B4%D1%80%D0%B5%D1%81%D0%B0-email-%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D1%82%D0%B5%D0%BB%D1%8F
     */
    public function delete(string $id): array
    {
        $requestData = [
            'action' => 'issue.emailsender.delete',
            'id' => $id
        ];
        return parent::_innerExecRequest($requestData);
    }
}