<?php

namespace Sendsay\ApiClient\service;

/**
 * Класс для работы с подтвержденными отправителями SMS.
 * @link https://sendsay.ru/blog/courses/course/lesson3/
 */
class SMSSender extends AbstractService
{
    /**
     * Список SMS отправителей
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%B8%D0%BC%D1%91%D0%BD-sms-%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D1%82%D0%B5%D0%BB%D0%B5%D0%B9
     */
    public function list(): array
    {
        $requestData = [
            'action' => 'issue.smssender.list',
        ];
        return parent::_innerExecRequest($requestData, 'list');
    }

    /**
     * Чтение SMS отправителя
     * @param string $id
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A7%D1%82%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B8%D0%BC%D0%B5%D0%BD%D0%B8-sms-%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D1%82%D0%B5%D0%BB%D1%8F
     */
    public function get(string $id): array
    {
        $requestData = [
            'action' => 'issue.smssender.get',
            'id' => $id
        ];
        return parent::_innerExecRequest($requestData, 'obj');
    }

    /**
     * Создать или изменить SMS отправителя
     * Создание или изменение имени автоматически попадает на модерацию.
     * Если имя состоит из одних цифр, то его длина не должна превышать 15 символов.
     * Иначе имя может содержать от 1 до 13 символов больших и маленьких латинских букв и цифр
     * Если id НЕ указан - создание, иначе - изменение
     * @param string $name
     * @param int|null $id
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B8%D0%BB%D0%B8-%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B8%D0%BC%D0%B5%D0%BD%D0%B8-sms-%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D1%82%D0%B5%D0%BB%D1%8F
     */
    public function set(string $name, ?int $id = NULL): array
    {
        $requestData = [
            'action' => 'issue.smssender.set',
            'obj' => ['name' => $name],
            'return_fresh_obj' => 1
        ];

        if (NULL !== $id) {
            $requestData['id'] = $id;
        }
        return parent::_innerExecRequest($requestData, 'obj');
    }

    /**
     * Удаление SMS отправителя
     * @param string $id
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B8%D0%BC%D0%B5%D0%BD%D0%B8-sms-%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D1%82%D0%B5%D0%BB%D1%8F
     */
    public function delete(string $id): array
    {
        $requestData = [
            'action' => 'issue.smssender.delete',
            'id' => $id
        ];
        return parent::_innerExecRequest($requestData);
    }
}