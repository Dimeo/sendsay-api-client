<?php

namespace Sendsay\ApiClient\service;


use GuzzleHttp\Exception\GuzzleException;
use Sendsay\ApiClient\DTO\SendEmailFiltersData;
use Sendsay\ApiClient\DTO\SendEmailResultGroupsData;
use Sendsay\ApiClient\DTO\SendEmailSendWhenData;
use Sendsay\ApiClient\entity\EntityCollection;
use Sendsay\ApiClient\entity\EntityCollectionI;
use Sendsay\ApiClient\entity\issue\DraftToUpdate;
use Sendsay\ApiClient\entity\letter\AbstractLetter;
use Sendsay\ApiClient\entity\letter\EmailLetter;
use Sendsay\ApiClient\entity\letter\SmsLetter;
use Sendsay\ApiClient\enum\filterItem\GroupListField;
use Sendsay\ApiClient\enum\filterItem\IssueDraftField;
use Sendsay\ApiClient\enum\IssueSendWhen;
use Sendsay\ApiClient\exception\ApiResponseErrorException;
use Sendsay\ApiClient\exception\EntityValidationErrorException;
use Sendsay\ApiClient\filter\AbstractFilter;
use Sendsay\ApiClient\filter\IssueDraftFilter;
use Sendsay\ApiClient\helpers;

class Issue extends AbstractService
{
    /**
     * Разослать Email
     * Вызов асинхронный - получение положительного ответа означает, что задание на рассылку поставлено в очередь.     *
     * @param string $groupId ID группы-списка получателей
     * @param EmailLetter $letter
     * @param string|null $name Можно указать желаемое имя рассылки
     * @param array|null $label Набор меток для группировки в статистике
     * @param SendEmailSendWhenData|null $whenToSend
     * @param SendEmailFiltersData|null $filtersData Эти опции доступны не во всех тарифах
     * @param SendEmailResultGroupsData|null $resultGroupsDataData Кое что из этих опций не доступно в дешевых тарифах
     * @param string|null $classId
     * @param string|null $campaignId
     * @return mixed|string trackId задания в очереди sendSay
     * @link https://sendsay.ru/api/api.html#%D0%9E%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D1%82%D1%8C-%D0%B2%D1%8B%D0%BF%D1%83%D1%81%D0%BA
     */
    public function sendOutEmail(
        string $groupId,
        EmailLetter $letter,
        ?string $name = NULL,
        ?array $label = [],
        ?SendEmailSendWhenData $whenToSend = NULL,
        ?SendEmailFiltersData $filtersData = NULL,
        ?SendEmailResultGroupsData $resultGroupsDataData = NULL,
        ?string $classId = NULL,
        ?string $campaignId = NULL
    )
    {
        if (!$letter->validate()) {
            throw new EntityValidationErrorException($letter);
        }
        
        if (NULL === $whenToSend) {
            $whenToSend = new SendEmailSendWhenData(['sendwhen' => IssueSendWhen::NOW()]);
        }
        $sendWhen = helpers::processIssueSendWhen($whenToSend);
        $filtersData = empty($filtersData) ? [] : $filtersData->toArray();
        $resultGroupsDataData = empty($resultGroupsDataData) ? [] : $resultGroupsDataData->toArray();
        $requestData = array_merge(
            [
                'action' => 'issue.send',
                'letter' => $letter->toArray(),
                'group' => $groupId,
                // черновик слабее параметров.
                // Т.е. если письмо берется из существующего черновика, то одноименные параметры в черновике НЕ будут перебивать значения
                'weak_draft' => 1,
                
                'relink' => 1, // преобразовывать ссылки для учета перехода по ним
                'relink.param' => ['link' => 1, 'test' => 0] // ссылки, оформленные тегом <a> преобразовывать, а существование адресов в ссылках НЕ проверять
            ],
            $sendWhen,
            $filtersData,
            $resultGroupsDataData,
        );
        
        if (NULL !== $name) {
            $requestData['name'] = $name;
        }
        if (!empty($label)) {
            $requestData['label'] = $label;
        }
        if (NULL !== $classId) {
            $requestData['class.id'] = $classId;
        }
        if (NULL !== $campaignId) {
            $requestData['campaign.id'] = $campaignId;
        }
        
        $res = parent::_innerExecRequest($requestData, 'track.id');
        return $res[0] ?? '';
        
    }
    
    /**
     * Отправка тестового письма
     * @param EmailLetter $letter
     * @param array $theListToWhom список получателей не более пяти адресов
     * @param array|null $replacements массив с данными для подстановок
     * @param string|null $name необязательное имя
     * @param array|null $label необязательный набор меток
     * @return string trackId задания в очереди SendSay
     */
    public function sendTestEmail(
        EmailLetter $letter,
        array $theListToWhom,
        ?array $replacements = NULL,
        ?string $name = NULL,
        ?array $label = []
    ): string
    {
        if (!$letter->validate()) {
            throw new EntityValidationErrorException($letter);
        }
        $when = IssueSendWhen::TEST();
        $requestData = [
            'action' => 'issue.send',
            'letter' => $letter->toArray(),
            'sendwhen' => $when->getValue(),
            'mca' => $theListToWhom,
            'weak_draft' => 1,
            'relink' => 1,
            'relink.param' => ['link' => 1, 'test' => 0],
        ];
        if (NULL !== $name) {
            $requestData['name'] = $name;
        }
        if (!empty($label)) {
            $requestData['label'] = $label;
        }
        if (!empty($replacements)) {
            $requestData['extra'] = $replacements;
        }
        
        $res = parent::_innerExecRequest($requestData, 'track.id');
        return $res[0] ?? '';
    }
    
    /**
     * Отправка транзакционного письма типа Email.
     * Вызов асинхронный - получение положительного ответа означает, что задание на рассылку поставлено в очередь.
     * @param string $targetEmail Email получателя
     * @param EmailLetter $letter
     * @param string|null $name Название выпуска
     * @param array|null $label Набор произвольных меток выпуска для фильтрации(классификации. не более 10 меток длинной не более чем 128 символов)
     * @return string track.id
     * @link https://sendsay.ru/api/api.html#%D0%9E%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D1%82%D1%8C-%D0%B2%D1%8B%D0%BF%D1%83%D1%81%D0%BA
     */
    public function sendPersonalEmail(
        string $targetEmail,
        EmailLetter $letter,
        ?array $replacements = NULL,
        ?string $name = NULL,
        ?array $label = NULL
    ): string
    {
        return $this->_innerSendPersonalEmailOrSms($targetEmail, $letter, $name, $label, $replacements);
    }
    
    public function sendPersonalSms(
        string $targetPhone,
        SmsLetter $letter,
        ?array $replacements = NULL,
        ?string $name = NULL,
        ?array $label = NULL
    ): string
    {
        return $this->_innerSendPersonalEmailOrSms($targetPhone, $letter, $name, $label, $replacements);
    }
    
    private function _innerSendPersonalEmailOrSms(
        string $receiver,
        AbstractLetter $letter,
        ?string $name = NULL,
        ?array $label = NULL,
        ?array $replacements = NULL
    ): string
    {
        if (!$letter->validate()) {
            throw new EntityValidationErrorException($letter);
        }
        
        $requestData = [
            'action' => 'issue.send',
            'email' => $receiver,
            'letter' => $letter->toArray(),
            'group' => 'personal',
//            'sendwhen' => 'now',
            'sendwhen' => IssueSendWhen::NOW(),
            
            // черновик слабее параметров.
            // Т.е. если письмо берется из существующего черновика, то одноименные параметры в черновике НЕ будут перебивать значения
            'weak_draft' => 1,
            
            'relink' => 1, // преобразовывать ссылки для учета перехода по ним
            'relink.param' => ['link' => 1, 'test' => 0] // ссылки, оформленные тегом <a> преобразовывать, а существование адресов в ссылках НЕ проверять
        ];
        if (NULL !== $name) {
            $requestData['name'] = $name;
        }
        if (NULL !== $label) {
            $requestData['label'] = $label;
        }
        if (!empty($replacements)) {
            $requestData['extra'] = $replacements;
        }
        $res = parent::_innerExecRequest($requestData, 'track.id');
        return $res[0] ?? '';
    }
    
    /**
     * @param IssueDraftFilter $filter
     * @param IssueDraftField $order
     * @param bool $desc
     * @param int $skip
     * @param int $first
     * @return array
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D1%87%D0%B5%D1%80%D0%BD%D0%BE%D0%B2%D0%B8%D0%BA%D0%BE%D0%B2
     */
    public function draftsList(
        IssueDraftFilter $filter,
        IssueDraftField $order,
        bool $desc = false,
        int $skip = 0,
        int $first = 50
    ): array
    {
        $action = 'issue.draft.list';
        $sort = $order->getValue();
        if ($desc) {
            $sort = '-' . $sort;
        }
        $requestData = [
            'action' => $action,
            'filter' => $filter->toArray(),
            'order' => [$sort],
            'skip' => $skip,
            'first' => $first
        ];
        
        try {
            $response = $this->httpClient->sendRequest($requestData);
            $responseData = $response->getData();
            $responseData['last_page'] = boolval($responseData['last_page'] ?? false);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
        return $responseData;
    }
    
    /**
     * @param string $id
     * @param bool $noVars
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A7%D1%82%D0%B5%D0%BD%D0%B8%D0%B5-%D1%87%D0%B5%D1%80%D0%BD%D0%BE%D0%B2%D0%B8%D0%BA%D0%B0
     */
    public function draftGet(string $id, bool $noVars = false): DraftToUpdate
    {
        $action = 'issue.draft.get';
        $requestData = [
            'action' => $action,
            'id' => $id,
            'novars' => strval($noVars)
        ];
        $result = parent::_innerExecRequest($requestData, 'obj');
        
        return new DraftToUpdate($result);
    }
    
    /**
     * удаление черновика
     * @param string $id
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D1%87%D0%B5%D1%80%D0%BD%D0%BE%D0%B2%D0%B8%D0%BA%D0%B0
     */
    public function draftDelete(string $id): array
    {
        $requestData = [
            'action' => 'issue.draft.delete',
            'id' => $id,
        ];
        return parent::_innerExecRequest($requestData);
    }
    
    /**
     * @param string $id ID существующего черновика
     * @param DraftToUpdate $draft
     * @return DraftToUpdate
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B8%D0%BB%D0%B8-%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D1%87%D0%B5%D1%80%D0%BD%D0%BE%D0%B2%D0%B8%D0%BA%D0%B0
     */
    public function draftUpdate(string $id, DraftToUpdate $draft): DraftToUpdate
    {
        return $this->_innerDraftSet($draft, $id);
    }
    
    /**
     * @param DraftToUpdate $draft
     * @return DraftToUpdate
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B8%D0%BB%D0%B8-%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D1%87%D0%B5%D1%80%D0%BD%D0%BE%D0%B2%D0%B8%D0%BA%D0%B0
     */
    public function draftCreate(DraftToUpdate $draft): DraftToUpdate
    {
        return $this->_innerDraftSet($draft);
    }
    
    private function _innerDraftSet(DraftToUpdate $draft, ?string $id = NULL): DraftToUpdate
    {
        if (!$draft->validate()) {
            throw new EntityValidationErrorException($draft);
        }
        
        $requestData = [
            'action' => 'issue.draft.set',
            'obj' => $draft->toArray(),
            'return_fresh_obj' => 1
        ];
        if (!empty($id)) {
            $requestData['id'] = $id;
        }
        $result = parent::_innerExecRequest($requestData, 'obj');
        
        return new DraftToUpdate($result);
    }
    
}