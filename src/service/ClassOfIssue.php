<?php

namespace Sendsay\ApiClient\service;

use Sendsay\ApiClient\entity\issue\DraftToUpdate;
use Sendsay\ApiClient\enum\filterItem\IssueClassField;
use Sendsay\ApiClient\enum\ObjForCampaignType;
use Sendsay\ApiClient\exception\ApiResponseErrorException;
use Sendsay\ApiClient\exception\EntityValidationErrorException;
use Sendsay\ApiClient\filter\IssueClassFilter;
use Sendsay\ApiClient\service\AbstractService;

/**
 * Класыы выпуска.
 * Класс выпуска это набор разнообразных параметров выпуска влияющих на его формирование (параметры одноимённые с issue.send).
 * Подходит для хранения "типичного набора параметров" одинаковых для нескольких черновиков или выпусков.
 * Так же полезен просто для классификации выпусков.
 * Для использования класса просто укажите его id или alias в вызовах issue.send и/или issue.draft.*
 * Приоритет определения параметров выпуска в случае нескольких источников описан в issue.send.
 * @link https://sendsay.ru/api/api.html#%D0%9A%D0%BB%D0%B0%D1%81%D1%81%D1%8B-%D0%B2%D1%8B%D0%BF%D1%83%D1%81%D0%BA%D0%BE%D0%B2
 */
class ClassOfIssue extends AbstractService
{
    /**
     * Список классов
     * @return array
     * @param IssueClassFilter $filter
     * @param IssueClassField $order
     * @param bool $orderDesc
     * @param int $skip
     * @param int $pageCount
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%BA%D0%BB%D0%B0%D1%81%D1%81%D0%BE%D0%B2
     */
    public function list(
        IssueClassFilter $filter,
        IssueClassField $order,
        bool $orderDesc = false,
        int $skip = 0,
        int $pageCount = 50
    ): array
    {
        $sort = $order->getValue();
        if ($orderDesc) {
            $sort = '-' . $sort;
        }
        $requestData = [
            'action' => 'issue.class.list',
            'filter' => $filter->toArray(),
            'order' => [$sort],
            'skip' => $skip,
            'first' => $pageCount
        ];
        
        try {
            $response = $this->httpClient->sendRequest($requestData);
            $responseData = $response->getData();
            $responseData['last_page'] = boolval($responseData['last_page'] ?? false);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
        return $responseData;
    }
    
    /**
     * чтение класса
     * @param string $id
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A7%D1%82%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D0%BB%D0%B0%D1%81%D1%81%D0%B0
     */
    public function get(string $id): DraftToUpdate
    {
        $requestData = [
            'action' => 'issue.class.get',
            'id' => $id
        ];
        $res = parent::_innerExecRequest($requestData, 'obj');
        return new DraftToUpdate($res);
    }
    
    /**
     * создание класса выпуска
     * @param DraftToUpdate $data описание
     * @return DraftToUpdate данные созданной камапнии
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%D0%98%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D0%B0%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8
     */
    public function create(DraftToUpdate $data): DraftToUpdate
    {
        $data = $data->toArray();
        unset($data['letter']);
        
        $requestData = [
            'action' => 'issue.class.set',
            'obj' => $data,
            'return_fresh_obj' => 1
        ];
        $res = parent::_innerExecRequest($requestData, 'obj');
        return  new DraftToUpdate($res);
    }
    
    /**
     * изменение класса выпуска
     * @param DraftToUpdate $data
     * @return DraftToUpdate что получилось после правок
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B8%D0%BB%D0%B8-%D0%B8%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D0%BB%D0%B0%D1%81%D1%81%D0%B0
     */
    public function update(string $id, DraftToUpdate $data): DraftToUpdate
    {
        $data = $data->toArray();
        unset($data['letter']);
        $requestData = [
            'action' => 'issue.class.set',
            'id' => $id,
            'obj' => $data,
            'return_fresh_obj' => 1
        ];
        $res = parent::_innerExecRequest($requestData, 'obj');
        return new DraftToUpdate($res);
    }
    
    /**
     * удалить класс выпуска
     * @param array $ids список кандидатов на удаление
     * @return DraftToUpdate[]
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D0%BB%D0%B0%D1%81%D1%81%D0%B0
     */
    public function delete(array $ids): array
    {
        $requestData = [
            'action' => 'issue.class.delete',
            'id' => $ids
        ];
        $res = parent::_innerExecRequest($requestData, 'laststate');
        return is_array($res) ?  array_reduce($res, function ($ret, $i) {
            if (!empty($i['obj'])) {
                $ret[] = new DraftToUpdate($i['obj']);
            }
            return $ret;
        }, []) : [];
    }
}