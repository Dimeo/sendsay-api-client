<?php

namespace Sendsay\ApiClient\service;

use GuzzleHttp\Exception\GuzzleException;
use Sendsay\ApiClient\entity\EntityCollection;
use Sendsay\ApiClient\entity\EntityCollectionI;
use Sendsay\ApiClient\enum\filterItem\GroupListField;
use Sendsay\ApiClient\exception\ApiResponseErrorException;
use Sendsay\ApiClient\exception\EntityValidationErrorException;
use Sendsay\ApiClient\filter\AbstractFilter;
use Sendsay\ApiClient\HttpClient;

class GroupList extends AbstractService
{
    /**
     * @param string $listId
     * @return \Sendsay\ApiClient\entity\GroupList
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws ApiResponseErrorException
     */
    public function get(string $listId): \Sendsay\ApiClient\entity\GroupList
    {
        $requestData = [
            'action' => 'group.get',
            'id' => $listId,
            'with_filter' => 1
        ];
        try {
            $response = $this->httpClient->sendRequest($requestData);
            $result = new \Sendsay\ApiClient\entity\GroupList($response->data['obj']);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
        return $result;
    }

    /**
     * @param \Sendsay\ApiClient\entity\GroupList $groupList
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws ApiResponseErrorException
     * @throws EntityValidationErrorException
     */
    public function create(\Sendsay\ApiClient\entity\GroupList $groupList): string
    {
        $tmpIdValue = 'TMP_ID_VALUE_FOR_LIST';
        $id = $groupList->id;

        if (empty($id)) {
            $groupList->id = $tmpIdValue;
        }
        if (!$groupList->validate()) {
            throw new EntityValidationErrorException($groupList);
        }
        if ($groupList->id == $tmpIdValue) {
            $groupList->id = '';
        }

        $action = 'group.create';
        $requestData = $groupList->toArray();
        $requestData['action'] = $action;

        try {
            $response = $this->httpClient->sendRequest($requestData);
            $result = $response->data['id'];
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
        return $result;
    }

    /**
     * @param \Sendsay\ApiClient\entity\GroupList $groupList
     * @return \Sendsay\ApiClient\entity\GroupList
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(\Sendsay\ApiClient\entity\GroupList $groupList): \Sendsay\ApiClient\entity\GroupList
    {
        if (!$groupList->validate()) {
            throw new EntityValidationErrorException($groupList);
        }
        $requestData = [
            'action' => 'group.set',
            'return_fresh_obj' => 1
        ];
        $availableFields = ['id', 'name', 'reltype', 'relref'];
        $listData = $groupList->toArray();
        foreach($availableFields as $field) {
            if (isset($listData[$field])) {
                $requestData[$field] = $listData[$field];
            }
        }
        try {
            $response = $this->httpClient->sendRequest($requestData);
            $result = new \Sendsay\ApiClient\entity\GroupList($response->data['obj']);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
        return $result;
    }

    /**
     * @param string $listId
     * @return bool
     * @throws GuzzleException
     * @throws ApiResponseErrorException
     * Удалить список
     */
    public function delete(string $listId): bool
    {
        $action = 'group.delete';
        try {
            $response = $this->httpClient->sendRequest(['action' => $action, 'id' => $listId]);
        } catch (ApiResponseErrorException $e) {
            return false;
        }
        return true;
    }

    /**
     * @param string $listId
     * @param array $emailsToRemove
     * @return string
     * @throws GuzzleException
     * @throws ApiResponseErrorException
     * Очистка списка от указанных адресов
     */
    public function cleanFrom(string $listId, array $emailsToRemove): string
    {
        $action = 'group.clean';
        $requestData = [
            'action' => $action,
            'id' => $listId,
            'list' => $emailsToRemove,
            'sync' => 1
        ];
        try {
            $response = $this->httpClient->sendRequest($requestData);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
        return $response->request_id;
    }

    /**
     * @param string $listId
     * @return string
     * @throws GuzzleException
     * @throws ApiResponseErrorException
     * Полная очистка списка от всех адресов
     */
    public function clean(string $listId): string
    {
        $action = 'group.clean';
        $requestData = [
            'action' => $action,
            'id' => $listId,
            'all' => 1
        ];
        try {
            $response = $this->httpClient->sendRequest($requestData);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
        return $response->request_id;
    }
    
    /**
     * Добавить в список все контакты из другого списка
     * @param string $targetListId Id списка получателя контактов
     * @param string $sourceListId Id списка донора
     * @return string trackId запроса
     * @throws ApiResponseErrorException
     */
    public function copyContactsFromListTo(string $targetListId, string $sourceListId): string
    {
        return $this->_innerSnapshot($targetListId,$sourceListId,NULL, false);
    }
    
    /**
     * @param string $targetListId
     * @param array $emailsList Список Email адресов (не новых, а уже существующих)
     * @return string trackId запроса
     * @throws ApiResponseErrorException
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA-%D0%B3%D1%80%D1%83%D0%BF%D0%BF%D1%8B-%D0%A0%D0%B0%D1%81%D1%88%D0%B8%D1%80%D0%B8%D1%82%D1%8C-%D0%B3%D1%80%D1%83%D0%BF%D0%BF%D1%83-%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA
     *  Расширение (добавление) указанного списка контактами из других списков.
     * НЕ создает новых подписчиков, а берет уже существующих только!!
     */
    public function addExistingContactsToList(string $targetListId, array $emailsList): string
    {
        return $this->_innerSnapshot($targetListId,NULL,$emailsList, false);
    }
    
    /**
     * перенос контактов из списка в список
     * @param string $targetListId
     * @param string $sourceListId
     * @param bool $cleanBefore
     * @return string trackId запроса
     */
    public function moveContactsFromListTo(string $targetListId, string $sourceListId): string
    {
        return $this->_innerSnapshot($targetListId, $sourceListId,NULL, true);
    }
    
    private function _innerSnapshot(string $targetListId, ?string $sourceListId = NULL, ?array $emailsList = NULL, ?bool $cleanBefore = false): string
    {
        $action = 'group.snapshot';
        $from = [
            'sync' => 1 // синхронный запрос
        ];
        if (!empty($sourceListId)) {
            $from['group'] = $sourceListId;
        }
        if (!empty($emailsList)) {
            $from['list'] = $emailsList;
        }
        $requestData = [
            'action' => $action,
            'to' => [
                'id' => $targetListId,
                'clean' => intval($cleanBefore),
                'resync' => 0 // после внесения новых участников НЕ удалять старых, не попадающих более под условия источника
            ],
            'from' => $from,
            'sequence.event' => 1 // генерировать событие для триггеров
        ];
        
        try {
            $response = $this->httpClient->sendRequest($requestData);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
        return $response->request_id;
    }

    /**
     * @param AbstractFilter $filter
     * @param GroupListField $order
     * @param bool $desc
     * @param int $skip
     * @param int $first
     * @return EntityCollectionI
     * @throws GuzzleException
     * Пример использования
     * 1. Создать фильтр
     * $groupListFilter = new \Sendsay\ApiClient\filter\GroupListFilter();
     * 2. Подготовоить элементы фильтра:
     * Статус равен единице
     * $cond1 = new GroupListFilterItem(GroupListField::STATUS(), FIlterOpers::EQ(), 1);
     * И тип группы-списка = filter
     * $cond2 = new GroupListFilterItem(GroupListField::TYPE(), FIlterOpers::EQ(), GroupType::FILTER());
     * И тип адресов в группе-списке НЕ пуш
     * $cond3 = new GroupListFilterItem(GroupListField::ADDR_TYPE(), FIlterOpers::NOT_EQ(), AddrType::PUSH());
     * 3. добавить готовые элементы в фильтр
     * $groupListFilter->addFilterItem($cond1)->addFilterItem($cond2)->addFilterItem($cond3);
     * Сортировать по убыванию даты создания списка, получить первые 5 найденных
     * $result = $client->lists->getByFilter($groupListFilter,GroupListField::CREATE_TIME(), true, 0, 5);
     * Итоговый запрос будет выглядеть так:
     * {
     *   "action" : "group.list",
     *   "filter" : [
     *      {"a": "status", "op": "==", "v": "1"},
     *      {"a": "group.type", "op": "==", "v": "filter"},
     *      {"a": "group.addr_type", "op": "!=", "v": "push"}
     *   ],
     *   "order" : ["-group.create.time"],
     *   "skip" : 0,
     *   "first" : 5
     * }
     */
    public function getByFilter(AbstractFilter $filter, GroupListField $order, bool $desc = false, int $skip = 0, int $first = 50):EntityCollectionI
    {
        $action = 'group.list';
        $sort = $order->getValue();
        if ($desc) {
            $sort  = '-' . $sort;
        }
        $requestData = [
            'action' => $action,
            'filter' => $filter->toArray(),
            'order' => [$sort],
            'skip' => $skip,
            'first' => $first
        ];


        try {
            $response = $this->httpClient->sendRequest($requestData);
            $responseData = $response->getData();
            $isLastPage = boolval($responseData['last_page'] ?? false);
            $collection = new EntityCollection([],$isLastPage);
            foreach ($response->data['list'] as $groupListArr) {
                $collection->append(new \Sendsay\ApiClient\entity\GroupList($groupListArr));
            }
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
        return  $collection;
    }

}