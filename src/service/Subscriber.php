<?php

namespace Sendsay\ApiClient\service;

use GuzzleHttp\Exception\GuzzleException;
use Laminas\I18n\Exception\RuntimeException;
use Sendsay\ApiClient\ApiResponse;
use Sendsay\ApiClient\DTO\Id_Name;
use Sendsay\ApiClient\entity\anketa\BaseAnketa;
use Sendsay\ApiClient\entity\anketa\MemberAnketa;
use Sendsay\ApiClient\entity\MemberDatakey;
use Sendsay\ApiClient\entity\MemberDataSet;
use Sendsay\ApiClient\enum\AddrType;
use Sendsay\ApiClient\enum\ConfirmedResult;
use Sendsay\ApiClient\enum\IfExists;
use Sendsay\ApiClient\enum\SendConfirmType;
use Sendsay\ApiClient\exception\ApiResponseErrorException;
use Sendsay\ApiClient\exception\EntityValidationErrorException;

class Subscriber extends AbstractService
{
    /**
     * @param string $email
     * @param AddrType $type
     * @return bool
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%B8%D1%82%D1%8C-%D1%81%D1%83%D1%89%D0%B5%D1%81%D1%82%D0%B2%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%B0
     */
    public function isExists(string $email, AddrType $type): bool
    {
        $action = 'member.exists';
        $requestData = [
            'action' => $action,
            'addr_type' => $type->getValue(),
            'email' => $email,
        ];

        try {
            $response = $this->httpClient->sendRequest($requestData);
            $responseData = $response->getData();
            $result = boolval($responseData['list'][$email] ?? 0);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
        return $result;
    }

    /**
     * @param string $email
     * @return array
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%92%D1%81%D0%B5-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%B8-%D1%81-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D0%BC-%D0%B8%D0%B4%D0%B5%D0%BD%D1%82%D0%B8%D1%84%D0%B8%D0%BA%D0%B0%D1%82%D0%BE%D1%80%D0%BE%D0%BC
     */
    public function findById(string $email): array
    {
        $action = 'member.find';
        $requestData = [
            'action' => $action,
            'email' => $email
        ];
        try {
            $apiResponse = $this->httpClient->sendRequest($requestData);
            $responseData = $apiResponse->getData();
            $result = $responseData['list'] ?? [];
            $result = array_map(function ($subscriberItem) {
                $subscriberItem['addr_type'] = AddrType::from($subscriberItem['addr_type']);
                return $subscriberItem;
            }, $result);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
        return $result;
    }

    /**
     * Возвращает данные подписчика по указанному идентификатору
     * Значение идннтификаторра - это, как правило эл. почта, но не обязательно.
     * Также может быть ном.тел (msisdn), viber, клиентский идентификатор (csid),
     * идентификатор регистрации (push), vk, tg.
     * Кроме того в качестве идентификатора можно указывать внутренний id подписчика,
     * который присваивается ему после первоначального создания, но в этом случае необходимо точно указать
     * параметр addr_type = 'id'.
     * При использовании в качестве идентификатора эл.почты или ном.тел указывать addr_type не обязательно
     * @param string $email
     * @param AddrType|null $type
     * @return MemberDatakey
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%9F%D0%BE%D0%BB%D1%83%D1%87%D0%B8%D1%82%D1%8C-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D0%B5-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%B0-%D0%94%D0%9A
     */
    public function datakey(string $email, ?AddrType $type = NULL): MemberDatakey
    {
        return $this->_innerGetDatakey($email, '*', $type);
    }

    private function _innerGetDatakey(string $email, ?string $datakey = NULL, ?AddrType $type = NULL): MemberDatakey
    {
        $action = 'member.get';
        $requestData = [
            'action' => $action,
            'email' => trim($email),
            // в системную анкету member дополнительно включить массив со списком стоп-оистов
            'with_stoplist' => 2,
            // в системную анкету member дополнительно включить массив heads, содержащий список идентификаторов
            'with_heads' => 1,
            // missing_too = 0 -- в ответ не попадет ранее удаленный (отсутствующий) подписчик (будет сообщ. про ошибку)
            // missing_too = 1 -- в ответ попадет ранее удаленный (отсутствующий) подписчик с ключем в ответе missing = 1
            // что означает, что такой подписчик когда то был, но сейчас для использования не доступен
            'missing_too' => 0,
            // включить все анкеты, аналог ['base', 'member', 'custom'].
//            'datakey' => '*'
        ];
        if (!empty($type)) {
            $requestData['addr_type'] = $type->getValue();
        }
        if (!empty($datakey)) {
            $requestData['datakey'] = $datakey;
        }

        try {
            $apiResponse = $this->httpClient->sendRequest($requestData);
            $responseData = $apiResponse->getData();
            // в ответе может быть ключ datakey или obj в зависимости от содержания запроса
            $data = $responseData['datakey'] ?? [];
            if (empty($data)) {
                // NOTE!!: форма представления данных заполненных анкет отличается от аналогичных данных datakey
                // например в анкете member в случае  datakey есть ключ create, хранющий массив ['time' => '...', 'host' => '....']
                // а в анкетах (вызов без указания datakey возвращает obj) вместо этого два ключа 'create.time' и 'create.host'
                // есть и другие аналогичные несовпадения (update, import...). Как результат некоторые поля итогового экзампляра MemeberDatakey
                // оказываются не установленными в случае с анкетами (obj), но это не критично вроде.
                $data = $responseData['obj'] ?? [];
            }
            return MemberDatakey::createFromApiResponsedArray($data);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
    }

    /**
     * Апи возвращает анкеты подписчика
     * На основе ответа от Апи производится попытка заполнить данными объект MemberDatakey
     * @param string $email
     * @param AddrType|null $type
     * @return MemberDatakey
     * @link https://sendsay.ru/api/api.html#%D0%9F%D0%BE%D0%BB%D1%83%D1%87%D0%B8%D1%82%D1%8C-%D0%BE%D1%82%D0%B2%D0%B5%D1%82%D1%8B-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%B0-%D0%90%D0%92%D0%9E
     */
    public function ankets(string $email, ?AddrType $type = NULL): MemberDatakey
    {
        return $this->_innerGetDatakey($email, NULL, $type);
    }

    /**
     * специализированный вариант вызова member.get для чтения всех данных подписчиков, связанных с конкретным набором данных
     * @param int $dataSetId
     * @return MemberDataSet
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%9F%D0%BE%D0%BB%D1%83%D1%87%D0%B8%D1%82%D1%8C-%D0%BD%D0%B0%D0%B1%D0%BE%D1%80-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85-%D0%94%D0%9A
     */
    public function dataset(int $dataSetId): MemberDataSet
    {
        $requestData = [
            'action' => 'member.get',
            'email' => $dataSetId,
            'datakey' => '*',
            'addr_type' => 'dataset'
        ];
        try {
            $apiResponse = $this->httpClient->sendRequest($requestData);
            $responseData = $apiResponse->getData();
            return MemberDataSet::createFromApiResponsedArray($responseData);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
    }

    /**
     * @param string $id
     * @param MemberDatakey $data
     * @param AddrType|null $type
     * @param string|null $ip
     * @param int|null $confirmLetterId
     * @param int|null $noConfirmLetterId
     * @param bool $confirmRequired
     * @return MemberDatakey
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#C%D0%BE%D0%B7%D0%B4%D0%B0%D1%82%D1%8C-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%B0-%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%82%D1%8C-%D0%BE%D1%82%D0%B2%D0%B5%D1%82%D1%8B-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%B0-%D0%90%D0%92%D0%9E
     */
    public function create(
        string        $id,
        MemberDatakey $data,
        bool          $confirmRequired = true,
        ?int          $confirmLetterId = NULL,
        ?AddrType     $type = NULL,
        ?int          $noConfirmLetterId = NULL,
        ?string       $ip = NULL
    ): MemberDatakey
    {
        if ($data->validate()) {
            return $this->_innerSet(
                $id,
                $data,
                $type,
                $ip,
                IfExists::ERROR(),
                $confirmLetterId,
                $noConfirmLetterId,
                $confirmRequired,
                true
            );
        } else {
            throw new EntityValidationErrorException($data);
        }
    }

    /**
     * @param string $id
     * @param MemberDatakey $data
     * @param AddrType|null $type
     * @param string|null $ip
     * @param IfExists|null $ifExists
     * @param bool $confirmRequired
     * @param int|null $confirmLetterId
     * @param int|null $noConfirmLetterId
     * @param bool $returnFreshed
     * @return MemberDatakey
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#C%D0%BE%D0%B7%D0%B4%D0%B0%D1%82%D1%8C-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%B0-%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%82%D1%8C-%D0%BE%D1%82%D0%B2%D0%B5%D1%82%D1%8B-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%B0-%D0%90%D0%92%D0%9E
     */
    private function _innerSet(
        string        $id,
        MemberDatakey $data,
        ?AddrType     $type,
        ?string       $ip,
        ?IfExists     $ifExists,
        ?int          $confirmLetterId,
        ?int          $noConfirmLetterId,
        bool          $confirmRequired = true,
        bool          $returnFreshed = true
    ): MemberDatakey
    {
        $ip = ($ip === NULL) ? ($_SERVER['SERVER_ADDR'] ?? NULL) : $ip;

        $requestData = [
            'action' => 'member.set',
            'email' => $id,
            'newbie.confirm' => intval($confirmRequired),
            'obj' => $data->toArray(),
            'return_fresh_obj' => intval($returnFreshed)
        ];
        if (!empty($ip)) {
            $requestData['source'] = $ip;
        }

        if ($type !== NULL) {
            $requestData['addr_type'] = $type->getValue();
        }

        if ($ifExists !== NULL) {
            $requestData['if_exists'] = $ifExists->getValue();
        }

        if ($confirmLetterId !== NULL) {
            $requestData['newbie.letter.confirm'] = $confirmLetterId;
        }

        if ($noConfirmLetterId !== NULL) {
            $requestData['newbie.letter.no-confirm'] = $noConfirmLetterId;
        }

        try {
            $apiResponse = $this->httpClient->sendRequest($requestData);
            $responseData = $apiResponse->getData();
            return MemberDatakey::createFromApiResponsedArray($responseData['obj'] ?? []);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }

    }

    /**
     * @param string $id
     * @param MemberDatakey $data
     * @param AddrType|null $type
     * @param string|null $ip
     * @param int|null $confirmLetterId
     * @param int|null $noConfirmLetterId
     * @param bool $confirmRequired
     * @return MemberDatakey
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#C%D0%BE%D0%B7%D0%B4%D0%B0%D1%82%D1%8C-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%B0-%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%82%D1%8C-%D0%BE%D1%82%D0%B2%D0%B5%D1%82%D1%8B-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%B0-%D0%90%D0%92%D0%9E
     */
    public function update(
        string        $id,
        MemberDatakey $data,
        bool          $confirmRequired = true,
        ?int          $confirmLetterId = NULL,
        ?AddrType     $type = NULL,
        ?int          $noConfirmLetterId = NULL,
        ?string       $ip = NULL
    ): MemberDatakey
    {
        return $this->_innerSet(
            $id,
            $data,
            $type,
            $ip,
            IfExists::OVERWRITE(),
            $confirmLetterId,
            $noConfirmLetterId,
            $confirmRequired,
            true
        );
    }
    
    public function deleteOne(string $id, ?AddrType $type = NULL, ?bool $allData = false): array
    {
        return $this->_innerDelete($id, NULL, NULL, $allData, $type);
    }

    private function _innerDelete(
        ?string   $email = NULL, // идентификатор для удаления одного подписчика
        ?array    $list = NULL, // список идентификаторов для множественного удаления
        ?string   $group = NULL, // id группы, участников которой нужно удалить
        bool      $allData = false, // true: удалить все идентификаторы вместе с набором данных, false: удалить только заявленный идентификатор
        ?AddrType $type = NULL //
    ): array
    {
        $source = array_filter(compact('email', 'list', 'group'), function ($item) {
            return $item !== NULL;
        });
        if (count($source) == 0) {
            throw new RuntimeException('Subscriber`s id(ids) was not supplied for deleting');
        }
        if (count($source) > 1) {
            throw new RuntimeException('Please, supply just something one of "email" or "list" or "group". Not two(three) of them.');
        }
        $keys = array_keys($source);

        $requestData = [
            'action' => 'member.delete',
            'wipe' => intval($allData),
            'sync' => 1,
            $keys[0] => $source[$keys[0]], // email | list | group
        ];
        if ($type !== NULL) {
            $requestData['addr_type'] = $type->getValue();
        }
        try {
            $apiResponse = $this->httpClient->sendRequest($requestData);
            $responseData = $apiResponse->getData();
            return $responseData['list'] ?? [];
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
    }

    public function deleteList(array $ids, ?AddrType $type = NULL, ?bool $allData = false): array
    {
        return $this->_innerDelete(NULL, $ids, NULL, $allData, $type);
    }

    public function deleteGroup(string $groupId, ?AddrType $type = NULL, ?bool $allData = false): array
    {
        return $this->_innerDelete(NULL, NULL, $groupId, $allData, $type);
    }

    /**
     * @param string $id
     * @param AddrType|null $type
     * @return array
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%B8%D0%B4%D0%B5%D0%BD%D1%82%D0%B8%D1%84%D0%B8%D0%BA%D0%B0%D1%82%D0%BE%D1%80%D0%BE%D0%B2
     */
    public function headsList(string $id, ?AddrType $type = NULL): array
    {
        $requestData = [
            'action' => 'member.head.list',
            'email' => $id
        ];
        if (NULL !== $type) {
            $requestData['addr_type'] = $type->getValue();
        }
        try {
            $apiResponse = $this->httpClient->sendRequest($requestData);
            $responseData = $apiResponse->getData();
            return $responseData['list'] ?? [];
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
    }

    /**
     * @param string $id
     * @param int $headId
     * @param bool|null $confirmRequired
     * @param AddrType|null $type
     * @param AddrType|null $headType
     * @return array
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%9F%D1%80%D0%B8%D1%81%D0%BE%D0%B5%D0%B4%D0%B8%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B8%D0%B4%D0%B5%D0%BD%D1%82%D0%B8%D1%84%D0%B8%D0%BA%D0%B0%D1%82%D0%BE%D1%80%D0%B0
     */
    public function attachHead(string $id, int $headId, ?bool $confirmRequired = true, ?AddrType $type = NULL, ?AddrType $headType = NULL): ApiResponse
    {
        $requestData = [
            'action' => 'member.head.attach',
            'email' => $id,
            'head' => $headId,
            'newbie.confirm' => intval($confirmRequired),
        ];

        if (NULL !== $type) {
            $requestData['addr_type'] = $type->getValue();
        }
        if (NULL !== $headType) {
            $requestData['head_addr_type'] = $headType->getValue();
        }
        try {
            return $apiResponse = $this->httpClient->sendRequest($requestData);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }

    }

    /**
     * @param string $id
     * @param AddrType|null $type
     * @param bool|null $split
     * @return ApiResponse
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B8%D0%B4%D0%B5%D0%BD%D1%82%D0%B8%D1%84%D0%B8%D0%BA%D0%B0%D1%82%D0%BE%D1%80%D0%B0
     */
    public function detachHead(string $id, ?AddrType $type = NULL, ?bool $split = false): ApiResponse
    {
        $requestData = [
            'action' => 'member.head.detach',
            'email' => $id,
            'split' => intval($split),
        ];

        if (NULL !== $type) {
            $requestData['addr_type'] = $type->getValue();
        }
        try {
            return $this->httpClient->sendRequest($requestData);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }

    }

    /**
     * @param string $id Идентификатор подписчика (email, ном тел, и т.д.)
     * @param int $headId Новый идентификатор
     * @param bool|null $confirmRequired Подтверждать внесение в БД?
     * @param AddrType|null $type
     * @param AddrType|null $headType
     * @return ApiResponse
     * @throws GuzzleException
     */
    public function replaceHead(string $id, int $headId, ?bool $confirmRequired = true, ?AddrType $type = NULL, ?AddrType $headType = NULL): ApiResponse
    {
        $requestData = [
            'action' => 'member.head.replace',
            'email' => $id,
            'head' => $headId,
            'newbie.confirm' => intval($confirmRequired),
        ];

        if (NULL !== $type) {
            $requestData['addr_type'] = $type->getValue();
        }
        if (NULL !== $headType) {
            $requestData['head_addr_type'] = $headType->getValue();
        }
        try {
            return $apiResponse = $this->httpClient->sendRequest($requestData);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }

    }

    /**
     * @param string $id Идентификатор подписчика (Email, ном. тел. и т.д.)
     * @param array | NULL $group Список идентификаторов групп-фильтров. Не обязательный параметр для ограничения проверки
     * @param AddrType|null $type
     * @return array
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%A3%D1%87%D0%B0%D1%81%D1%82%D0%B8%D0%B5-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%B0-%D0%B2-%D0%B3%D1%80%D1%83%D0%BF%D0%BF%D0%B0%D1%85-%D1%84%D0%B8%D0%BB%D1%8C%D1%82%D1%80%D0%B0%D1%85
     */
    public function isInFilterGroups(string $id, ?array $group = NULL, ?AddrType $type = NULL): array
    {
        $requestData = [
            'action' => 'member.where',
            'email' => $id,
        ];

        if (NULL !== $group && !empty($group)) {
            $requestData['group'] = $group;
        }
        if (NULL !== $type) {
            $requestData['addr_type'] = $type->getValue();
        }
        try {
            $apiResponse = $apiResponse = $this->httpClient->sendRequest($requestData);
            $responsedData = $apiResponse->getData();
            return $responsedData['list'] ?? [];
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }

    }

    public function count(?string $group = NULL): array
    {
        $requestData = [
            'action' => 'member.list.count',
            'with_minmax' => 1
        ];
        if (NULL !== $group && !empty($group)) {
            $requestData['group'] = $group;
        }
        try {
            $apiResponse = $apiResponse = $this->httpClient->sendRequest($requestData);
            $responsedData = $apiResponse->getData();
            return $responsedData['obj'] ?? [];
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
    }

    /**
     * @return void
     * @link https://sendsay.ru/api/api.html#%D0%9E%D0%B1%D1%8A%D0%B5%D0%B4%D0%B8%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85-%D0%B4%D0%B2%D1%83%D1%85-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%BE%D0%B2
     *
     */
    public function mergeWith()
    {
        // TODO: реализовать по мере надобности
        // SEE: https://sendsay.ru/api/api.html#%D0%9E%D0%B1%D1%8A%D0%B5%D0%B4%D0%B8%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85-%D0%B4%D0%B2%D1%83%D1%85-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%BE%D0%B2
    }

    /**
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%BE%D0%B2
     */
    public function list(): array
    {
        // TODO: реализовать в случае потребности
        return [];
    }

    /**
     * Список подписчиков, входящих в группу
     * @param string $groupId
     * @param AddrType|null $addrType
     * @param int|null $page
     * @param int|null $pageSize
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%BF%D0%BE%D0%B4%D0%BF%D0%B8%D1%81%D1%87%D0%B8%D0%BA%D0%BE%D0%B2
     */
    public function ofGroupList(
        string    $groupId,
        ?AddrType $addrType = NULL,
        ?int      $page = NULL,
        ?int      $pageSize = NULL
    ): array
    {
        $requestData = [
            'action' => 'member.list',
            'group' => $groupId,
        ];
        if (NULL !== $addrType) {
            $requestData['addr_type'] = $addrType->getValue();
        }
        if (NULL !== $page && NULL !== $pageSize) {
            $pageSize = min($pageSize, 1000);
            $requestData['page'] = $page;
            $requestData['pagesize'] = $pageSize;
        }
        return $this->_innerExecRequest($requestData, 'list');
    }

    public function sendConfirmToId(
        string          $id,
        SendConfirmType $confirmType,
        int             $letterDraftId,
        ?AddrType       $type = NULL,
        ?string         $senderEmail = NULL,
        ?string         $issueName = NULL
    ): array
    {
        return $this->_innerSendConfirm($confirmType, $letterDraftId, $senderEmail, $issueName, $id, NULL, NULL, $type);
    }

    /**
     * @param SendConfirmType $confirmType
     * @param int $letterId код информационного письма. Письмо должно иметь заполненный адрес отправителя и не находиться на модерации
     * @param string|null $senderEmail
     * @param string|null $issueName "название выпуска" -- название создаваемого выпуска. Если отсутствует или пусто, то будет использована тема письма из letter
     * @param string|null $id Идентификатор подписчика
     * @param array|null $idsList либо список идентификаторов подписчиков
     * @param string|null $groupId либо идентификатор группы подписчиков
     * @param AddrType|null $type и их тип
     * @param bool|null $sync
     * @return array
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%92%D1%8B%D1%81%D0%BB%D0%B0%D1%82%D1%8C-%D0%BF%D0%B8%D1%81%D1%8C%D0%BC%D0%BE-%D0%BF%D0%BE%D0%B4%D1%82%D0%B2%D0%B5%D1%80%D0%B6%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5
     */
    private function _innerSendConfirm(
        SendConfirmType $confirmType,
        int             $letterId,
        ?string         $senderEmail = NULL,
        ?string         $issueName = NULL,
        ?string         $id = NULL,
        ?array          $idsList = NULL,
        ?string         $groupId = NULL,
        ?AddrType       $type = NULL,
        ?bool           $sync = true
    ): array
    {
        $requestData = [
            'action' => 'member.sendconfirm',
            'letter' => $letterId,
            'sync' => intval($sync),
        ];
        if (NULL !== $type) {
            $requestData['addr_type'] = $type->getValue();
        }
        if (NULL !== $issueName) {
            $requestData['issue_name'] = $issueName;
        }
        if ($confirmType == SendConfirmType::UNSUBSENDERCANCEL() && empty($senderEmail)) {
            throw new \http\Exception\RuntimeException('When send confirm type is "unsubsendercancel" senderEmail is required.');
        }

        switch ($confirmType) {
            case SendConfirmType::UNSUBSENDERCANCEL():
                $requestData[$confirmType->getValue()] = $senderEmail;
                break;
            default:
                $requestData[$confirmType->getValue()] = 1;
        }

        if (NULL !== $id) {
            $requestData['email'] = $id;
        } elseif (NULL !== $idsList) {
            $requestData['list'] = $idsList;
        } elseif (NULL !== $groupId) {
            $requestData['group'] = $groupId;
        }

        try {
            $apiResponse = $this->httpClient->sendRequest($requestData);
            $responsedData = $apiResponse->getData();
            return $responsedData['list'] ?? [];
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }

    }

    public function sendConfirmToList(
        array           $idsList,
        SendConfirmType $confirmType,
        int             $letterDraftId,
        ?AddrType       $type = NULL,
        ?string         $senderEmail = NULL,
        ?string         $issueName = NULL
    ): array
    {
        return $this->_innerSendConfirm($confirmType, $letterDraftId, $senderEmail, $issueName, NULL, $idsList, NULL, $type);
    }

    public function sendConfirmToGroup(
        string          $groupId,
        SendConfirmType $confirmType,
        int             $letterDraftId,
        ?AddrType       $type = NULL,
        ?string         $senderEmail = NULL,
        ?string         $issueName = NULL
    ): array
    {
        return $this->_innerSendConfirm($confirmType, $letterDraftId, $senderEmail, $issueName, NULL, NULL, NULL, $type);
    }

    /**
     * Подтвердить внесение в базу
     * @param string $id
     * @param string $confirmationCode
     * @return ConfirmedResult
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%9F%D0%BE%D0%B4%D1%82%D0%B2%D0%B5%D1%80%D0%B4%D0%B8%D1%82%D1%8C-%D0%B2%D0%BD%D0%B5%D1%81%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B2-%D0%B1%D0%B0%D0%B7%D1%83
     */
    public function setConfirmed(string $id, string $confirmationCode): ConfirmedResult
    {
        $requestData = [
            'action' => 'member.confirm',
            'email' => $id,
            'cookie' => $confirmationCode,
        ];
        try {
            $apiResponse = $apiResponse = $this->httpClient->sendRequest($requestData);
            $responsedData = $apiResponse->getData();
            $result = $responsedData['error'] ?? 'success';
            return ConfirmedResult::from($result);
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }

    }

    /**
     * возвращает информацию об адресе, не связанную с существованием подписчика и даже в случае, если подписчик уже удалён.
     * @param string $email
     * @param AddrType|null $type
     * @return array
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%98%D0%BD%D1%84%D0%BE%D1%80%D0%BC%D0%B0%D1%86%D0%B8%D1%8F-%D0%BE%D0%B1-%D0%B0%D0%B4%D1%80%D0%B5%D1%81%D0%B5
     */
    public function emailInfo(string $email, ?AddrType $type = NULL): array
    {
        $requestData = [
            'action' => 'email.get',
            'email' => $email,
        ];
        if (NULL !== $type) {
            $requestData['addr_type'] = $type->getValue();
        }

        try {
            $apiResponse = $apiResponse = $this->httpClient->sendRequest($requestData);
            $responsedData = $apiResponse->getData();
            return $responsedData['obj'] ?? [];
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
    }

    /**
     * @param string $email идентификатор подписчика
     * @param AddrType|null $type тип идентификатора
     * @param Id_Name|null $autoGroup автоматически создавать(дополнять) группу-список, содержащую подписчиков, не прошедших проверку
     * @param bool|null $cleanGroup предварительно очищать список из параметра $autoGroup?
     * @return array
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0-%D0%B0%D0%B4%D1%80%D0%B5%D1%81%D0%BE%D0%B2
     */
    public function verifyEmail(string $email, ?AddrType $type = NULL, ?Id_Name $autoGroup = NULL, ?bool $cleanGroup = NULL): array
    {
        return $this->_innerVerifyEmail($email, NULL, NULL, $type, $autoGroup, $cleanGroup);
    }

    /**
     * @param string|null $email идентификатор подписчика
     * @param array|null $list или список идентификаторов подписчика
     * @param string|null $groupId или идентификатор группы подписчиков
     * @param AddrType|null $type тип идетнификатора
     * @param Id_Name|null $autoGroup автоматически создавать(дополнять) группу-список, содержащую подписчиков, не прошедших проверку
     * @param bool|null $cleanGroup предварительно очищать список из параметра $autoGroup?
     * @return array
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0-%D0%B0%D0%B4%D1%80%D0%B5%D1%81%D0%BE%D0%B2
     */
    private function _innerVerifyEmail(
        ?string   $email = NULL,
        ?array    $list = NULL,
        ?string   $groupId = NULL,
        ?AddrType $type = NULL,
        ?Id_Name  $autoGroup = NULL,
        ?bool     $cleanGroup = NULL
    ): array
    {
        $requestData = [
            'action' => 'email.test',
            'delivery.error' => 1,
            'smtp.test' => 'lite',
        ];
        $source = array_filter(compact('email', 'list', 'groupId'), function ($i) {
            return $i !== NULL;
        });
        if (count($source) != 1) {
            throw new \http\Exception\RuntimeException('Exactly one of email or list or groupId is required.');
        }
        $keys = array_keys($source);
        $requestData[$keys[0]] = $source[$keys[0]];

        if (NULL !== $type) {
            $requestData['addr_type'] = $type->getValue();
        }
        if (NULL !== $autoGroup) {
            $requestData['auto_group'] = $autoGroup->toArray();
        }
        if (NULL !== $cleanGroup) {
            $requestData['clean_group'] = intval($cleanGroup);
        }

        try {
            $apiResponse = $apiResponse = $this->httpClient->sendRequest($requestData);
            $responsedData = $apiResponse->getData();
            return $responsedData['list'] ?? [];
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
    }

    /**
     * @param array $emailsList список идентификаторов подписчиков
     * @param AddrType|null $type тип идентификатора
     * @param Id_Name|null $autoGroup автоматически создавать(дополнять) группу-список, содержащую подписчиков, не прошедших проверку
     * @param bool|null $cleanGroup предварительно очищать список из параметра $autoGroup?
     * @return array
     * @throws GuzzleException
     * @link https://sendsay.ru/api/api.html#%D0%9F%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B0-%D0%B0%D0%B4%D1%80%D0%B5%D1%81%D0%BE%D0%B2
     */
    public function verifyEmailsList(array $emailsList, ?AddrType $type = NULL, ?Id_Name $autoGroup = NULL, ?bool $cleanGroup = NULL): array
    {
        return $this->_innerVerifyEmail(NULL, $emailsList, NULL, $type, $autoGroup, $cleanGroup);
    }

    /**
     * @param string $groupId идентификатор группы подписичиков
     * @param AddrType|null $type тип идентификатора
     * @param Id_Name|null $autoGroup автоматически создавать(дополнять) группу-список, содержащую подписчиков, не прошедших проверку
     * @param bool|null $cleanGroup предварительно очищать список из параметра $autoGroup?
     * @return array
     * @throws GuzzleException
     */
    public function verifyEmailsOfGroup(string $groupId, ?AddrType $type = NULL, ?Id_Name $autoGroup = NULL, ?bool $cleanGroup = NULL): array
    {
        return $this->_innerVerifyEmail(NULL, NULL, $groupId, $type, $autoGroup, $cleanGroup);
    }

    /**
     * @param string $email
     * @param AddrType|null $type
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BE%D1%88%D0%B8%D0%B1%D0%BE%D0%BA-%D0%B4%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B8
     */
    public function emailCleanDeliveryErrors(string $email, ?AddrType $type): array
    {
        return $this->_innerClearDeliveryErrors($email, NULL, NULL, $type);
    }

    private function _innerClearDeliveryErrors(
        ?string   $email = NULL,
        ?array    $list = NULL,
        ?string   $group = NULL,
        ?AddrType $type = NULL
    ): array
    {
        $source = array_filter(compact('email', 'list', 'group'), function ($i) {
            return $i !== NULL;
        });
        if (count($source) !== 1) {
            throw new \http\Exception\RuntimeException('Exactly one parameter of "id" or "idsList" or "groupId" must be supplied');
        }
        $sourceName = array_keys($source)[0];

        $requestData = [
            'action' => 'email.cleanerror',
            'sync' => 1,
            $sourceName => $source[$sourceName]
        ];

        if (NULL !== $type) {
            $requestData['addr_type'] = $type->getValue();
        }

        try {
            $apiResponse = $apiResponse = $this->httpClient->sendRequest($requestData);
            $responsedData = $apiResponse->getData();
            return $responsedData['list'] ?? [];
        } catch (ApiResponseErrorException $e) {
            throw $e;
        }
    }

    /**
     * @param array $emailslist
     * @param AddrType|null $type
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BE%D1%88%D0%B8%D0%B1%D0%BE%D0%BA-%D0%B4%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B8
     */
    public function emailsListCleanDeliveryErrors(array $emailslist, ?AddrType $type): array
    {
        return $this->_innerClearDeliveryErrors(NULL, $emailslist, NULL, $type);
    }

    /**
     * @param string $groupId
     * @param AddrType|null $type
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BE%D1%88%D0%B8%D0%B1%D0%BE%D0%BA-%D0%B4%D0%BE%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B8
     */
    public function groupCleanDeliveryErrors(string $groupId, ?AddrType $type): array
    {
        return $this->_innerClearDeliveryErrors(NULL, NULL, $groupId, $type);
    }
}