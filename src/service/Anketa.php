<?php

namespace Sendsay\ApiClient\service;

use Sendsay\ApiClient\entity\anketa\question\AbstractQuestion;
use Sendsay\ApiClient\exception\ApiResponseErrorException;
use Sendsay\ApiClient\exception\EntityValidationErrorException;

class Anketa extends AbstractService
{
    public function list(): array
    {
        return [];
    }

    /**
     * Чтение анкеты
     * @param string $key
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A7%D1%82%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B0%D0%BD%D0%BA%D0%B5%D1%82%D1%8B
     */
    public function get(string $key): array
    {
        $requestData = [
            'action' => 'anketa.get',
            'id' => $key
        ];
        return parent::_innerExecRequest($requestData, 'obj');
    }

    /**
     * Удаление анкеты
     * @param string $key
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B0%D0%BD%D0%BA%D0%B5%D1%82%D1%8B
     */
    public function delete(string $key): array
    {
        $requestData = [
            'action' => 'anketa.delete',
            'id' => $key
        ];
        return parent::_innerExecRequest($requestData);
    }

    /**
     * Создание анкеты
     * @param string $name
     * @param string|null $key
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B0%D0%BD%D0%BA%D0%B5%D1%82%D1%8B
     */
    public function create(string $name, ?string $key = NULL): array
    {
        $requestData = [
            'action' => 'anketa.create',
            'name' => $name,
            'return_fresh_obj' => 1
        ];

        if (NULL !== $key) {
            $requestData['id'] = $key;
        }
        return parent::_innerExecRequest($requestData, 'obj');
    }

    /**
     * Создать копию существующей анкеты
     * @param string $sourceKey
     * @param string $targetName
     * @param string|null $targetKey
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B0%D0%BD%D0%BA%D0%B5%D1%82%D1%8B
     */
    public function duplicate(string $sourceKey, string $targetName, ?string $targetKey = NULL): array
    {
        $requestData = [
            'action' => 'anketa.create',
            'name' => $targetName,
            'copy_from' => $sourceKey,
            'return_fresh_obj' => 1
        ];

        if (NULL !== $targetKey) {
            $requestData['id'] = $targetKey;
        }
        return parent::_innerExecRequest($requestData, 'obj');
    }

    /**
     * Сохранения анкеты
     * @param string $name
     * @param string|null $key
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B0%D0%BD%D0%BA%D0%B5%D1%82%D1%8B
     */
    public function update(string $name, ?string $key = NULL): array
    {
        $requestData = [
            'action' => 'anketa.set',
            'name' => $name,
            'return_fresh_obj' => 1
        ];

        if (NULL !== $key) {
            $requestData['id'] = $key;
        }
        return parent::_innerExecRequest($requestData, 'obj');
    }

    /**
     * создание нового вопроса анкеты
     * @param string $key
     * @param AbstractQuestion $question
     * @return array
     * @throws EntityValidationErrorException | ApiResponseErrorException
     * @link https://sendsay.ru/api/api.html#%D0%94%D0%BE%D0%B1%D0%B0%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F-%D0%BD%D0%BE%D0%B2%D0%BE%D0%B3%D0%BE-%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D0%B0-%D0%B0%D0%BD%D0%BA%D0%B5%D1%82%D1%8B
     */
    public function createQuestion(string $key, AbstractQuestion $question): array
    {
        if (!$question->validate()) {
            throw new EntityValidationErrorException($question, print_r($question->getValidationErrors(), true));
        }
        $requestData = [
            'action' => 'anketa.quest.add',
            'anketa.id' => $key,
            'obj' => $question->toArray(),
            'return_fresh_obj' => 1,
        ];
        return parent::_innerExecRequest($requestData, 'obj');
    }

    /**
     * изменение вопроса анкеты
     * @param string $key
     * @param AbstractQuestion $question
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%98%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D0%B0-%D0%B0%D0%BD%D0%BA%D0%B5%D1%82%D1%8B
     */
    public function updateQuestion(string $key, AbstractQuestion $question): array
    {
        if (!$question->validate()) {
            throw new EntityValidationErrorException($question, print_r($question->getValidationErrors(), true));
        }
        $requestData = [
            'action' => 'anketa.quest.set',
            'anketa.id' => $key,
            'obj' => $question->toArray(),
            'return_fresh_obj' => 1,
        ];
        return parent::_innerExecRequest($requestData, 'obj');
    }

    /**
     * Удаление вопроса анкеты
     * @param string $key
     * @param string $questionKey
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D0%B0-%D0%B0%D0%BD%D0%BA%D0%B5%D1%82%D1%8B
     */
    public function deleteQuestion(string $key, string $questionKey): array
    {
        $requestData = [
            'action' => 'anketa.quest.delete',
            'anketa.id' => $key,
            'id' => $questionKey,
            'return_fresh_obj' => 1,
        ];
        return parent::_innerExecRequest($requestData, 'obj');
    }

    /**
     * Изменение порядка следования вопросов в анкете
     * @param string $key
     * @param array $order
     * @return array содержит список идентификаторов вопросов анкеты в нужном порядке
     * @link https://sendsay.ru/api/api.html#%D0%98%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BF%D0%BE%D0%B7%D0%B8%D1%86%D0%B8%D0%B8-%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D0%B0-%D0%B0%D0%BD%D0%BA%D0%B5%D1%82%D1%8B
     */
    public function orderQuestions(string $key, array $newOrder): array
    {
        $requestData = [
            'action' => 'anketa.quest.order',
            'anketa.id' => $key,
            'order' => $newOrder,
            'return_fresh_obj' => 1,
        ];
        return parent::_innerExecRequest($requestData, 'obj');

    }

    /**
     * Удалить вариант ответа для вопросов типа 1m, nm (один из списка, много из списка)
     * @param string $key идентификатор анкеты
     * @param string $questionKey идентификатор вопроса в анкете
     * @param string $answerVariantKey идентификатор варианта ответа в вопросе
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BE%D1%82%D0%B2%D0%B5%D1%82%D0%B0-%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D0%B0-%D0%B0%D0%BD%D0%BA%D0%B5%D1%82%D1%8B
     */
    public function deleteQuestionAnswerVariant(string $key, string $questionKey, string $answerVariantKey): array
    {
        $requestData = [
            'action' => 'anketa.quest.response.delete',
            'anketa.id' => $key,
            'quest.id' => $questionKey,
            'id' => $answerVariantKey,
            'return_fresh_obj' => 1,
        ];
        return parent::_innerExecRequest($requestData, 'obj');
    }

    /**
     * Изменение порядка следования вариантов ответов в вопросе анкеты для вопросов типа 1m, nm (один из списка, много из списка)
     * @param string $key идентификатор анкеты
     * @param string $questionKey идентификатор вопроса анкеты
     * @param array $newOrder список идентификаторов вариантов ответов в нужном порядке
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%98%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BF%D0%BE%D0%B7%D0%B8%D1%86%D0%B8%D0%B8-%D0%BE%D1%82%D0%B2%D0%B5%D1%82%D0%B0-%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D0%B0
     */
    public function orderQuestionAnswerVariant(string $key, string $questionKey, array $newOrder): array
    {
        $requestData = [
            'action' => 'anketa.quest.response.order',
            'anketa.id' => $key,
            'id' => $questionKey,
            'order' => $newOrder,
            'return_fresh_obj' => 1,
        ];
        return parent::_innerExecRequest($requestData, 'obj');

    }

}