<?php

namespace Sendsay\ApiClient\service;

use Sendsay\ApiClient\enum\ObjForCampaignType;
use Sendsay\ApiClient\service\AbstractService;

/**
 * Кампании позволяют объединять разные объекты для удобства и для упрощения получения сводной статистики.
 * Функционал кампаний доступен не на всех тарифах! Уточняйте у техподдержки.
 * @link https://sendsay.ru/api/api.html#%D0%9A%D0%B0%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8
 */
class Campaign extends AbstractService
{
    /**
     * Список компаний
     * @return array
     * @link https://sendsay.ru/api/api.html#C%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%BA%D0%B0%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B9
     */
    public function list(): array
    {
        $requestData = [
            'action' => 'campaign.list'
        ];
        
        $res = parent::_innerExecRequest($requestData, 'list');
        return $res;
    }
    
    /**
     * чтение кампании
     * @param string $id
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A7%D1%82%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D0%B0%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8
     */
    public function get(string $id): array
    {
        $requestData = [
            'action' => 'campaign.get',
            'id' => $id
        ];
        return parent::_innerExecRequest($requestData, 'obj');
    }
    
    /**
     * создать кампанию
     * @param string $name название компании
     * @param string|null $description описание
     * @return array данные созданной камапнии
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%D0%98%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D0%B0%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8
     */
    public function create(string $name, ?string $description = NULL): array
    {
        $requestData = [
            'action' => 'campaign.set',
            'obj' => [
                'name' => $name
            ],
            'return_fresh_obj' => 1
        ];
        if (!empty($description)) {
            $requestData['obj']['descr'] = $description;
        }
        return  parent::_innerExecRequest($requestData, 'obj');
    }
    
    /**
     * изменение кампании
     * @param string $id id кампании
     * @param string $name название
     * @param string|null $description описание
     * @return array что получилось после парвок
     * @link https://sendsay.ru/api/api.html#%D0%A1%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%B8%D0%B5%D0%98%D0%B7%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D0%B0%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8
     */
    public function update(string $id, string $name, ?string $description = NULL): array
    {
        $requestData = [
            'action' => 'campaign.set',
            'id' => $id,
            'obj' => [
                'name' => $name
            ],
            'return_fresh_obj' => 1
        ];
        if (!empty($description)) {
            $requestData['obj']['descr'] = $description;
        }
        return parent::_innerExecRequest($requestData, 'obj');
    }
    
    /**
     * удалить кампанию
     * @param string $id
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BA%D0%B0%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8
     */
    public function delete(string $id): array
    {
        $requestData = [
            'action' => 'campaign.delete',
            'id' => $id
        ];
        return parent::_innerExecRequest($requestData);
    }
    
    /**
     * Список объектов кампании
     * @param string $id
     * @return array [['type' => '...', 'id' => '...'], ...]
     * @link https://sendsay.ru/api/api.html#C%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%BE%D0%B1%D1%8A%D0%B5%D0%BA%D1%82%D0%BE%D0%B2-%D0%BA%D0%B0%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8
     */
    public function objectsList(string $id): array
    {
        $requestData = [
            'action' => 'campaign.member.list',
            'id' => $id
        ];
        return parent::_innerExecRequest($requestData, 'list');
    }
    
    /**
     * Добавить объект в камапанию
     * @param string $id id камапании
     * @param string $objId id добавляемого объекта
     * @param ObjForCampaignType $type тип добавляемого объекта
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%94%D0%BE%D0%B1%D0%B0%D0%B2%D0%B8%D1%82%D1%8C-%D0%BE%D0%B1%D1%8A%D0%B5%D0%BA%D1%82-%D0%B2-%D0%BA%D0%B0%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D1%8E
     */
    public function addObject(string $id, string $objId, ObjForCampaignType $type): array
    {
        $requestData = [
            'action' => 'campaign.member.add',
            'id' => $id,
            'obj' => [
                'type' => $type->getValue(),
                'id' => $objId
            ]
        ];
        return parent::_innerExecRequest($requestData);
    }
    
    /**
     * Удалить объект из кампании
     * @param string $id
     * @param string $objId
     * @param ObjForCampaignType $type
     * @return array
     * @link https://sendsay.ru/api/api.html#%D0%A3%D0%B4%D0%B0%D0%BB%D0%B8%D1%82%D1%8C-%D0%BE%D0%B1%D1%8A%D0%B5%D0%BA%D1%82-%D0%B8%D0%B7-%D0%BA%D0%B0%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8
     */
    public function removeObject(string $id, string $objId, ObjForCampaignType $type): array
    {
        $requestData = [
            'action' => 'campaign.member.delete',
            'id' => $id,
            'obj' => [
                'type' => $type->getValue(),
                'id' => $objId
            ]
        ];
        return parent::_innerExecRequest($requestData);
    }
}