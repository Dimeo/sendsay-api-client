<?php

namespace Sendsay\ApiClient;

use Sendsay\ApiClient\service\Anketa;
use Sendsay\ApiClient\service\Campaign;
use Sendsay\ApiClient\service\ClassOfIssue;
use Sendsay\ApiClient\service\EmailSender;
use Sendsay\ApiClient\service\GroupList;
use Sendsay\ApiClient\service\Issue;
use Sendsay\ApiClient\service\ServiceI;
use Sendsay\ApiClient\service\SMSSender;
use Sendsay\ApiClient\service\Subscriber;

/**
 * @property GroupList $lists
 * @property Subscriber $subscribers
 * @property Anketa $anketa
 * @property EmailSender $emailSender
 * @property SMSSender $smsSender
 * @property Issue $issue
 * @property Campaign $campaign
 * @property ClassOfIssue $class
 */
class Client
{
    private const SERVICE_LIST = [
        'lists' => GroupList::class,
        'subscribers' => Subscriber::class,
        'anketa' => Anketa::class,
        'emailSender' => EmailSender::class,
        'smsSender' => SMSSender::class,
        'issue' => Issue::class,
        'campaign' => Campaign::class,
        'class' => ClassOfIssue::class,
        'filters' => '',
    ];
    private GroupList $lists;
    private Subscriber $subscribers;
    private Anketa $anketa;
    private EmailSender $emailSender;
    private SMSSender $smsSender;
    private Issue $issue;
    private Campaign $campaign;
    private ClassOfIssue $class;

    private HttpClient $httpClient;

    public function __construct(AuthConfig $authConfig, ?string $cacheDirPath = NULL)
    {
        $this->httpClient = new HttpClient($authConfig, $cacheDirPath);
    }

    /**
     * @param $name
     * @return mixed
     * @throws \RuntimeException
     */
    public function __get($name)
    {
        $isService = in_array($name, array_keys(self::SERVICE_LIST));
        if (!$isService) {
            throw new \RuntimeException('Trying to get unknown property ' . $name . ' in ' . self::class);
        }
        if (property_exists($this, $name) && !empty($this->$name)) {
            return $this->$name;
        } else {
            try {
                $this->createService($name);
                return $this->$name;
            } catch (\RuntimeException $e) {
                throw $e;
            }
        }
    }

    /**
     * @return bool
     * @param string $name
     * @throws \RuntimeException
     */
    public function createService(string $name): bool
    {
        $className = self::SERVICE_LIST[$name] ?? false;
        if (!$className) {
            throw new \RuntimeException("Service $name cannot be created. Classname for alias $name not found");
        }
        try {
            /** @var ServiceI $service */
            $service = new $className($this->httpClient);
            $this->$name = $service;
            return true;
        } catch (\RuntimeException $e) {
            throw $e;
        }
    }
}