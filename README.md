# sendsayApiClient
Клиент создан и поддерживается усилиями одного человека в рамках потребностей компании работодателя.  
Реализован не весь функционал, доступный в АПИ сендсей.  
Дополнение функционала, правка багов и др. активность производится исключительно по мере возникновения необходимости и(или) наличия свободного времени.  
Полная документация по [АПИ сендсей](https://sendsay.ru/api/api.html), которая использовалась для написания клиента.

## Установка
Выполните нижеследующее:

Добавить в список репозиториев, установить minimal stability и добавить данный пакет 
```
composer config repositories.dimeoRepo vcs https://gitlab.com/Dimeo/sendsay-api-client.git
composer config minimum-stability dev
composer require dimeo/sendsay-client:1.*
```

## Авторизация
```
$autoloadPath = './path-to-vendor/vendor/autoload.php';
require_once $autoloadPath;

// рекомендвано
$authData = [
    'login' => 'mainLogin',
    'apikey' => 'subLoginApiKey' // предварительно получить от сендсей
];

// Либо
$authData = [
    'login' => 'mainLogin',
    'sublogin' => 'subLogin',
    'password' => 'subloginPassword',
];

$authConfig = new \Sendsay\ApiClient\AuthConfig($authData);

$client = new \Sendsay\ApiClient\Client($authConfig);

```
## Общее

#### реализованые сервисы:
| Имя         | Класс                                  | Описание                     |
|-------------|----------------------------------------|------------------------------|
| lists       | Sendsay\ApiClient\service\GroupList    | Работа со списками.          |  
| subscribers | Sendsay\ApiClient\service\Subscriber   | Работа с подписчиками        |  
| anketa      | Sendsay\ApiClient\service\Anketa       | Работа с анкетами            | 
| emailSender | Sendsay\ApiClient\service\EmailSender  | Работа с отправителями Email |
| smsSender   | Sendsay\ApiClient\service\SMSSender    | Работа с отправителями SMS   |
| issue       | Sendsay\ApiClient\service\Issue        | Работа с выпусками           |
| campaign    | Sendsay\ApiClient\service\Campaign     | Работа с кампаниями          |
| class       | Sendsay\ApiClient\service\ClassOfIssue | Работа с классами выпусков   |

#### Доступ ко всем сервисам через экземпляр клиента по одноименным свойствам
```
$listService = $client->lists;
$subscribersService = $client->subscribers;
//  и т.д.
```
## Примеры:
### Добавить или изменить подписчика
```
$email = 'some.email@iLoveRussia.ru';
$groupListId = 'groupListAlias';
// OR
$groupListId = 10;

$confirmationDraftId = 61;

if (!$client->subscribers->isExists($email, AddrType::EMAIL())) {
    $baseAnketa = new \Sendsay\ApiClient\entity\anketa\BaseAnketa([
        'lastName' => 'Фамилия',
        'firstName' => 'Имя',
        'middleName' => 'Отчество',
        'city' => 'Город',
        'country' => 'Россия',
        'birthDate' => '1980-01-01'
    ]);
    
    // ИЛИ 
    $baseAnketa = new \Sendsay\ApiClient\entity\anketa\BaseAnketa([]);
    $baseAnketa->lastName = 'Фамилия';
    $baseAnketa->firstName = 'Имя';
    // и т.д.
    
    $memberAnketa = new \Sendsay\ApiClient\entity\anketa\MemberAnketa([]);
    $subscriber = new \Sendsay\ApiClient\entity\MemberDatakey($baseAnketa,$memberAnketa,[]);
    $subscriber->_group = [$groupListId => 1]; // добавить в списки
    // с отправкой письма подтверждения
    $subscriber = $client->subscribers->create($email, $subscriber, true,$confirmationDraftId);
    // или без отправки
    $subscriber = $client->subscribers->create($email, $subscriber, false);
} else {
    $subscriber = $client->subscribers->datakey($email);
    if (!$subscriber->isInGroup($groupListId)) {
        $subscriber->addToGrouplist($groupListId);
        $client->subscribers->update($email,$subscriber);
    }
}
// отправить письмо подтверждение
if (!$subscriber->isConfirmed()) {
    $res = $client->subscribers->sendConfirmToId(
        $email,
        \Sendsay\ApiClient\enum\SendConfirmType::CONFIRM(),
        $confirmationDraftId
    );
}
```
### Узнать что-то про подписчика по известному Email
```
$subscriberDK = $client->subscribers->datakey($email);
$subscriberInfo = $client->subscribers->findById($email);
$heads = $client->subscribers->headsList($email);
$emailInfo = $client->subscribers->emailInfo($email);
// ... 
```
### Удалить подписчика по известному Email
```
$deleteResult = $client->subscribers->deleteOne($email);
```
### Кампании
```
// Создание новой
$newCampaign = $client->campaign->create('test', 'ТЕстовая компания');

// Удаление
$campaignToDeleteId = 93;
$deleteResult = $client->issue->draftDelete($campaignToDeleteId);
```
### Создать черновик
```
$newDraft = new \Sendsay\ApiClient\entity\issue\DraftToUpdate([]);
$newDraft->name = 'Заголовок (название) черновика';

$letterForNewDraft = new \Sendsay\ApiClient\entity\letter\EmailLetter([]);
$letterForNewDraft->subject = 'Тема письма';
$letterForNewDraft->from_email = 'confirmed.email@sender.ru';
$letterForNewDraft->message = new \Sendsay\ApiClient\DTO\message\EmailMessage([
    'text' => 'текстовая версия письма', 
    'html' => '<html><body><div>Крутая верстка крутого письма</div></body></html>'
]);
// OR
$letterForNewDraft->message = new \Sendsay\ApiClient\DTO\message\EmailMessage([]);
$letterForNewDraft->message->html= '<html><body><div>Крутая верстка крутого письма</div></body></html>';
// и т.д.

$newDraft->letter = $letterForNewDraft;
$draft = $client->issue->draftCreate($newDraft);
```
### Изменить существующий черновик
```
$draftIdToUpdate = 10;
$draft = $client->issue->draftGet($draftIdToUpdate);
$draft->letter->message->html = 'New version of HTML code';
$draft->name = 'Новое название черновика';
$updatedDraft = $client->issue->draftUpdate($draftIdToUpdate, $draft);
```

### Отправить тестовое письмо
```
$html = <<<HTML
    <body>
        <h1>Верстка с персонолизацией!</h1>
        <div>Что-то интересное. </div>
        <div>Строка с подстановкой [% Info.key1 %] персонолизации</div>
        <div>Еще строка с подстановкой [% Info.key2 %] чего-то. </div>
    </body>
HTML;

$replacements = [
    'Info' => [
        'key1' => 'Подстановка 1',
        'key2' => 'Подстановка 2', 
        'key3' => 'Имя получаетеля'
    ]
];
$subject = 'Тема письма. Письмо для [% Info.key3 %]';
$message = new \Sendsay\ApiClient\DTO\message\EmailMessage([
    'html' => $html
]);
$email = new \Sendsay\ApiClient\entity\letter\EmailLetter([
    'from_name' => 'Имя отправителя',
    'from_email' => 'confirmed.email@sender.ru',
    'to_name' => 'получатель',
    'subject' => $subject,
    'message' => $message
]);
    
// Либо
$email = new \Sendsay\ApiClient\entity\letter\EmailLetter([]);
$email->from_name = 'Имя отправителя';
$email->from_email = 'confirmed.email@sender.ru';
$email->to_name = 'получатель';
$email->subject = $subject;
$email->message = new \Sendsay\ApiClient\DTO\message\EmailMessage([
    'html' => $html
]);


$sendToList = ['some.email@test.ru']; // список получателей тестового письма (не более пяти)
$trackId = $client->issue->sendTestEmail(
    $email,
    $sendToList,
    $replacements, 
    NULL, 
    ['label-test']
);

```
### Отправить транзакционное письмо со  своим контентом (без использования шаблона).

```
$emailMessage = new \Sendsay\ApiClient\DTO\message\EmailMessage([
    'html' => '<body>HTML код письма</body>',
    'text' => 'Текстовая версия письма'
]);
$recipientEmail = 'send@to.me.ru';

$sendSayEmail = new \Sendsay\ApiClient\entity\letter\EmailLetter([
    'from_name' => 'имя отправителя',
    'to_name' => 'имя получателя',
    'from_email' => 'confirmed.email@sender.ru',
    'subject' => 'Тема',
    'message' => $emailMessage,
    'basedraft_id' => 90, // id псевдошаблона, созданного по рекомендации техподдержки для группировки писем в статистике sendsay
    'autotext' => 256 // ограничитель для автогенерации текстовой версии письма
]);
$sendSayTrackId = $client->issue->sendPersonalEmail($recipientEmail, $sendSayEmail);
```

### Отправить транзакционное письмо по заранее сохраненному шаблону.
```
$prevPreparedDraftId = 10;
$replacements = [
    'key1' => 'value1',
    'key2' => 'value2',
    'key3' => 'value2',
];
$message = new Sendsay\ApiClient\entity\letter\EmailLetter([
    'draft_id' => $prevPreparedDraftId,
    'from_email' => 'confirmed@sender.ru', // должен быть в списке подтвержденных отправителе
]);
$mailTrackId = $sendsayApiClient->issue->sendPersonalEmail($email, $message, $replacements);
```
### Запустить рассылку черновика (шаблона) по списку
```
$draftId = 10;
$groupListId = 'coolList';

$letterToSend = new Sendsay\ApiClient\entity\letter\EmailLetter([
    'from_email' => 'confirmed@sender.ru',
    'draft_id' => $draftId
]);

$whenToSend = new Sendsay\ApiClient\DTO\SendEmailSendWhenData([
    'sendwhen' => Sendsay\ApiClient\enum\IssueSendWhen::NOW()
]);

// или для отложенной рассылки
$whenToSend = new Sendsay\ApiClient\DTO\SendEmailSendWhenData([
    'sendwhen' => Sendsay\ApiClient\enum\IssueSendWhen::DELAY(),
    'delayMinutes' => 30
]);
// также доступна рассылка в указанное время

$trackId = $client->issue->sendOutEmail(
    $groupListId,
    $letterToSend,
    'Имя выпуска(кампании)',
    ['Label1', 'LABEL2'],
    $whenToSend
);
```

#### to be continued..
